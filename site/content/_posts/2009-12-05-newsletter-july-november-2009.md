---
author: thulir
date: 2009-12-05 18:55:02+00:00
draft: false
title: Newsletter July - November 2009
type: post
url: /wp/2009/12/newsletter-july-november-2009/
categories:
- Newsletters
---

[![Welcome](http://www.thulir.org/wp/wp-content/uploads/2009/11/blog012.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/blog012.jpg)Monsoon is here and we are almost at the tail end of it now. Winter is slowly creeping in and there is eager anticipation of the English New Year and Pongal!! This years rains have been average, our wells are full and thanks to not having too many big storms, crops have survived the rains and doing well. Heres news of Happenings in Thulir in the past five months.

**BT Course summary: **

The first four months of the Basic Technology Course has been eventful. In these months, the students of the batch have slowly settled down and have begun to get into the flow of things. For Perumal, Rajamma, Devagi and Vinu its a first time experience in looking after a batch of teenagers and conducting classes/ sessions for them. The mood is generally upbeat [ with occasional mood swings and periods of inactivity]. The batch is a nice size so that at any given time, even if there are a few students in a low morale, enthusiasm from the rest ensures activity and progress. Luckily for us the interpersonal relationships have been good so far, and there is a fairly high team spirit, witnessed by the groups sticking together on weekly holidays and launching on expeditions. The recent trend  in the past couple of months has been  bicycling long distances [inspired by Balaji!]  -- a couple of days back they went cycling through wet roads in a mild drizzle for a 150 km ride on rickety second hand bicycles [they carried a set of tools with them and fixed problems on the way].

[![Electronics](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept006.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept006.jpg)

In terms of  classes, they have had a mix of theory classes -- basic math, Tamil writing, basic Engineering Drawing, Estimation of quantities, writing a diary, preparing bills of work done etc. Their worshop / practical sessions have included masonary, plumbing, electrical wiring, bee keeping, organic farming, crafts, soap making, electronics, etc. The progress has been slow but steady and after these months, the students are slowly gaining in confidence. Some areas , especially in skills they have been quick to learn, whereas other areas have been difficult  electronics, and some of the theory, for instance. Of course there is variation within the group also with different persons finding different subjects/ skills difficult to varying degrees.

**The B T course diaries**

As you may be aware, we have tried to maintain a monthwise diary of the course in this blog. The monthly summaries are below, and please follow the links for details.

October diary

The cycle shed is finally over!. A bamboo door was a nice opportunity for some of the students to show their craft skills.More electronics work and sessions on HIV/ AIDS are other highlights. [Read more](http://thulir.wordpress.com/the-bt-course-diary/october-09-diary)

September diary

The cycle shed construction continues and some serious organic farming work initiated. Soap making continues [with improving results!]. Sanjeev and anita come for 10 days and held a series of sessions on electronics, singing and games! [Read more](http://thulir.wordpress.com/the-bt-course-diary/september-09-diary)

August diary

August was a month of variety in the tasks taken up. More time spent in learning masonry, the fun of building an arch in the Cycle shed wall, learning to make soap, the first steps in learning to use a computer, gaining confidence in handling bee colonies, and more. [Read more](http://thulir.wordpress.com/the-bt-course-diary/august-09-diary-bt-course)

July diary

Growing in confidence, our students take on building construction skills; in a short time learning a variety of skills, hoping to play a part in the construction of the new Training Centre coming up in Sittilingi. [Read more](http://thulir.wordpress.com/the-bt-course-diary/july-09-diary-bt-course)

June diary

The new batch of Basic Technology Course students join this month and learning process starts right away with some plumbing, and electronics projects. [Read more](http://thulir.wordpress.com/the-bt-course-diary/june-09-diary-bt-course)

**Independence day celebrations : **

The students  were very keen to celebrate Independence day in Thulir. They offered to do all the organisation and started on the preparations on August 14th! After frantic search for a flag [they couldn't find one in the village and the Government school did not have a spare to lend!], they cycled to Kottapatti 10 kms away in the hope that they might get to buy one. Of course in this small village it wasn't available for sale, nor did anyone have a spare so they cycled back determined to get a flag ready. In Sittilingi they went to the man who sells clothes and bought pieces of cloth of the required colour and set off to Perumal's house 2 kms away at 8.30p.m. By now it was raining heavily and  dark. Perumal has a sewing machine and stitches dresses occasionally for family and neighbours. He then stitched the flag. Vinu, the resident artist, then took permanent markers and drew the wheel in the flag.The flag was ready,in the early hours of the 15th.

[![Independence Day](http://www.thulir.org/wp/wp-content/uploads/2009/11/blog020.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/blog020.jpg)

Next morning they discovered that there wasn't a suitable bamboo pole long enough and straight  to serve as a flag post, so off they went off in search of a suitable piece. They finally  found a 35 feet long straight pole and erected it. A bit of rope, lots of flowers and the flag was raised ready for unfurling. Meanwhile, excited kids, came trooping in all scrubbed and neatly dressed, and  stood in rows in front of the flag!! The flag was hoisted by Kannagi and Rajkumari-the Thulir cooks!

After this, we had  a cultural event with different groups performing songs and dance.All of us then trekked up the hill nearby to a nice spot with a clearing of rocks big enough for all of us to sit, enjoy the view of the entire valley below and have a snack!

This  event again showed us the great effort, time and persistence the children exhibit when the motivation  or interest to do something comes from within themselves! And further these were children who were regarded as the non-motivated, good-for nothing failures in the villages!!

**Visiting children's houses **

In October,  Rajamma, Devagi, Vinu and we  spent four days visiting  each of  our students' homes to interact with the parents. We went to houses in  Malaithangi village the first day, Sittilingi village the next 2 days and Moola sittilingi the last day.We realised that over the years we have established relationships. We also find that  parents are more open and  relaxed when we visit them in their houses than when we invite them for a group meeting in Thulir. Seeing them in their setting, we are also able to understand each student's situation and problems better. We had a better dialogue with the parents this year compared to earlier years.

**Visitors to Thulir: **

_Prof Ravindran and Mrs Vanaja Ravindran_ have been regular resource persons for Thulir. Prof Ravindran took sessions on Basic Engineering drawing and Vanaja akka has been teaching Tamil reading and writing skills for the seniors.

_Sanjeev, Anita and Vinod _came to Thulir  end of July. They held several sessions for the Basic Technology Course students as well as for the younger children.  The electronics sessions were great  fun.  They  demonstrated how  ' or'  and 'and' circuits work and how LEDs could be made to light up in different patterns when switches were put on in different  combinations. This helped to introduce the Binary system in the following week.

Sanjeev and Anita spent 10  days again in Thulir in October. This time they took a series of sessions on basics of electronics, Maths , and also taught new songs.

_Balaji_ too has been making regular trips to Thulir. Under his care the running programme has really taken off.Balaji has also been assessing the running styles  and potentials of the students and has introduced many new aspects in the training sessions. His first trip to Thulir was on a bicycle and this has made quite an impression on the Thulir seniors. Now almost every weekend they go biking for distances ranging from 20 kms to 140 kms ! What is more remarkable is that they all have old secondhand  bikes and so this means they have to do a bike servicing session before setting off and often have to carry out repairs on the way.

[![Balaji](http://www.thulir.org/wp/wp-content/uploads/2009/11/blog001.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/blog001.jpg)Balaji has also been teaching the senior students Tamil, communication skills, basic computer skills and commercial maths [prices, interests etc.]. He showed videos of football matches to introduce the sport and its rules and also showed a wonderful video on the enviromental crises.

_Dr Carolyn from UK_ who had visited Sittilingi earlier came for a visit. She did an origami session with the younger children.

[![Carolyn](http://www.thulir.org/wp/wp-content/uploads/2009/11/blog013.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/blog013.jpg)

_Mrs Julie_ from Nalam Child Development Centre, Namakkal visited Thulir.- She  conducted a very effective session on  the facts and myths around AIDS/ HIV.

_Ms. Niranjana_ an artist visited us and demonstrated sketching for our children.

_Anand__ _, electrician from NIOT took a 2 day session on  wiring and electricity.  The students were extremely happy with his classes as it was a very practical oriented class.

_Sourabh _, architect from Poona ,showed them pictures of his work. His projects involving reuse of  waste materials were an inspiration to us.

_Archana_, from  Keystone Foundation, Kotagiri, talked to our students about the Tribal forest right's bill. We need to have more sessions on this.

**Organic Farming workshop : **

People in this valley have always practised rainfed , organic and sustainable agriculture, planting many varieties of  millets. Cash crops, waterlogged paddy and introduction of pesticides and fertilisers  are relatively recent developments  but are strong influences. Even now, millets are grown organically.Perumal and kannagi attended a 3 day workshop conducted by Nammazhvar, who spearheads the organic farming movement in Tamil nadu.  They came back completely inspired and enthusiastic!' 'Iam never again going to use pesticides or fertilisers !!'declared Perumal. He had taken video clippings of important parts of the sessions. He shared this with the rest of the group and got them all higly enthused!The students prepared mulched beds,  sandwich beds,compost piles, prepared Panchakavyam, Jeevamirtham etc. and  planted  vegetables enthusiastically, Organic farmer Jayappa's visit happened then at the right time and gave an additional boost to their spirits. .But sadly September was a very dry month and many of their efforts didn't come to fruit.

[![Farming](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept004.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept004.jpg)

**Single rice planting:**

Some of the students took initiative and have started Single rice planting on a small patch. This is doing well. A  much more ambitious scale planting of S.R.I was planned but though a lot of work was done, it didn't materialise because of no rain at the crucial time.

**Kaveri Trail Marathon:**

For the second year, Thulir students participated in the Kaveri Trail Marathon.

Says Santhosh , who initiated running in Thulir,

"Firstly, the thulir kids - Vinoo, Ezhumalai and Sakthivel put on a stupendous performance. Even with the minimal training this season, all three of them did a great job at the run! Vinoo did a super fast half at 1hr 54 mins and Ezhumalai followed at 2hr 2mins. I tried hard to hold them back on the run and in the end they just bolted out :) Sakthivel, coming to Bangalore for the very first time in his life did his first 10.5K in about an hour! Congrats to the thulir kids  for being an inspiration for all of us."

**Bangalore Ultra marathon: **

Motivated by the Kaveri Trail experience, six of our senior students ran 12.5 km distance in this event. Santhosh prepared a training schedule for them. Vinu supervised the team and assumed a leadership role.

**Reading Skills:**

For years we have been trying to motivate our students to read books, through  various activities. Though they are interested now and do read when they have to, the teenagers are not motivated enough to pick up a book on their own initiative and read it  for enjoyment.They still view books with trepidation.We have always felt sad that we have been unable to show them the beauty and magic of the world of books! Reading is never a hobby for them. With   the younger kids too we have been having frequent  reading and storytelling sessions. But this  year , to our great delight,we find that the younger kids- [ 7-10year olds ]are really enthusiastic about  handling books;  they  read and derive enormous satisfaction from them. They borrow books from our library very regularly.We have sessions where each of them tells stories from their favourite books to the others. They also enact stories as small plays on saturdays.They make their own small books too.

In Math too,  we find the younger kids are more enthusiastic and capable – maybe because from a very young age they have been taught math as a series of enjoyable activities.

[![Reading](http://www.thulir.org/wp/wp-content/uploads/2009/11/blog022.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/blog022.jpg)

**Onam:**

Every year onam gives us an opportunity to make flower kolams. This year too on a small scale flower kolams were made.

[![Onam](http://www.thulir.org/wp/wp-content/uploads/2009/11/blog011.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/blog011.jpg)

**Puppet shows!! **

The younger children got onto a puppet making fever. They took up small stories from the library and tried to make puppets and enact the story. The first phase of making the puppets is over and a couple of enactments happened. It was a lot of fun and we realise there is scope for exploring this further as a learning tool.

[![Puppets](http://www.thulir.org/wp/wp-content/uploads/2009/11/blog003.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/blog003.jpg)

[![Puppets](http://www.thulir.org/wp/wp-content/uploads/2009/11/blog009.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/blog009.jpg)

**Travel and absences:**

Whenever we have had to travel out, our friends Prof Ravindran and Vanaja akka; Sanjeev and Anita or Balaji have been visiting Thulir and standing in for us. This has been of great help to us.We had to be away for quite a while in September due to illness in the family and hospitalisation.

Perumal, Rajamma, Devagi and Vinu work very well as a team and now take on a lot of responsibility managing Thulir. The Thulir kitchen functioning is also smooth now . We are now able to have volunteers staying here because of this. Rajamma and Devagi  are now learning to purchase all the provisons  either from local farmers, or the weekly market in Velanur or Kottapatti.They keep track of accounts and do a detailed stock- taking every month to calculate meal costs. The menu in the kitchen has  many experimental recipes using organic, traditional millets.

**Rains and tree planting:**

With the advent of the north east monsoon in November, we have restarted organic farming and planting in Thulir. This year we also  raised saplings for planting in the new Government high school in Sittilingi. Our students also planted some fuel and fodder trees there.

**Orissa trip:**

Perumal and Ezhumalai went with Mr. Ramasubramanium of Villagers to  2 projects in Orissa to help him in  his installations of micro- hydel power plants there. It was a very good learning and confidence boosting experience for them.
