---
author: nimda321
date: 2011-12-09 08:12:47+00:00
draft: false
title: Newsletter -- August to November 2011
type: post
url: /wp/2011/12/newsletter-august-to-november-2011/
categories:
- Newsletters
---

## [![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-19.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-19.jpg)




## Mural on the wall


This year the staff and students decided to make a large mural on the wall at the Entrance Lobby of Thulir. This became and interesting project where we made a collage of different individual pieces, so everybody got to paint a portion! New Batch joins Theerthagiri, Jasgannathan, Dhanabalan, Ajit, Gomati and Prasanth joined us this year in August. Sathya, Jayabal and Parameswaran decided to continue. So has Sakthivel who continues his training in Bangalore.
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-10.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-10.jpg)


## Smokeless chulha [firewood stove]


The old mud chulha we have been using in the kitchen need to be replaced and we built a new one using a RCC stove top. This became a good opportunity for our students to learn concrete molding and also in construction of a smokeless firewood stove.
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-9.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-9.jpg)
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-14.jpg)
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-15.jpg)


## Biogas


The Tribal Health Initiative donated a fiberglass made Biogas reactor, to produce gas for cooking. We decided to use it on an experimental basis, charging it with Cow dung, to learn the technology. We hope to scale it up when our dairy project takes off to provide for most of the cooking energy in our kitchen.
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-13.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-13.jpg)


## Wash area Construction


A new water conserving/ recycling outdoor Wash Area needed to be built and we thought it is a good opportunity for the new Batch to learn basics of masonry. The wash area consists of a simple wall , a drip irrigation pipes based plumbing [to regulate and lessen water coming out] and a simple mulch bed with Banana and Canah plants where the waste water is absorbed.
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-1.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-1.jpg)
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-2.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-2.jpg)
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-4.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-4.jpg)


## Lavanya and Gautham's visit


Lavanya and Gautham visited us in September. They have a Wood Wokshop in Bangalore where they design and make furniture. Sakhtivel is currently being trained by them. They did sessions where they taught making of lampshades using split bamboo frames and wrapping with paper.
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-21.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-21.jpg)
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-22.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-22.jpg)


## Exposure tour to  Kattaikkuttu Gurukulam  Kanchipuram


The Thulir Seniors went on an Exposure tour to the Kattaikkuttu Gurukulam near Kanchipuram. The School, a residential one, trains children in the traditional theater form of Kattaikkuttu and the students put up professional performances. It was a very inspiring experience. The school tries to balance formal academic learning with very serious, high quality training in Kattaikuttu. The apprentices group [made of former students of the Gurukula], conducted a session for our students and also demonstrated Kattaikuttu performance. Thulir students, taught paper folding, and soap making to the Gurukulam students. Though initially the students were shy, and it took a while to break the ice, once they started interacting, it became difficult for them to part when time came to say goodbyes. We hope to continue interactions for mutual benefits. From Kanchipuram, the students also visited Mahabalipuram to see the famous archaeological monuments and to go the Beach [a novelty for us living in an inland valley with mountains around]. The Kattaikuttu Gurukulam, kindly arranged for their vehicle for this trip and we are grateful.
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-6.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-6.jpg)


## Students leaving/ joining:


In October, Sathya and Gomathi stopped attending Thulir due to financial problems at home and so have gone for agricultural contract labour. Both have said they would like to rejoin after a few months when their contract gets over. Meanwhile, Parthibhan a regular in our evening sessions, underwent major surgery and so is out of school. He is joining Thulir for the rest fo this academic year as a full timer. Raman, a new student is also joining as we write this newsletter!


## Outdoor Seating area


Encouraged by the wash area construction, there was enthusiasm to continue. So a project to build another wash area [this time for the evening class children to use] and also seats under the trees in the Thulir Courtyard, were taken up.
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-40.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-40.jpg)
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-32.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-32.jpg)


## Work on land


The organic garden work continued and a nice circular vegetable patch with Permaculture beds took shape. In the months of October and November, this provided quite a bit of vegetables to our kitchen, not enough to replace all our purchases but hopefully enough to spur us on to produce more!!
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-17.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-17.jpg)
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-34.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-34.jpg)


## Cycling/ Running


A cycle tour to Sattanur Dam [about 60 kms away] was organized for the new batch boys as an introduction to long distance bicycle tours. In September, 6 of our students participated in the Kaveri Trail Marathon [2 of them did half marathons and the rest 10 km]. This has been a regular event Thulir Students have been participating over the years.
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-27.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-27.jpg)
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-28.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/newslet-dec-2011-28.jpg)

In November, the new batch students went to Bangalore to participate in a running event organized for the Karnataka Spastics Society. As has become a regular feature, they stayed at Ananya School. Sanjeev organized a day long Electronics workshop at his office [[Read his report for details and pics](http://www.thulir.org/wp/wp-content/uploads/2011/12/sanjeev-report-weekend-ws.pdf)].


## Visit to Chirag school


Anu and Krishna visited [Chirag School](http://chirag.org/what-we-do/education/chirag-school) in Nainital District of Uttarakhand. It was an opportunity to Interact with the staff and understand the curriculum and methods of teaching the School follows. A 3 day building construction workshop for class 4 and 5 students and teachers was conducted during this visit. A small outdoor seating area was built and basics of arch building were demonstrated. 

This visit was made possible by Prof Ravindran, Vanajakka and Balaji. Prof Ravindran and Vanajaakka were in  Sittilingi during this time and took classes for the students. Prof. took classes in basics of Plumbing, units and measurements, while Vanajakka took Tamil classes. Balaji, also kindly volunteered to be in Thulir and take classes for the students. He wrote a report on his visit. [[Read his Report](http://www.thulir.org/wp/wp-content/uploads/2011/12/Balajis-report-Visit_November_2011-.pdf)]
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/Kumaon-Nov-2011-178.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/Kumaon-Nov-2011-178.jpg)
[![](http://www.thulir.org/wp/wp-content/uploads/2011/12/Kumaon-Nov-2011-175.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/12/Kumaon-Nov-2011-175.jpg)


## The New Organic Farming Project


In October, Senthil and three of our students, proposed a project of Dairy and organic farming to the rest of Thulir. They wanted Thulir to make an investment in purchase of cows and some minimal facilities [like cow shed]. They would look after the cows, and also all the pieces of land around Thulir and in the new campus. They would do it outside of the Thulir class hours [mostly in the mornings and evenings and on holidays. The idea is to see how much income can be generated through these efforts and to learn organic farming techniques. The produce and the milk would be purchased by the Thulir Kitchen [so we get to eat home grown organic food!]. Over many sessions, the whole team brainstormed and a financial proposal was worked out to see what sort of investments needed to be made and also what possible income could be generated. The exercise yielded a positive picture, and we felt this was worth giving a try to increase our own understanding of organic Farming practice. The project started by mid October and since end October the four of them have started living in the campus and looking after the Cow and the 2 calves we already have. Efforts are on to purchase a second cow [of native breed, so it is taking some time].


************
