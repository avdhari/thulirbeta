---
author: thulir
date: 2005-12-07 13:19:00+00:00
draft: false
title: First Blog
type: post
url: /wp/2005/12/first-blog/
categories:
- Newsletters
---

[![](http://photos1.blogger.com/blogger/8103/1906/320/thulir.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/thulir.jpg)

This blog is for the purpose of updating all friends of Thulir about activities at [Thulir](http://thulir.org/). As you know, the Thulir website does have a news section, but we are able to update it only once every 4 months or so. Of course, we do send out the news updates to our mailing lists once every 2 months.

We thought we would like to experiment with blogging as a means of more quicker sharing of events at Thulir. Do write back your comments to improve this further.The Thulir web site has been updated and the design changed this month. Do have a look [[Thulir](http://thulir.org/)], if you haven't looked at it already.



clay work

Sittilingi, 07 december 2005

[![](http://photos1.blogger.com/blogger/8103/1906/320/DSCN0907.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/DSCN0907.jpg)We decided to celebrate the coming out of the sun [after weeks of rains and cloudy days], by moving outdoors and doing some clay work, last week. The various pieces made by the children are drying in the shade getting ready for firing. If the weather holds [there is a cyclonic storm in the making over the Bay of Bengal as this is being written!], we hope to fire them soon.


[![](http://photos1.blogger.com/blogger/8103/1906/320/DSCN0898.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/DSCN0898.jpg)
