---
author: nimda321
date: 2013-09-16 09:30:02+00:00
draft: false
title: Newsletter - January to August 2013
type: post
url: /wp/2013/09/newsletter-january-to-august-2013/
categories:
- Newsletters
---

### [![](http://www.thulir.org/wp/wp-content/uploads/2013/09/20130111_020.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/20130111_020.jpg)




### Pongal Celebrations- Kolam festival.


2012 had been a year of practically no rain. Most people of the valley including us still grow rainfed crops and most crops had failed last year. But yet, the year started off festively and colourfully with the Pongal and kolam festivals. This time this entire event was initiated, organised and managed by our young team. We are very proud of this fact since many of them are our former students. This young team has managed Thulir very well during our absences this year. Earlier we would always ask any of our friends to stay here and oversee things when we were not here. Now our team has grown in confidence and are able to manage things well. They had always been good in managing practical work but were diffident about handling the evening sessions. This year they have been handling even those quite professionally.


### Evening Classes for Government School Children


The evening classes have been going on more enthusiastically and are well attended. The children coming in the evening are of various groups. Rajammal, Devaki, Senthil, Ambika, Anjali, Nikhil and Anu take up teaching duties in the evenings. Attendance is not compulsory,so our numbers vary from 15 to 60 each day.


### [![](http://www.thulir.org/wp/wp-content/uploads/2013/09/11-200x300.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/11.jpg)


During the day we have regular preparatory sessions for the teachers to learn the content and plan the evening sessions. Most of the sessions are activity based learning sessions though there are classroom teaching sessions as well for some concepts. This year we started filing all the children's work more methodically. Each child has a file and all his/her work is filed there by the child. So that at the end of the year one can see what the child has accomplished. Besides the regular group coming, other children come from time to time to refer books for some specific reason or to use the learning materials or to prepare for some specific exam or project, etc.


### Children's Camp - April 20th -21st


Children from the age group 6-15 camped in Thulir for 2 days and participated in intensive sessions on life skills. There were many interactive sessions, discussions, plays, songs and movies around the theme. The sessions were on understanding oneself, one's strengths, needs, wants, relationships, communication, listening skills, conflict resolution etc. All of them felt these sessions were very important and necessary.


### Participation in the Auroville Marathon event


The highlight this time was younger students participating. 10 boys aged between 10 and 14, took part along with 5 of our older boys. Senthil and Sakthivel took initiative and organized and supervised the training schedule every morning for the youngsters.


### [![](http://www.thulir.org/wp/wp-content/uploads/2013/09/2-300x225.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/2.jpg)


Santhosh and Balaji organized a sight seeing trip for the group on February 9th. They enjoyed the experience so much and talked about it so much in the village that more children, especially girls,wanted to participate in the next running event. Senthil, Sakthivel and Parameswaran took the initiative for training with Santhosh and Balaji's guidance and the group of girls too turned up at Thulir religiously and enthusiastically every morning at 5.30 am for their running practice through

April and May! This was new! Earlier students would never come to Thulir during their annual vacation in May. Anandayana run . They participated in the Anandayana run – a run organized by Runner's high to raise awareness about projects working for underprivileged children- in Bangalore. This was the first time younger girls were participating and the highlight was that the group of 25 travelled to the venue and back by public transport. Senthil and the group have managed to sustain their interest and presently their training continues.


### [![](http://www.thulir.org/wp/wp-content/uploads/2013/09/3-300x225.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/3.jpg)





### Arrivals and Departures


Last year's batch have left for various options. **Chidhambaram and Parthiban** our full time students last year have joined the lab technician course started by Tribal Health Initiative.

**Parameswaran** has joined the ITI , Sankarapuram to formally study electrical wiring.

**Perumal, Ezhumalai **and** Chinnadurai** have finished their class 12 successfully and have gone on to do their graduation! Perumal has been inspired by his exposure to physics and electronics. He has decided to and has joined a B.Sc. Physics program in Salem and comes back to thulir now and then to share with us his experience of college.

The situation in the village has changed drastically in the last few years.  Changes which have happened over generations in our families have taken place within the last decade here. Subsistence farming has disappeared. The need for cash has grown. Money is now needed for education, health, for food and to buy the things one sees on TV. Costs of essentials have risen even here and the  prices of agricultural produce have not risen correspondingly.  Most families have started building concrete houses under the Government housing scheme but the money is invariably not enough and are in high interest debt. Other families have borrowed money to level the land to plant cash crops and are in debt due to that. In fact one of our very committed staff , **Sakthivel**, had to go on a long leave in order to earn money to repay his family's loan of Rs. 70,000 or he would lose his land. He has gone to Kerala to work as daily wage labour hoping to earn Rs.500 per day! He has promised to come back in three months but one never knows!

**Danabal **and** Jayabal** too had left for the same reasons, to do daily wages labour but on their return, we employed them in the constructions here so that they can be trained in masonry. A project to build a new cooking space has been initiated and is well under way with both the boys working hard on it. With guidance from Krishna and supervised by Senthil, the building is rapidly taking shape!

**Rajakumari** our cook in the Thulir kitchen has opened a small canteen in front of the hospital. We encouraged her to do that and we are happy that not only our students but also the employees learn something in Thulir and move on in life using what they learnt here. **Kamala**, our new cook is now in charge of the kitchen. Kannagi with her usual cheerfulness pitches in effortlessly and takes care of our guests and students when needed.

New teenagers, **Anjala, Masi, Madheswari, Suganthi** and **Thirupathi** have joined us this year.

**Nikhil Iyer**, a graduate of film making from SAE institute, Mumbai has been volunteering in Thulir from July. He is taking regular classes for the children in the evening and also English classes for the teachers in the afternoon as part of their preparation for teaching and communication with the outside world in general! The entire team has been cheered and enthused with his presence. He has brought in new energy and has become a very welcome part of all our work and life here.


### The Next Generation


Senthil and Rajammal have a baby boy, Rishi. The three of them stay in the Thulir campus now. Lakshmi has a baby girl and Sasikala has a baby boy! With the birth of two lively babies, both Lakshmi's and Sasikala's families move about with bright faces and a spring in their step. Since the babies need full time attention, they are on maternity leave for the next three months. Two young girls from the village – Suganthi and Madheshwari are taking to their new responsibilities as the stand-in caretakers of kutty thulir, in the absence of Lakshmi and Sasikala, with much cheerfulness and enthusiasm. It is good to see that the young ones are engaged in a variety of activities like solving puzzles, figuring out shapes, listening to stories, learning to sing new songs while singing the older ones better, making a collage or drawing the local birds here like the drongos and babblers. The whole group seems energetic and prosperous! Last year, some of the hospital staff wanted us to help them homeschool their children who were in the 3-5 age group. So the 'Kutty Thulir' started in the courtyard of professor Ravindran's house.


### [![](http://www.thulir.org/wp/wp-content/uploads/2013/09/4-300x225.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/4.jpg)


This year there has been more emphatic demand from the parents and staff of the hospital, for us to start a school.  We have expressed the opinion that a school should be a group initiative with complete parent and village participation in management and administration.  We would help in education aspects and in training of teachers and in fund raising. An education committee has been formed. We are having meetings now. This committee is first going to visit other similar schools, talk to people, discuss and decide whether the idea of starting a school is feasible or not in the next two months.  This process of talking, discussing, motivating and exploring takes time and effort. The process is slow but we hope more sustainable in the long run.


### Sittilingi Youth Club


Another inspiring happening is the formation of a youth group in the village. 35 young men of the village, - aged 18 to 35- a few of whom  are ex students of Thulir, got together in summer to organize a cricket tournament inviting teams from the other villages around. We supported them with some of the prize money. At that time we also motivated them to get together to do community work too. They immediately agreed and asked that we guide their initiative. And thus, the Sittilingi Youth Club was registered.

[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/5.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/5.jpg)

The youth group have now rented a house in the village and started a library. Thulir is helping them with the rent, books , newspaper etc. We hope that this space would be  a learning cum meeting space where learning would not be limited to book learning but to learning hand skills, music, theater, social and environmental consciousness etc. The positive aspect is that the initiative has come from the community and they have asked us to support them minimally.


### Clean Sittilingi/ Anti Plastic Campaign


Was the first major activity of the youth group. The staff and children of Thulir too were enthusiastic participants. We cleaned the main streets of plastic and the youth group talked to the shop owners and residents about refusing, reducing and managing plastic wastes. We had done these drives twice before but both those times we were initiating the events and they had a limited effect. But this time the youth were the driving force and they could drive the messages more forcefully as they were from the same village. They have also urged the panchayat to help with the dustbins and daily cleaning and maintenance. Many more such campaigns are planned for this year. Classes for nursing students Nursing students from THI attended classes on social, personal and environmental issues in Thulir once a week the whole of the last academic year. This year their classes are yet to start.


### Cooking with biogas


In the beginning of the year, we had a biogas plant set up for cooking lunch for the entire thulir staff (around fifteen people). The plant was run with the dung of six cows that were part of the thulir family. As much as this was a great alternative, little did we expect the rains to fail for the second continuous year. But what is the connection between steady supply of biogas and good rainfall?

Good rain=lots of grass to graze=healthy cows=lots of dung=lots of gas for cooking!

The summer was suffocatingly hot with temperatures soaring around 42 degrees through april and May. There was absolutely no grass and we had to sell all our cows except Lakshmi! This has resulted in us scaling down the plant to Anu and Krishna's house and that too not as the main source of cooking gas.


### Traditional Millet Festival


[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0624.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0624.jpg)

Due to the growing change in the diet of the village people to more “easy” foods like ration rice, vegetables grown with heavy chemical usage and inorganically grown fruits, the health of the people is deteriorating fast and many new diseases are evolving. The cases of diabetes, hypertension and other lifestyle diseases have dramatically increased, diseases that have been unheard of in this remote valley for the past twenty years. As rains had also failed for the past two years, the water table has reduced to a dangerously low level. But at the same time, cultivation of rice, sugarcane and other water intensive crops continues drawing much water through bore wells and diesel pumps in every farm. These have become a necessity. The problem goes deeper than it appears – while the water guzzling rice and sugarcane are cash crops, the traditional varieties of millets require much lesser water and are also many fold more nutritious than rice. The farmers here need returns for what they invested in the way of pesticides and fertilizers and thus this becomes a vicious cycle (millets do not need these chemicals as the seed varieties and the very nature of the plant itself is to be sturdy and resistant).

[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0611.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0611.jpg)

Considering this situation, Tribal Health Initiative decided to organize a traditional millet food festival in Thulir grounds to revive the usage of millets and to make clear the reasons for the diseases that the villagers are experiencing. For this cause, we had doctors Regi and Lalitha from the tribal hospital, here in Sittilingi, talk about the various problems that can be caused to the body by consumption of inorganically grown food.

[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0407.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0407.jpg)

To support their point of view, there were organic farmers coming in from all over the valley (and outside it too) to talk of the benefits of farming closer to nature. Of course, it was all from personal experience, which made the talk so much more relevant and easier to relate to for the farmer audience.

[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0562.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0562.jpg)

To make sure that it was not all talk and no fun, there were dance performances and singing too, all traditional, and supported by a tawil (a traditional drum) player. The words of some of the songs advocated the planting of spinach, beans and other vegetables for nutrition and the tale of a farmer who took care of his soil like a rich man would take care of all his gold (and so the song spoke about methods of soil conservation and sustainable farming) Traditional songs being sung by the villagers in praise of the soil and the rain gods.

To end this well was the most enjoyable part of the program – a feast of millets!! The locals had all been organized into groups and each group cooked delicious dishes for all of us! Ragi, thenai, saame, kambu were the main components of all the food in the feast.

[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0622.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0622.jpg)

Four varieties of grains that made close to 30 varieties of mouth watering treats, including thenai paayasam, kambu roti, ragi murukku and saame upma, and many many more. Whether or not one paid attention during the talking, everyone surely ate with great gusto! We made sure to try out ALL the dishes and greatly enjoyed the rather extravagant spread. Hope has been renewed in the form of a small food grain...


### Sports Day – Let the games begin!


Just after the millet festival was the sports day for the kids to make good use of all the protein and carbohydrates consumed the previous week! The day started off with one of Anu's signature talks on winning, losing and having the right attitude which in itself was great fun to watch as the kids completed many of her sentences with enthusiasm and followed it up with vigorous nodding.

[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0182-1.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0182-1.jpg)

After that was the warm up led by three volunteers from the students' group. Some parts of it looked like they were all doing the bhangra but it was good to see the entire group including the older ones Paramesh, Masi, Senthil, Anu and Nikhil bending and stretching along with the energetic children.

[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0235.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0235.jpg)

The sports kicked off with the track events including the sprints and relay. The day was quite hot and bright but the weather took a backseat for the kids who wore their best clothes and flowers in their hair! Well hydrated and having eaten enough, everyone was ready for a running start.

[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0377.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0377.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0370.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0370.jpg)

Next to follow were the long jump and high jump. This was completely new to the kids and was greatly enjoyed by the first time jumpers. Coaching for jumping from the crease was given in the evenings preceding sports day and we could see that the effort paid off well!



[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0471.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0471.jpg)







[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0302.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0302.jpg)





















After this was a short lunch break followed by a great rush of heads to the field to play musical chairs. There were a few technical issues with the sound but nothing could slow down the day – the kids jointly helped fix the issue and soon, a crowd was running around the chairs to melodious tamil songs from older movies. This game is popular in thulir. Its one where the players scoop a handful of water from a bucket, run a certain distance to where an empty soda bottle is placed and fill it as fast as they can. A nice way to cool the heated little bodies and learn to close those little gaps between the fingers through which water (and as some would say, money) flows away.

[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0300.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0300.jpg)














Kabaddi, the best part of sports day for some, the most dreaded for others, the sport that holds a lot of surprises for the players and audience alike. The capacity of the players to turn into monsters or mice is what the best part of this game is. Thulir children made the change into both and more creatures in between too.

To end the evening – TUG OF WAR! And the women won pulling the men over!

[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0304.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0304.jpg)















[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0309.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/DSC_0309.jpg)















Many old and new friends have dropped in for shorter durations, catching up and encouraging us. Thanks to all of you for being with us through all our ups and downs and keeping us going.


### Rain!


As we write this, we have had a few cooling showers of rain. Though it is not enough to recharge our wells, the grass has grown eagerly, fresh leaves have sprouted, buds have bloomed, some trees have broken out with profuse flowers and scores of butterflies flutter around enthusiastically. With promise of more rain, our land has been optimistically ploughed and planted with millets. There's hope in the air after all!


********



