---
author: nimda321
date: 2018-07-16 09:58:16+00:00
draft: false
title: Newsletter Jan - June 2018
type: post
url: /wp/2018/07/newsletter-jan-june-2018/
categories:
- Newsletters
---

Greetings from Thulir!
[![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180716_152035.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180716_152035.jpg)


### Sittilingi Run


The Sittilingi Run – initiated last year --  was held again on the 7th of January this year. The run was a huge hit locally. Around hundred men, women and children turned up early in the morning and enthusiastically ran the 10 km and 4 km runs! A group of runners from Runner's High, Bangalore, two Stanley Medical College Alumni and three runners from Thekampattu also participated. The runners were rewarded with a very tasty millet breakfast prepared by our women farmers' group and  T-shirts organised by Muthu.

[![](http://www.thulir.org/wp/wp-content/uploads/2018/07/b9a4ef765d918b71299ebe503c5b16a405045166.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/b9a4ef765d918b71299ebe503c5b16a405045166.jpg)


### Pongal Celebrations


The Pongal celebrations were a little subdued this year but the children surpassed themselves in their wall paintings and Kolams. Except for the blues and pinks, all the other colours used for the wall painting were derived from natural materials.

[![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180109_160906.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180109_160906.jpg)


### [![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180109_124038.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180109_124038.jpg)




### New Arrival in the Thulir Family


We welcome Ram and Archana' s new baby girl, Chaitanya! She was born in the Tribal Hospital during the Pongal holidays.


### Parents meeting


This year’s parents meeting in January started with a performance by our children with their eager parents in the audience. Some children read a story or an excerpt from their books, while the older groups performed a dance sequence and the 4 year olds did a small play. We ensured that all our children participated in these activities.

[![](http://www.thulir.org/wp/wp-content/uploads/2018/07/2018-07-20_11-49-46_946.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/2018-07-20_11-49-46_946.jpg)

Their lack of stage fear and openness in expressing themselves were appreciated by all the parents in the meeting that followed. The parents also gave their feedback  about the school and its teaching, learning approaches. Some parents expressed the concern that as we do not have a school van, children from hamlets that are further away were finding it hard to commute to the school. They decided to explore the possibility of parents getting together and arranging a vehicle to  transport the kids  from  these hamlets  to the school.

[![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180411_151359.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180411_151359.jpg)

The teachers brought up the topic of media and its impact on  children. Many parents expressed their helplessness in curtailing children's screen time. Encouraging children to read a variety of books and the importance of books in helping children grow and in promoting literacy among them was also discussed.

Maya, a volunteer from Switzerland, talked about the garbage problem that is plaguing the world and how the beauty of this landscape and our environment is ruined by garbage.


### Farmer’s meet


Our children did a short song and dance performance for the Annual Meeting of the Sittilingi  Organic Farmers Association!


### An introduction into the enchanting world of Bharatha Natyam


A unique experience was in store for all of us in March, thanks to Ms.Jyotsna and her team of dancers from the Shamkaram troupe, trained in the Kalakshetra school of classical dance. They came all the way from Chennai, engaged with the children during the day, did a stunning performance in the evening and left by night. Their unflagging interest in dance was highly contagious.

The creatures of the wild came alive in our classrooms. Herd of elephants went on a rampage, peacocks danced in the rain, fishes and crocodiles leaped out of the sea. Do we need explicit instruction to appreciate art ? Children immediately merged into the performance without any barriers and were captivated by the elegance, poise and grace of the movements.

[![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_0100.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_0100.jpg)

In the evening, the staff of Thulir, Hospital, SOFA and Porgai, alongside half the village community gathered in the meeting hall in the Sittilingi Organic Farmers Association campus. Jyotsna talked about dance and the performing arts in general. She opined that dance is not exclusive and talked about how we do not need any special qualification to appreciate this art form, the diversity of indigenous dance forms that exist in our land, how all our communities valued performing arts, how not to lose sight of this diversity, the dangers of dogmatism etc. With a few dance movements she demonstrated how dance was a language and can be used to express oneself. This talk helped us to gain a perspective to observe and understand the performance by the team that followed. This was the first time most of us witnessed a live Bharatanatyam recital.

[![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_0453-e1531733979558.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_0453.jpg)



[![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_0331.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_0331.jpg)

It was heartening to witness the effect of performing arts on human souls. Our thanks to the entire Shamkaram team for their efforts in bringing joy and dance to Sittilingi.


### Classical Dance Training


The Shamkaram team has offered to teach dance to our students and teachers. As a first step, Rajamma and Sasikala visited Shamkaram campus, Chennai, during the first week of May and learned some basic classical dance steps. They stayed at Yoga Vahini. This was their first visit to Chennai on their own. Thanks to the Yoga Vahini and Shamkaram teams for giving them a memorable time.


### Sports day at Thulir


Our annual sports day events were held in March, with enthusiastic participation from children and adults.


### [![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_0504.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_0504.jpg)




### [![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_1159.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_1159.jpg)




### Folk Dance Workshop


Sri Mohan from Porur conducted a workshop on rural folk song and dance from April 4th to 7th. The teachers and children enthusiastically learned Paraiyattam, Oyilattam, Karagam, Kummi, Kolattam, Kazhiyiyalattam and Villupaatu. It was a short but intense session and the whole campus resounded with music and dance from morning to night each day.

[caption id="attachment_2659" align="aligncenter" width="400"][![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180405_101059.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180405_101059.jpg) Practice session for Karagattam[/caption]

Villupaatu is a traditional form of musical narration in a group format addressing various social issues, usually with plenty of humour involved. The narration is started by the main figure of a Guru, and the disciplines intervene with doubts and comments. We were amazed by the children's ability to master the script, learn the songs and their cues and perform fluently. One or two of them were only six years old!


### Cultural Evening


The dance workshop culminated in a cultural evening where the children displayed all that they had learned to an audience of parents, villagers and staff of the various organisations in Sittilingi. Teachers also performed alongside their students. There was no recorded or film music. All songs and music were folk pieces provided live by children and teachers.

[![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180407_173726.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180407_173726.jpg)

The audience was enthralled by the performance. The villupaatu in particular was a big hit. It was very encouraging to have one or two parents go up on stage and express a very positive feedback of not only of the performance but of the school also.

[![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180407_174922.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180407_174922.jpg)


### Parents meeting


At the end of the academic year in April, we held another parent’s meeting. Our program  started with a detailed feedback session. All the parents expressed their appreciation of the  cultural evening performance, especially the villuppattu performance by our children. Accounts of parents contribution towards the nutritional expenses were reported. Some of the parents had not contributed anything to the school and the issue of sustaining this school amidst these challenges were discussed by the parents. Creation of a Parent's Association to get other parents involved in the school was discussed. It was a gratifying sight to witness the parents actively engaged in the discussion about uniforms, fundraising and finances, the shift to the new school, transport problems etc,  A strong sense of involvement of the community was evident.  This is not a school run by just  by one or two people but by the community.

Due to a dearth of good picture books and children’s fiction in Sittilingi, we had ordered books from NBT. These were kept on display during the meeting for the parents to  see and buy. Kids picked the books they  were already familiar with in the school and it was heartening to see the joy of holding the book in their faces.

[![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180411_171931.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180411_171931.jpg)

We think it is important to provide a wide range of experiences to the children. Learning doesn’t happen only in classrooms or behind four walls. We discussed the importance of involving children in household activities or in the field.


### Auroville Marathon


Sakthivel took a group of seven runners to participate in the Auroville Marathon on February 11th .


### Human chain in Thirvuannamalai


Our teachers and staff joined hands with Marudam team, Thiruvannamalai and rest of the teacher community for a human chain to express our concern at the recent incidents of violence against women and children and to emphasise the need to bring up our male children sensitively as well as to protect and cherish our girl children.

[![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180419_153336_HDR.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180419_153336_HDR.jpg)


### ASHA teacher training workshop


ASHA Chennai conducts a teacher training workshop every May. Rajammal had attended it earlier and this year Ravi participated. He has come back with numerous Math puzzles up his sleeve. It is nice to see the children quiz us on a new one each day.

[![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180528_165141_HDR.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180528_165141_HDR.jpg)


### Becoming Yoga Teachers!


Yoga Vahini from Chennai had conducted a fifteen month yoga course for the nurses in the hospital and three of the Thulir teachers. Practitioners from Yoga Vahini came to Sittilingi once a month for a period of two to three days. The teachers learned how to observe their bodies and the effects different Asanas have on different parts of the body. They also learned how to tailor their teaching according to their students' age, health and capacities. Yoga is a very simple form of physical activity where the mind, body and breath are synchronised.

At the end of the course, a graduation ceremony was held on June 24th at the Anna Centenary Library in Chennai. Chintamani, Ravi, Rajammal and Anu attended the function.

[![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180624_180110_HDR.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180624_180110_HDR.jpg)


### Visitors & Volunteers


Maya Muehlemann from Switzerland was here for three months. She brought with her years of expertise in teaching English for german/foreign students. She helped us see how teaching English could be stressfree and lively by the innovative use of materials, creation of worksheets, etc. We thank Maya for her efforts and wish her the best.

Niru Ramaswamy came back to spend February and March here. Niru conducted sessions with the children and teachers on identifying our emotions, recognising and acknowledging them and discovering positive and harmless ways of expressing and channelising them. She also helped organise our forest walks and did a lot of craft work with the teachers and students.

In March Lolle and Anne  -- medical electives from Germany -- spent their free time in the school. They were very good singers and taught the children many English songs. It was amazing to hear the children singing 'We shall overcome' with perfect diction and emotion. Children thoroughly enjoyed their singing and the tunes from the ukulele.

[![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180321_091125.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180321_091125.jpg)

Swetha Nambiar --  an artist and old friend of Thulir, -- visited for a week in February. The organic farmer's association had requested our students to do some artwork on the walls of their new Meeting hall. Swetha anchored this project and brought out the children's creativity to the fullest. The walls of the meeting hall came alive with scenes from the forest, hills, fields and villages. The art has been well appreciated by all subsequent visitors to the SOFA (Sittilingi Organic Farmers Association) meeting hall.

[![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_0199.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_0199.jpg)


### [![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_0211.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_0211.jpg)




### Construction Updates


The school buildings are progressing fast. We hope to shift to the new school sometime this year.  The classrooms have been designed so that they will stimulate the children's curiosity, creativity and learning.

The walls and roofs of all the classrooms are finished. The classrooms will have steel grill doors and windows so that there is more openness, light and air inside. These are being fabricated on site by the Thulir alumni.

[![](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180428_090614.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2018/07/IMG_20180428_090614.jpg)

We are very happy that the construction process itself has been a learning opportunity for all those involved. The buildings have been built completely by our ex- BT course students. They have become full-fledged masons and builders.  In addition, three young architects are at present interning on the building site. One of them, Muthu, has been here almost from the beginning. Dinesh has been here for six months and Meenakshi for the last three months. Other architects and architecture students have been coming in for shorter durations to learn practical skills  on site.

All this has been possible only because of your support. Thank you for being a part of our efforts.


*********
