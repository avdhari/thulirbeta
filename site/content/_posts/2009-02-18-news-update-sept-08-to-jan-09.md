---
author: thulir
date: 2009-02-18 18:26:45+00:00
draft: false
title: News update Sept 08 to Jan 09
type: post
url: /wp/2009/02/news-update-sept-08-to-jan-09/
categories:
- Newsletters
---

_Welcome to the News update from Thulir. In this post we review happenings of the past 5 months_




![”image”](//thulir.files.wordpress.com/2009/02/dec08-jan09-054.jpg)



**__**



_
_

**Marathon Fever**

As we write this blog today our senior students - 3 girls and 4 boys, have just finished participating in the Auroville Marathon. We just received an excited phone call from them. The marathon fever has been spreading in Thulir for some months now. After Sridhar's visit for the science camp [see our earlier news update], Team Asha came to Sittilingi for their pre marathon trial run in preparation for the Cauvery marathon.This visit inspired the Thulir students to take up running. Senthil and Vinu participated in the Cauvery marathon on Oct 19th. Senthil finished the full marathon and Vinu did a 10 km walk. Spurred by this event, other students of Thulir showed interest. Team Asha has been very supportive by hosting the students, providing running accessories and encouragement. Santosh Padmanabhan has been in regular touch with us through phone and email, patiently and diligently providing training schedules and following up progress. His sensitivity to the children's background, his confidence in them, and his thoughtfulness have been  major factors in their accomplishment today.

![”image”](//thulir.files.wordpress.com/2009/02/2a.jpg)


Our children are born athletes and up till now their talents have been neglected and have had no opportunities to flower. Training for and participating in these events have given them a tremendous leap in their self confidence. It has also given them an opportunity to travel and to interact with the outside world. It has long been our wish that Thulir children would get opportunities in sports and we had felt handicapped by our lack of skills and resources in this field. Team Asha and Santosh's  effort  has been a wonderful beginning and we  are looking forward to such possibilities  opening up in future. See [team asha's blog](http://teamashablr.blogspot.com) for more details and pictures.

**Pongal / Kolam Festival Jan 9th -19th**

****
![”image”](//thulir.files.wordpress.com/2009/02/3.jpg)


![”image”](//thulir.files.wordpress.com/2009/02/4.jpg)

Pongal, the harvest festival, and a time for celebrating and thanking nature was a week long affair in  Sittilingi. At Thulir we had our annual Kolam festival. The highlight this year was the increase in the number of boys making  Kolams. Traditionally this has been considered a women's domain. We were invited to visit students' homes at Moola sittilingi, Sittilingi and Nammangadu throughout the week. Being a part of the festivities and interacting with all of them socially was a very refreshing experience.
![”image”](//thulir.files.wordpress.com/2009/02/5.jpg)


**Exposure visit to Timbaktu - Dec 9th to 14th**

****

We have always had warm and fruitful exchanges with the Timbaktu school and children's Centre. In the past students and teachers from there had visited us; students from Timbaktu and Thulir have jointly attended training workshops and executed projects.So it was highly exciting for our older students and staff to finally visit Timbaktu as a group.Since the students of both the places were similar in social backgrounds, skills and faced similar issues, they got along extremely well with each other even without a common language to communicate with. We had workshops where both groups taught and learnt from each other. Our group came back inspired and strengthened by the visit.

![”image”](//thulir.files.wordpress.com/2009/02/6.jpg)


![”image”](//thulir.files.wordpress.com/2009/02/7.jpg)


**Bee Keeping workshop**

Justin from Keystone foundation was here on Dec 19, 20 and conducted an intensive hands on workshop in beekeeping. Since we had about 8 working colonies of bees, Justin could demonstrate all the steps in Bee keeping practically. The workshop was held to introduce bee keeping to newer students and a few interested farmers; and to provide further training to the earlier students.It was a success in both respects.

![”image”](//thulir.files.wordpress.com/2009/02/8.jpg)


![”image”](//thulir.files.wordpress.com/2009/02/9.jpg)


**Music and crafts workshop**.

Thulir walls sported a colourful look after the visit of   Lalitha from Centre for Learning, Banglaore who was here in the first week of December. She did  intensive music and wall painting sessions with the senior students and clay work sessions with the juniors. Being an experienced teacher, she was able to draw out even students who were inhibited in singing and painting. There was enthusiastic participation.We hope she would continue visiting and teaching us.

![”image”](//thulir.files.wordpress.com/2009/02/10.jpg)


![”image”](//thulir.files.wordpress.com/2009/02/11.jpg)


![”image”](//thulir.files.wordpress.com/2009/02/12.jpg)


**Building Construction**

The common Kitchen and Dining hall construction is finally over and we are all heaving a big sigh of relief. The construction while taking significant time and effort, has provided an opportunity for a group of local youth to be trained in alternative construction techniques. We also have added much needed office space and guest accommodation.

![”image”](//thulir.files.wordpress.com/2009/02/13.jpg)


![”image”](//thulir.files.wordpress.com/2009/02/14.jpg)


**Parents meeting**
This year we invited the parents of the older children to come individually or in groups of 2 to 3 persons. We showed them in detail what their children have been doing at Thulir and some of what they have learnt. We spent 2-3 hours with each group, reported on the progress of individual student and discussed with them our plans, their expectations etc. All of them were happy with their children's progress.We hope to do this with the other parents too.

**Priya Nallan's Visit:**

Priya Nallan of Asha Princeton visited us. Though the visit was brief, the interaction was good and we feel encouraged and look forward to a continuing and meanigful relationship with Asha Princeton.

**Visit of Rishi Valley students/ staff:**

Abinanad, Ira,  and Siddharth, students of Rishi Valley school interacted with our students and conducted sessions in sketching, painting and making friendship bands. Another student, Abhay, made keychains.

Mr Dilip Joshi, a teacher from the school amazed us with demonstartions of furniture making using cardboard.

![”image”](//thulir.files.wordpress.com/2009/02/15.jpg)


**Prof Ravindran and Mrs Vanaja Ravindran**

have as always been regular visitors and have been taking classes in language and science. Prof's classes on repair of machines have been of great value and our senior students have grown in their confidence to service faults in our equipment.

**Crafts camp:**

Our evening sessions especially during rainy seasons and winter, are sometimes too short for intensive craft work. Now a days the children have stopped coming on sundays and come on Tuesday evenings instead. So we decided to have two-day camps to get more time for intensive work. We had one crafts camp in September. Around 20 children stayed over during the weekend and made bamboo keychains, friendship bands, greeting cards, paper jewellery etc.

![”image”](//thulir.files.wordpress.com/2009/02/16.jpg)


**Support of Asha Bangalore: **

We are happy that Asha Bangalore has decided to support activities at Thulir. This has come at a crucial time as the newly registered Trust is in its infancy.Several visits have been made by volunteers from this chapter and we have had good interactions with them. We hope this partnership would flourish in the coming years.
![”image”](//thulir.files.wordpress.com/2009/02/17.jpg)


**Health workers' classes:**

The THI hospital has started training a new group of Health workers. Twice a week this this group comes to Thulir for classes and discussions on social and environmental issues.Some of the topics covered include tribal culture, tribal self respect, terrorism, suicides in the community, global warming [causes and effects] etc.

![”image”](//thulir.files.wordpress.com/2009/02/18.jpg)


**Vegetable gardening:**

When vegetable prices skyrocketed in October and November, no vegetables were sold in the weekly vegetable market at Velanur. We had to make do with the little that we were growing. This situation and the subsequent visits to Timbaktu and Auroville [where our students saw good vegetable cultivation], have inspired the group to start organic vegetable cultivation on a serious scale this month. We hope to sustain our enthusiasm and reap the benefits!! This is important as most youth in our villages are reluctant to engage in gardening and farming activities.

![”image”](//thulir.files.wordpress.com/2009/02/19.jpg)

**
**

_As always we look forward to your comments and feedback!!_


***********************************************
