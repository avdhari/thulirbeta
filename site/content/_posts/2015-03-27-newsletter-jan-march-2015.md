---
author: nimda321
date: 2015-03-27 17:46:59+00:00
draft: false
title: Newsletter Jan-March 2015
type: post
url: /wp/2015/03/newsletter-jan-march-2015/
categories:
- Newsletters
---

_Wishing you a happy Pongal and New Year from Sittilingi!_

(This newsletter was written and uploaded by Nikhil just before he left Thulir to pursue his interest in music and higher studies. He assures us that he will be back after a break. He will remain a part of the Thulir team wherever he is and we are eagerly awaiting his return. We wish him All The Very Best in his pursuits!)

**Pongal Celebrations**

As Pongal routine, buildings and floors were cleaned, washed and swabbed with cow dung water to give the entire courtyard a tidy and fresh appearance. In the rest of the village too, dusty brown turned to a dark fragrant green. It was then divided into separate areas in which _kolams_ would be drawn and coloured. Soon it was evening time when children, nurses and a few doctors showed up for the get together and drew colourful _kolams_ on the mud! It was great to see such a highly developed sense of symmetry in the artists as they drew large patterns-growing-outward-from-the-center style of kolams, and achieving perfectly circular figures without the use of any external tools. This years kolams were particularly impressive !

[![](http://www.thulir.org/wp/wp-content/uploads/2015/03/10-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/10-Jan-March-15.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2015/03/12-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/12-Jan-March-15.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2015/03/11-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/11-Jan-March-15.jpg)

**Farming**

The rains passed and the cold was on till mid march and even though the days have grown hotter now, the nights remain equally cold. In Thulir, we got around 9 kilos of Coriander seeds, a bumper harvest of brinjals and tomatoes, chillies and spinach. In fact, we got so many brinjals and tomatoes that it supported 14 of us for two weeks! We ate only brinjal and tomato __ for breakfast, lunch and dinner, and didn't have to buy any vegetables from the shops till the 15th day!


Our rice crop failed though due to a lack of water. We harvested _thattam payaru, _a local lentil as we needed, fresh from the plant and into the cooking pot and so were left with only a kilo of seed for the next planting.
The tapioca is growing along, although a bit slowly, and we can see a difference in the way the leaves have wrinkled up in the plants in Thulir and how they have lushly spread out in the ones outside (which presumably are watered often).

[caption id="attachment_2221" align="aligncenter" width="480"][![](http://www.thulir.org/wp/wp-content/uploads/2015/03/15-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/15-Jan-March-15.jpg) Thattam Payaru sprouting[/caption]

Although things don't look completely dry yet, the hills are also slowly turning brown. It is also that time of year when the bamboos are turning a beautiful orange-yellow and shedding leaves. A couple of bamboos in the village of Moola Sittilingi have flowered and borne seed and there is much excitement to eat the rice – an unexpected gift from the forest with love.

Trees in the campus are flowering and bearing fruits and seeds so all the walkways and the campus in general are full of seeds and dry leaves. These are good for the children to collect and keep safely, to see later at a time when there will be no seeds and remember the season of fruiting for that tree – A lesson in local ecology.

[caption id="attachment_2224" align="aligncenter" width="480"][![](http://www.thulir.org/wp/wp-content/uploads/2015/03/18-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/18-Jan-March-15.jpg) A seeding Bamboo![/caption]

[![](http://www.thulir.org/wp/wp-content/uploads/2015/03/17-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/17-Jan-March-15.jpg)


### **Auroville Marathon**


Thirteen people from Thulir, 9 young boys and 4 teachers went to Auroville in early Februrary to be a part of the Auroville Marathon. Children from many schools across India had come. Our boys, aged from 10-14, ran the 10 km run, and the teachers ran the 21km run, and seem to have had enjoyed themselves. Some of them got to see the beach for the first time! There was activity time before the marathon where all the participants from all schools worked together on art projects.

[![](http://www.thulir.org/wp/wp-content/uploads/2015/03/20-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/20-Jan-March-15.jpg)

[caption id="attachment_2217" align="aligncenter" width="480"][![](http://www.thulir.org/wp/wp-content/uploads/2015/03/13-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/13-Jan-March-15.jpg) Activity time - Working in groups[/caption]

**New School**

Discussions on how the school should be still continue. In the last months we have talked to the hospital team, the farmers group and the womens group. Smaller working committee meetings are also going on. The land for the school has still not come through. After much debate and discussion, it has been decided that the new school will temporarily start functioning in Professor's house from June with 20 children. Work on finalising and buying the land, building all the buildings, getting government recognition, will continue simultaneously. The minimum age limit has been decided to be 4 years.

The curriculum will give importance to tribal culture, agriculture and the forest that surrounds Sittilingi. Working on specific themes, like clothing or water or food will give us many opportunities to talk about the processes behind the themes, and how it came to be the way it is. For example, if the theme is growing plants, then the children can learn about the biology of the plant, good soil and climate, seasons, geography of its spread, measuring plant growth, garden patch size, plot growth graphs, measure weight and volume of produce, understand nutrients provided by the vegetable/fruit, organic vs. inorganic ways of growing, writing poems and stories about plants, drawing them, sculpting them, etc. The limit to what can be taught with a central theme in mind is only limitied by the imagination and resourcefulness of the teacher. Therefore, development of the teacher's interests and knowledge base becomes important.

[caption id="attachment_2233" align="aligncenter" width="480"][![](http://www.thulir.org/wp/wp-content/uploads/2015/03/DSC_1047.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/DSC_1047.jpg) Temporary School[/caption]

Exposure to different learning situations we hope will enthuse and inspire our  teachers to think differently about teaching, learning and living itself. So we travelled to Sita school in Bangalore by jeep and to Anand Niketan school in Sevagram( Gandhi ashram at Wardha, Maharashtra) by train. 

**Train Travel**

[caption id="attachment_2212" align="aligncenter" width="480"][![](http://www.thulir.org/wp/wp-content/uploads/2015/03/8-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/8-Jan-March-15.jpg) Had the bay to ourselves mostly. Story Telling![/caption]

The travel itself was a great learning. It was the first time many of us travelled by train. In it were people from Bihar, Maharashtra, Assam, Kerala, Andhra and Tamil Nadu, both healthy and diseased, rich and poor, all packed together in some places and spread out comfortably in others. The views from our windows told us what was being grown at each place and topography, and as all our teachers are from an agricultural background, it was all very interesting for them to see - how crops were being grown in different parts of the country, where there was plenty of water, where the land was rocky and undulating, etc. A visit to the pantry came as a rude shock to some teachers, and after that, they weren't very keen on eating anything prepared there. Some were surprised that all the cooks there were men. __

**Anand Niketan School, Sevagram**

[![](http://www.thulir.org/wp/wp-content/uploads/2015/03/1Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/1Jan-March-15.jpg)

 From the railway station, the 11 of us ( and the 4 babies !) caught 3 auto rickshaws to Sevagram, what used to be the residence of Gandhi in pre independence times. It was early evening when we arrived. There were a lot of bustling khadi clad figures moving about the place. We found out that there was another meeting being held there near our guest house. Medha Patkar, Subhash Palekar and Anna Hazare were a few among them as we later found out. (they were visiting to rally people for a _pad yatra_ from Sevagram to Delhi _**) **_The idea of a_ pad yatra_ excited our group and they got thinking about how people could journey like that, without money or any necessities. In the evening was a short daily prayer out in the open by the residents of the ashram, after which was dinner and then bed.

The next day, we went to the school, situated within the premises, but in a different area. The school runs on Gandhi's philosophy of education called “Nai Talim” which in Hindi means “New Education” or “Basic Education”. It focuses on teaching children the basic aspects of living, which are food , clothing and shelter and involving them in the actual basic work of the community. The original Nai Talim school was started in 1938 and functioned till 1965. The present school was restarted in 2005 by Sushama Sharma and her friends in the Nai Talim Samiti. 

[caption id="" align="aligncenter" width="480"][![](http://www.thulir.org/wp/wp-content/uploads/2015/03/7-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/7-Jan-March-15.jpg) Learning to make a mat out of coconut fibre rope[/caption]

There, in the school, we met Guruji, the tabla teacher, a gentle old man who had studied in the Nai talim school before it was shut down in 1965. He told us that in his time, children would wake up at 4.30 in the morning and go off to their respective duties, like milking the cows or helping out in the kitchen with the cooking or gardening work or general cleaning. The groups would rotate so that each group would get to do all the activities through the week. After an early breakfast, the children would clean the campus till lunch time around mid day. He specifically emphasized the time they spent sweeping and cleaning. After lunch, children would have academics where the lessons would be based on the activities that they did in the morning. For example, if a child had helped cutting vegetables in the kitchen in the morning, then the lesson would invariably cover basic mathematics, science, diet, geography, history, economics of cultivation, basic literacy and language just to name a few. After being brought up in the Nai Talim tradition, it was Guruji's opinion that this way of teaching children was much more effective than studying the same things out of a textbook. 

[caption id="attachment_2237" align="aligncenter" width="480"][![](http://www.thulir.org/wp/wp-content/uploads/2015/03/21-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/21-Jan-March-15.jpg) Walking around Sevagram[/caption]

All children were involved in all activities in the ashram, whether it be agriculture or spirituality or thinking about the direction that society is moving in and coming up with ways to bring about change. All children would also spin the _charkha, _the simple device that brought down the rule of the British and the East India Company. This process of spinning thread from cotton using the c_harkha_ was very meditative and required concentration and skill to get a strong and evenly thick(thin) thread. Once we did it, we understood what a great activity it would be for all children to really apply themselves to, to develop power of concentration and patience.

[caption id="attachment_2206" align="aligncenter" width="270"][![](http://www.thulir.org/wp/wp-content/uploads/2015/03/2-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/2-Jan-March-15.jpg) The Charkha![/caption]

[caption id="attachment_2242" align="aligncenter" width="480"][![](http://www.thulir.org/wp/wp-content/uploads/2015/03/24-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/24-Jan-March-15.jpg) Preparing the cotton before it can be used in the Charkha[/caption]

Presently, the school doesn't function the way it used to in Gandhian times. There is less time spent working and more time devoted to academics, but still enough time is found in the day to learn dance, music, craft, gardening, embroidery, cooking, vegetable cutting, cleaning and spinning the charkha. Even though we got only two full days at the school, we feel we got to see a lot of things and came back inspired and understood the place of work in elementary education.

**Visit to Sita School in Silvepura, Rural Bangalore**

This is a school that Thulir shares a close relationship with. We've had more than a few exchange programs for students but this time, the entire teacher team visited the school. Along with the mothers came their babies, happily dressed in colourful clothes but constantly soiling them without warning. We went together from Thulir in a Tata Sumo, the owner of which is a man from Sittilingi who had never previously been to Bangalore. Apart from the confusions with the routes and experience of being completely smoked in in rush hour traffic and minor incidents with the traffic police, it was a smooth ride to the school, jolly and excited as the whole group was. We were glad to reach though, after long last, and to be welcomed by the smiling figure of Jane Sahi. 

[caption id="attachment_2207" align="aligncenter" width="480"][![](http://www.thulir.org/wp/wp-content/uploads/2015/03/3-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/3-Jan-March-15.jpg) Mother and son[/caption]

The babies, we noticed, were giving the mothers a lot of trouble (as usual) and made it impossible for the latter to concentrate on anything else so we arranged for a baby sitter to distract them and take care while the mothers were gone – Me!  The mothers were relieved at having “a day off” (after a year!) and went around the school to observe its functioning.

[caption id="attachment_2208" align="aligncenter" width="480"][![](http://www.thulir.org/wp/wp-content/uploads/2015/03/4-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/4-Jan-March-15.jpg) Learning Materials in one of the classrooms in Sita School[/caption]

[![](http://www.thulir.org/wp/wp-content/uploads/2015/03/5-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/5-Jan-March-15.jpg)

The teachers were particulalry impressed with Jyoti Sahi's oil paintings and other abstract art works that were put up all over the campus on large pieces of canvas. Sasikala remarked, “Each one has at least a hundred different stories! We can imagine whatever we like by looking at the painting, and all of it could be true!” Even our driver Theerthan absorbed a bit of the philosophy of the school with respect to raising healthy, capable children, and was pleasantly surprised to find that kids here swept, washed vessels, arranged their own classrooms in order and did other chores without the least bit of supervision from the teachers. Even while eating our meals, he was surprised to see that  boys helped with the kitchen work, and even men can help with the arranging and the cleaning up of the table, and that more importantly, there was no shame in doing so. What inspired the teachers most was the excellent quality of education and care given to the children there while using very simple resources and infrastructure and the teachers ( especially Jane) being gentle and soft spoken!


[caption id="attachment_2210" align="aligncenter" width="480"][![](http://www.thulir.org/wp/wp-content/uploads/2015/03/6-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/6-Jan-March-15.jpg) A different pattern arranged by children every morning before prayer. This one uses Mahogany pods and flowers - all materials for this are taken from in the campus.[/caption]

We left early in the morning from Sita School so that we could reach Lalbagh Botanical Garden before the morning traffic began. Fresh from a good dinner and a good night's sleep, a morning walk in cloudy Lalbagh was very enjoyable. The large trees and massive honey combs, far above our reach, swayed gently in the cool breeze, and brought us a scent of flowers from somewhere near. We then headed to MTR for a breakfast of _bisi bele bath _and _rava idly. _The babies were fascinated by the traffic, and much to our dismay, so was Lakshmi, and that too while crossing the road! We had crossed to the other side, and realized that we had left Lakshmi behind, who was just coolly crossing Double Road with peak hour traffic as if it was the road outside her house in the village. But we still managed to travel back to Thulir in one piece, and even enjoyed the gift of rain on the way, after hot weather. The anxious mothers were much easier within themselves to be, after what seemed like a long journey, back home.

[caption id="attachment_2218" align="aligncenter" width="480"][![](http://www.thulir.org/wp/wp-content/uploads/2015/03/14-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/14-Jan-March-15.jpg) Lalbagh[/caption]

_What we saw in the few alternative schools that we visited were curriculums designed keeping in mind not only scientific aspects of children's learning and failure, but also ways to promote equality, justice and a sense of inclusiveness among other things. All of which are  ignored in the conventional curriculum followed by most mainstream schools._

**Construction and Moving in**

There has been a bit of construction going on here in the Thulir campus. The building {Professor Ravindran and Vanajakka's house}that is to be used as the temporary school for the next two years or so (till the land comes through and construction is done) needed some changes to its structure before it could be used. Some sunshades were installed in place to keep  the rains out, and a new sheet roof was put to cover the central courtyard of the building. 
The books and other things from the shelves were all first cleaned up. All books except for children up to 8 years of age and some reference books for the teachers were packed up in cardboard cartons and kept aside. The remaining books, puzzles and play things were arranged in the shelves and moved to the new classrooms.

The present Thulir classrooms are being converted into guest rooms. This conversion requires some modifications of the existing structure, and additions like a new battery room and tool shed. The roof of the existing tools shed and the library had to be changed, the thatch had lived its life and was beginning to leak in places. Dried sugarcane leaves were used as the roofing.

[caption id="attachment_2232" align="aligncenter" width="480"][![](http://www.thulir.org/wp/wp-content/uploads/2015/03/DSC_1029.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/DSC_1029.jpg) Leaky roof fixed with sugarcane leaves[/caption]

The hill grass that was previously used to roof the main Thulir building and library is now getting increasingly rare and expensive to use. Even the men skilled enough to use it are reducing in numbers, and soon, it may be go out of circulation as a common roofing solution for hot climates. The thatch is a bad conductor of heat, and hence doesn't radiate outside heat into the house during the day while during cold nights, doesn't radiate the heat from inside to out or the cold from outside to in, keeping the house at a moderate temperature at all times.It is an ideal roof for our country. 


###  **Evening Classes**


With all of this going on, we are also busy learning and preparing  to be full time teachers at the new school. The learning materials are being prepared by us during the day, and when added to all the regular responsibilities of taking care of the accounts and the campus in general gives us very little time to prepare for classes in the evening and hence, we have decided to stop evening classes for children for the time being. 

The number of  children attending too has reduced gradually. The children at the government school here are being kept for longer hours at school.The children of class 9 and 10, especially, are in school from 7am to 6 pm preparing for and writing revision exams. So they are hardly able to come to Thulir. For the other classes too there is more emphasis on rote learning and writing tests than on understanding the lessons and actually learning. So the children are exhausted and drained by the time they come to us in the evenings. This last year when they were in Thulir in the evenings, it would be clearly visible that they are not interested in any more learning and we would find it hard to do any kind of activity with them. They would just want to play in the evenings together, and attempting anything else suddenly became very difficult.


Just the other day, i met a few boys who used to come to Thulir previously but stopped as they moved into the tenth. The boys who used to be so spirited and energetic seemed dull and defeated, and on asking how being in tenth was treating them, answered sadly  with the usual sense of disillusionment that all the tenth graders here exhibit. They wanted me to teach them English. I gladly invited them to come but wondered silently when they would find the time or energy to learn, living the way they were. 
Now with the exams going on, and their parents insisting they score high marks and literally locking them up indoors to study, the least we can do is wish them well and hope that the school that we are starting will prevent or at least reduce the damage to children in this little village...

******Sports Day**

_A Sports day was suddenly organised on the 29th of this month, as a sort of farewell to me! Though the evening classes had been stopped, when we sent word to them about this event, children promptly came. It was sweltering heat but the children were enthusiastically running and playing and refused to go home after it was over. We had to have an indoor session of  viewing photographs and films after that!_

**Kutty Thulir**

****_****_The children in Kutty Thulir are growing up really fast, and we often forget how magical the process is. Here's hoping to a great time ahead for these children and us.


[![](http://www.thulir.org/wp/wp-content/uploads/2015/03/rsz_1img_20141127_121928987.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/rsz_1img_20141127_121928987.jpg)

_**Congratulations are in order **_to our friend and colleague Ravi and his wife Ambika who have a baby boy now, 2 months old. We wish them all the best for parenthood and beyond!

[![](http://www.thulir.org/wp/wp-content/uploads/2015/03/16-Jan-March-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/03/16-Jan-March-15.jpg)

**
**


