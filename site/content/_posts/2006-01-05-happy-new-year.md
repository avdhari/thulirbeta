---
author: thulir
date: 2006-01-05 11:28:00+00:00
draft: false
title: Happy New Year!
type: post
url: /wp/2006/01/happy-new-year/
categories:
- Newsletters
---

[![](http://photos1.blogger.com/blogger/8103/1906/320/x1.1.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/x1.1.jpg)Naikutti children in Thulir


This is the first posting of this year. We Wish you all a very Happy New Year ahead.


From 24th December, the Christmas holidays for the local Government. school
began. So the children were free during the day and we thought it would be
good to have them come during the day to Thulir, so that we can have extended
period of activities.




[![](http://photos1.blogger.com/blogger/8103/1906/320/x2.0.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/x2.0.jpg)




The children were happy about this [there is a consistent demand from
Sittilingi children that we open during the mornings during the holidays].
The children from Moola Sittilingi decided to stay at home and not come
during the holidays. Many of them, stayed back to help their families with
the harvest. But there was an unexpected request. A couple of children from the village Naikutti [6 km away, from where our trainee Anbu comes], expressed interest in coming to Thulir during these holidays. We were glad that Thulir could be used by even children who are not in the immediate vicinity.




From a couple of boys in the first day, it went on to become a group of nine
boys by the end of the week! Most of these boys study in Government boarding
schools away from home, and were home for holidays. A couple of them had
bicycles to cover the 6 km distance, but the rest actually walked all the way
everyday! A couple of them go to the Sittilingi school but don't find time
in the evenings to come to Thulir as they live far away.




[![](http://photos1.blogger.com/blogger/8103/1906/320/x2a.0.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/x2a.0.jpg)




The activities during this week ranged from paper folding, to drawing and painting, to watching a video on bacteria. On the day of the video show, we also had Madaiyan [who is from this area, got trained as a lab technician and works in the tribal hospital. Madaiyan brought a microscope and some slides
of blood cells and also TB bacteria from the hospital. We also made a slide
of onion peel to look at plant cells.




We finally have started the cooking of diet supplement for the children
starting last month. It is a simple Ragi porridge or boiled channa right now.




This is the time of the year when Thulir usually has a lot of visitors. This
year the following visitors have come to Thulir.




Ramasubramanian:




an old friend and a mechanical engineer who specialises in design, manufacture
and erection of micro-hydro power plants, he shared his experiences of work
with the children. He also enjoys origami and taught the children some paper
folding.




[![](http://photos1.blogger.com/blogger/8103/1906/320/x2b.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/x2b.jpg)Music teacher, Udaya Banu:




He teaches music at the tribal school in Gudalur Nilgiris,and is currently
visiting us as we write this. So there is much singing, laughter and fun in Thulir!!




Carolyn and Patti:




Friends of Thulir from UK, who come every winter to Sittilingi ,are back in
Sittilingi. Carolyn has brought some more learning materials from UK. Some of
the materials she had brought last time proved very useful [like the
interlocking cubes, and triangular dominoes for math learning]. Patti, you
may remember, last year had introduced clay work to the children. There is
much excitement in Thulir in anticipation of more Clay work and firing of
the oven.[![](http://photos1.blogger.com/blogger/8103/1906/320/x4b.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/x4b.jpg)
