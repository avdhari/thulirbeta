---
author: thulir
date: 2006-03-04 08:21:00+00:00
draft: false
title: News II March
type: post
url: /wp/2006/03/news-ii-march/
categories:
- Newsletters
---

Senthil and Vediappan are in Timbaktu!  
  
[![](http://photos1.blogger.com/blogger/8103/1906/320/SenVedTtu.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/SenVedTtu.jpg)  
[![](http://photos1.blogger.com/blogger/8103/1906/320/centreT%27tu.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/centreT%27tu.jpg)  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
From Thali [where the Thulir team went for an exposure visit last week---see previous post below for more on it!], Vediappan and Senthil along with Krishna proceeded towards "Timbaktu Collective" in Andhra Pradesh. We stopped at Vijay and Gracy's farm in Thali for the night. It was a nice opportunity to visit our friends whose Sittilingi visit and interaction with Thulir children are still fresh in our minds. Timbaktu has an interesting school and children's centre, which agreed to host Senthil and Vediappan for a one month stint. Subba Raju who is instrumental in the running of the school has been talking about giving opportunities for children to use their hands in school. This vision has taken the form of a Children's centre where a well equipped library and workshop staffed with sympathetic instructors are available for children to explore their creativity. Senthil and Vediappan took to the place as soon as we reached and by the middle of the third day when it was time for Krishna to leave them, had settled down nicely inspite of a language problem. As this blog is being published the news is that they are happy and keeping themselves busy learning and working in the centre.
