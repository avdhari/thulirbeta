---
author: thulir
date: 2006-02-28 05:42:00+00:00
draft: false
title: 'News I Feb '
type: post
url: /wp/2006/02/news-i-feb/
categories:
- Newsletters
---

[![](http://photos1.blogger.com/blogger/8103/1906/320/dscn0087.0.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/dscn0087.0.jpg)  
  


Theatre workshop  


  
Dr. Velu Saravanan, a theatre professional who specialises on working with children had visited Thulir on a weekend and conducted a theatre workshop. An accomplished professional, he quickly got the children into the act and in 24hrs managed to teach them a play and rehearse sufficiently to put up a performance for the Sittilingi parents and friends from the hospital. His friend, Mr Gambhiran who specialises in writing stories for children, accompanied him and regaled us all with a story written by him. He helped three of our junior children put up a skit that had us all in splits!  
  
  
  
[![](http://photos1.blogger.com/blogger/8103/1906/320/dscn0068.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/dscn0068.jpg)  


[![](http://photos1.blogger.com/blogger/8103/1906/320/dscn0112.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/dscn0112.jpg)  


[![](http://photos1.blogger.com/blogger/8103/1906/320/dscn0163.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/dscn0163.jpg)
