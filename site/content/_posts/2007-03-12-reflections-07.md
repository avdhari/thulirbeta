---
author: thulir
date: 2007-03-12 03:22:00+00:00
draft: false
title: Reflections --2007
type: post
url: /wp/2007/03/reflections-07/
categories:
- Reflections
---

  
 Its almost April and it is anniversary time. As usual, it is time for reflection. Of course we just put up a news update that talked in detail about the activities at Thulir, so this note is more to summarise it and add how we see ourselves currently and where we are headed to.  
  
 Well, as things stand now, these are the various groups and set of activities currently taking place at Thulir:  
 

<blockquote>    * **Evening Sessions **are being attended by about 55 children [even as we write this the past two weeks have seen fresh children coming in. But on an average we have about 25 to 30 children attending. We have now arranged them into 3 groups [age and ability wise]. We have introduced more ordered exercises after feed back from the students. Apart from this we have the usual activities of basic language, Math , General knowledge, art and crafts and General topics, viewing documentaries and feature films.We have recently started computer classes .We are also seriously thinking of starting a sort of film club with regular screenings followed by discussions as we realize that a lot of their current thinking is deeply influenced by mainstream Tamil movies and therefore it is important to make them aware of this impact and to show alternatives. _Apart form this activity, we are currently working on how we could organize the academic learning part more effectively. This is a big challenge and source of many of our frustrations currently, as the students are not very regular and come for a short period of time everyday._ </blockquote>

   
 

<blockquote>    * **The Basic Technology course **students are continuing to do their mix of academics with technology skills.  Some of their sessions are invoking interest from the other students and we are considering doing some of the skills activities with the evening session students too. We are truly amazed at the positive benefits  working with the hands has on their self confidence levels and there is a  subsequent drastic improvement in academic skills. _Ideally this should be available to all students and this might become the biggest challenge in the coming years for us. In a small way we have started this process for the evening class students and hope to work forward slowly. _Perumal, Senthil and Balu [along with Mohan who should be joining us back after he recovers from Surgery] being our first batch, need more time than the one year that would end by June. We feel they need a bit more of "hand holding" time, before they are ready to move on to either taking up a job or going for higher level skill training. So we are postponing  taking in a new batch to the end of this year. </blockquote>

   
 

<blockquote>    * **Sreyarth and Bharathi** continue with their "home" learning using Thulir resources. Twice a week, Ragini who is 6 years old and whose parents work at our hospital , also joins them. This group in the coming years may grow, as we have other parents in the hospital whose children are in the 0-5 age group. </blockquote>

   
 

<blockquote>    * **The exam batch : **A group of 5 students are currently preparing for their 10th boards, They are here full time and apart from organizing their preparation we have introduced some general discussions classes for them. _Some of these students could continue to come to Thulir even after the exams and we are looking closely now at what sort of a programme might benefit them_. </blockquote>

   
  
 It is obvious  we dont have a conventional structure [say like that of a school with classes], and things look a bit too varied/ unorganised/ hap hazard. In fact part of our frustration is not being able to look at it  with some sense of order, and especially in not being able to communicate this. It certainly seems we dont have a "target group" or "focus".  
  
  It is however getting clearer that what we __are__, is a Learning Resource Centre that is open to a wide age group. The way this is working out now is that _Individuals  _are either ,at the one end, getting motivated to learn and/or on the other , actually learning skills [both academic and vocational]. We wonder if it could be in some sense like students going to a University  [in US?] and choosing her/ his  credits and pace of completing the credits. For this to happen properly two factors are crucial :  
    * 1]. the ability of  students to decide what they want to learn and when; and  * 2] that we are available with time/ energy and the right resources for them to be able to do this. In a sense this is what we are doing and with this clarity we may be able to a] evaluate ourselves better and b] perform better in future.    
  
 One of the important learnings from the Basic Technology Course is also that in a group, not everyone is ready to undergo a shared experience of learning and the specifics of what is taught/ to be learnt may not suit some individuals [which explains our drop outs from the course]. It would of course be ideal to open up the place for "any day admission". [In a sense we are that right now, except for the Basic Technology Course where we had a formal date of starting]. It is not too radical a thought as the National Open School now offers the possibility to write exams on demand [one can walk into any of its Centres on any working day, pay the fees and take an exam!!].  
  
  Some questions that arise are:  
    *     

##### Can we leave things to individual students to come up and say " this is what I want to learn now"?

     Yes, we can. this is beacuse for one, most students come to Thulir on their own: they are not compelled by parents to come. Secondly, after 3 years, we now have a core group of students who are motivated and therefore have created an atmosphere where by newer students get motivated much sooner.  
    *     

##### So once we accept this what about organizing learning for so many different students at different times/ different paces.? How is this to be done?

     This has been our big challenge so far and it hasnt been easy. But given our peculiar situation of an institution where students do not come regularly, _there is no other way to do it. _It doesnot mean that we cant have classes for a group. Where it is appropriate, this does happen. Given our scarcity of teachers, we need to encourage students themselves to teach each other. This will lead to higher confidence levels as explaining a concept to another person really clarifies ones own understanding.  
    *     

##### How do we evaluate our performance / students performance?

     The probable way to do this is to have individual portfolios where all the work of the children and occasional tests that they voluntarily take are all filed. Along with the dates and their attendence records this would make it possible to have a fairly clear idea of how a particular student has progressed. These records would also give a pointer towards our efforts in organizing materials as well as teaching sessions that were involved with each individual. Apart  from this, we could also include our notes on sessions we take for the children and notes on activities that happen here. This does seem a lot of work, but some of it we already are doing and so it requires the additional effort of streamlining this process.  
  
 Of course conventional pointers are always available...such as number of children passing 10th Std exams and so on.We also intend exploring other alternatives to the private candidate in the state exam mode that is currently being used. These could be various possibilities under the National Institute Open Schooling exams with certificate exams in various vocational streams, apart from the 10th equivalent academic/vocational 10th exams.  
  
  
 We would be very happy to hear from you what you all think and any suggestions/ comments would be very valuable. We look forward to hearing from you all.  
  
******************************
