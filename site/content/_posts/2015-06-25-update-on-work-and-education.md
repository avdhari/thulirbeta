---
author: nimda321
date: 2015-06-25 07:17:31+00:00
draft: false
title: update on "work and education"
type: post
url: /wp/2015/06/update-on-work-and-education/
categories:
- Newsletters
- Reflections
---

[![](http://www.thulir.org/wp/wp-content/uploads/2015/06/MG_9434.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/MG_9434.jpg)

As you may be aware from our previous posts, the theme of work in education has played an important role in Thulir's activities. This post will attempt to update our activities in specific relationship to this theme and our current thinking.

Anu and Krishna were recently asked to write on this theme for Learning Curve, a magazine brought out by Azim Premji Foundation. You can access the full article at [Learning curve article. ](http://www.thulir.org/wp/wp-content/uploads/2015/06/A-K-Learning-curve-article-.pdf)

The post school programme that Thulir ran for many years [variously referred in our News letters and website as Basic Technology course/ Life skills course etc.] was a unique opportunity for us to explore the role of work in education. The idea of hands on work as part of the learning process at Thulir was introduced mainly as a way to bring in useful vocational skills. Young people [of the age group 14 to 20], we felt, would benefit by being able to acquire skills that might come in handy to make a living in the village/ local area.

Over the years, as we wanted to tailor learning to suit individuals and specific groups, we experimented with different mix of skills. So some years the emphasis was on construction skills [masonry, plumbing, electrical wiring etc.], while on other years it was on electronics, and bamboo crafts; and on still other year teaching preschool children, crafts, soap making etc. On one particular year we even tried tutoring and preparing the group only for appearing for the 10th public exam, without any hands on work. Incidentally, this was a disaster and the students did very badly, and we quickly discontinued it. In contrast, the hands on work based programme seemed to give confidence to the students to tackle academic exams which they could not earlier. Many decided to continue higher studies enrolling in class 11 in schools outside of our valley, some even at the age of 19 and 20! Some have gone on to Colleges for degree courses.

[caption id="attachment_2270" align="aligncenter" width="400"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1000426.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1000426.jpg) Fixing a solar water heater, designed and fabricated at TTI[/caption]

During the past 2 years, we stopped the organized course at Thulir for the post schoolers. This was due to many reasons. One reason was that, parental expectations and the student peer thinking  had changed and there was a drop in number of students seeking to join the course. This is due to the perception that academic qualification is more superior to acquiring vocational related education. The aggressive canvassing for students by the numerous new private colleges around the region is a major influence.

There was also a fatigue for the teachers/ instructors after years of running the course. Changing curriculum on a yearly basis with each group and constantly finding appropriate real life work situations for their projects, and to guide the students through these or find suitable resource people, was tiring work.

The second reason was that we started a small preschool,  mainly due to the continuous pressure on Thulir to start a full time school. We also started thinking / working seriously with a group of parents/ locals who showed interest in starting a school. The idea being that a community owned school would be a more sustainable institution in the long run.

The third reason being that there was a need for the training of building artisans, as the THI hospital in Sittilingi is expanding. This meant more attention was needed for on site training. Also some of the students who finished their post school stint at Thulir were keen on pursuing training to become full time building artisans.


### Training of building artisans in Sittilingi.


Murugan, a young person from Sittilingi had come to us 12 years ago and started working at construction site as helper to masons. He is a good worker and a keen learner and soon we started training him in masonry work. Over the years, he has grown in confidence and after doing several buildings in and around Sittilingi with other masons, was able to become a senior artisan and build a small team of Adivasi youngsters from this area. Over the past 2 years, this group has been able to take on full responsibility for the construction of the new Operating theater building, the Women's dormitory building and is currently building the new Ward building, all for the THI hospital in Sittilingi. This team, given the training has been able to produce high quality  and the buildings are much appreciated by various visitors to Sittilingi.

[caption id="attachment_2266" align="aligncenter" width="400"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/IMG_20150102_124113582_HDR.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/IMG_20150102_124113582_HDR.jpg) Murugan building the New Ward building at the Hospital[/caption]

While this group was being trained, Jayabal and Dhanabal, ex students of Thulir, also expressed interest in similar skills and joined the team. They started with masonry pointing work and in prefab concrete work at the women's dormitory building and were able to produce work of good quality. They were given small masonry work around the building which gave them an opportunity to learn more. Currently their confidence levels are good and are negotiating to take on an independent project  from THI which they plan to build with the help of a couple of more youngsters from their village [also ex Thulir students]. This would be quite a challenging project for them and they will need further on site training/ hand holding that we plan to provide.

[caption id="attachment_2263" align="aligncenter" width="400"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/IMG_20140801_121345186.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/IMG_20140801_121345186.jpg) Jayabal learning to do pointing at the Women's Dormitory building[/caption]

The Tribal Technology Initiative,  a project started in Sittilingi independently has a steel fabrication workshop and has a small team taking on projects in and outside Sittilingi. They have done steel fabrication work fore the Hospital buildings mentioned above. A few of Thulir's ex students have joined and worked with TTI in different projects.

Perumal, our ex student and staff, is currently going to college. He also does part time projects for TTI, helping with electrical wiring, solar PV system installations, micro hydro power project installations, and electronics projects such as white LED light fixtures assembly. He has become a well sought after technician in and around Sittilingi, doing among other things, servicing of electronics gadgets at the Hospital, and doing maintenance of street lights in the Sittilingi Panchayat.

The building projects in Sittilingi have used various alternative technology ideas. We hope to update on the details in a separate post soon!

[caption id="attachment_2271" align="aligncenter" width="400"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1000427.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1000427.jpg) Perumal fixing new Solar PV panels at Thulir[/caption]


### Collaborations outside Sittilingi:


Meanwhile, Gramavidya an NGO started by Prof Jagadish and actively run by Dr Yogananda and his group from Bangalore requested that the Thulir experience be shared with their training programmes conducted on alternative building technologies. These are 3 day programmes usually held 3-4 times a year.

Architecture / civil engineering Interns have been joining Gramavidya and associated groups for training. So guiding / mentoring is another area of involvement. The interesting outcome has been one of infusing alternative ideas in education [especially the theme of combining hands on activities with more formal/ academic methods] into Gramavidya's activities. At the same time, this relationship has strengthened building skills training and the quality of construction projects in Sittilingi.

[caption id="attachment_2264" align="aligncenter" width="169"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/IMG_20140801_121352309.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/IMG_20140801_121352309.jpg) Dhanabal learning to do pointing at Women's Dormitory building[/caption]


### Current situation/ thought for the future:


We see that there are now  push outs from even Govt Schools, in the race to achieving 100% pass results in 10th board exams. So while there was a lull in children dropping out at 14 from the formal system, it seems to be reemerging. We do have a couple of such students in Thulir now, and so there might be a need to restart the Post school programme in an organized way in the coming years. At the same time, many of our ex students are now in the process of pursuing different vocations and start offering their services for the local community. The challenge in coming years would be to enable both these groups and to make linkages so that each activity benefits the other. We also realize that this needs more energy and people with different skills to guide, and hope the Thulir team will grow to face these new challenges.

[caption id="attachment_2277" align="aligncenter" width="400"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/IMG_20150620_124741360.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/IMG_20150620_124741360.jpg) Jayabal and Dhanabal currently building a dry composting toilet for Thulir.[/caption]

[caption id="attachment_2267" align="aligncenter" width="400"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/IMG_20150313_073212604_HDR.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/IMG_20150313_073212604_HDR.jpg) Women's Dorm., THI Sittilingi[/caption]


 *************
