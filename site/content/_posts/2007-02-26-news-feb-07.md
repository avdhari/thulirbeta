---
author: thulir
date: 2007-02-26 14:49:00+00:00
draft: false
title: Newsletter Feb 2007
type: post
url: /wp/2007/02/news-feb-07/
categories:
- Newsletters
---

[![](http://bp1.blogger.com/_qz2j5uas_y0/ReL8KODB5jI/AAAAAAAAABM/UsIR_xPvV68/s320/Thulir%2520Feb%2520Blog%2520Draft_html_m44ac9d2c.jpg)
](http://bp1.blogger.com/_qz2j5uas_y0/ReL8KODB5jI/AAAAAAAAABM/UsIR_xPvV68/s1600-h/Thulir%2520Feb%2520Blog%2520Draft_html_m44ac9d2c.jpg)Its been a long gap since our last post. The reasons for this are many fold. Our internet connectivity has taken a nosedive in terms of quality and we are unable now to send anything more than simple text messages, if at all we are able to connect. The Basic Technology course has taken a lot more of our time and energy than we had imagined. This combined with the preparations for the 10th class exams and our regular Resource Centre activities took a toll on our energy levels and we started feeling frustrated. The low morale led to less communication with the outside world!  
  
The course has been going through many exciting times as well as some frustrations. Two of the students left for Thiruppur to take jobs. One of these two students had to leave due to family problems which made it impossible for him to continue living in Sittilingi. The other left suddenly without giving us reasons and so we are not very sure of why he left. A third student has stopped as he has to undergo a major heart surgery, and is quite unwell right now. A fourth is also on sick leave as he has been frequently falling sick. These students' difficulties while they were on the roll had a negative effect on the group as a whole.  
  
One of our staff, Anbu , left in November. He was commuting from a village 6 kms away and found it difficult to continue. Over the last one and half years , with Ravi's help we had slowly trained him to handle accounts and other Admin work and he had just started doing a good job when we lost him. Our workload increased. All this resulted in our feeling frustrated and affected the general morale in Thulir. It has taken a series of efforts, from taking a break and trekking in the Himalayas with the family [something we have been planning for years], to taking the Thulir children in various groups on study tours , to bring back our optimism.  
  
We are currently reviewing Thulir activities to chart the course for the coming year. It started as a review with just the two of us as we were not very happy with the way things were, wondering if we ought to be making changes. We were looking for more tangible and faster results. We had more questions than we had answers; we were looking at all the aspects that were unsatisfactory and could be improved. But this soon lead to a feeling of frustration and a bit of pessimism. Luckily, we were able to interact with friends who are seniors [and with many decades of experience] in the field of alternative education and this helped us hugely.  
  
We were able to look at things in a more objective and positive manner. Subsequently we talked to the children, putting forward questions that we had in our minds and asked them to reflect with us and give us a detailed feedback. This was something that had been in the back of our minds but had not been initiated. We called it a " Test for Thulir ". It was gratifying to see the children take the whole process very seriously and put in a lot of effort for a couple of days . Even the children who couldn't write well wrote whatever they could. Right now we are in the middle of this process and will report on it in detail soon. But what has come forward till now has been very very useful and positive and makes us feel hopeful about Thulir's future.  
  
Update on the Basic Technology Course:[![](http://bp2.blogger.com/_qz2j5uas_y0/ReL_yeDB5kI/AAAAAAAAABY/bbF6lXIM85s/s320/Thulir%2520Feb%2520Blog%2520Draft_html_903ac40.jpg)
](http://bp2.blogger.com/_qz2j5uas_y0/ReL_yeDB5kI/AAAAAAAAABY/bbF6lXIM85s/s1600-h/Thulir%2520Feb%2520Blog%2520Draft_html_903ac40.jpg)  
The dropping out of two boys and the illness of two others caused some lowering of the morale of the remaining boys. But they have steadily been learning and doing various things..  
  
Meanwhile, Mohan's health has deteriorated . He has to have a double valve replacement heart surgery immediately. With the financial help of a friend of Thulir, he is getting operated in C.M.C, Vellore on March 7th.  
  
Bamboo work   
The boys had learned to make chairs out of Bamboo earlier at Timbaktu and based on that experience made similar chairs at thulir too. There is some local demand for their chairs â€“ the forest council leader and the postman want these chairs.  
  
Excursions  
  
[![](http://bp3.blogger.com/_qz2j5uas_y0/ReMAgeDB5lI/AAAAAAAAABg/ecdx_zQye54/s320/Thulir%2520Feb%2520Blog%2520Draft_html_m7a143571.jpg)
](http://bp3.blogger.com/_qz2j5uas_y0/ReMAgeDB5lI/AAAAAAAAABg/ecdx_zQye54/s1600-h/Thulir%2520Feb%2520Blog%2520Draft_html_m7a143571.jpg)[![](http://www2.blogger.com/Thulir%20bolg_files/Thulir%2520Feb%2520Blog%2520Draft_html_m7a143571.jpg)
](http://photos1.blogger.com/x/blogger/8103/1906/1600/757333/Thulir%20Feb%20Blog%20Draft_html_m7a143571.jpg)We made a few excursions to cheer the group and provide them exposure. First we went exploring some of the villages on the nearby Chinnakalrayan hills during a day trip. This was a welcome break for us from routine and helped pull ourselves up a bit.  
  
Later we went to Bangalo[![](http://bp0.blogger.com/_qz2j5uas_y0/ReMBYuDB5mI/AAAAAAAAABo/pv10cENmxrI/s320/Thulir%2520Feb%2520Blog%2520Draft_html_165fe4de.jpg)
](http://bp0.blogger.com/_qz2j5uas_y0/ReMBYuDB5mI/AAAAAAAAABo/pv10cENmxrI/s1600-h/Thulir%2520Feb%2520Blog%2520Draft_html_165fe4de.jpg)re for a three day trip. The objective was to fix our old computers that were not functioning properly. We opened them up to learn about some basics of the hardware before going to Bangalore. There friends of ours helped to fix some of the troubles and helped us to go shopping for some spares. This has now lead to Mahaboob Subhan visiting to spend a month teaching basics of Computers to our students. Mahaboob Subhan, 22 years old, studied at the Learning Centre run at Timbaktu in Andhra and is learning computer hardware and software.  
  
[![](http://bp3.blogger.com/_qz2j5uas_y0/ReMCReDB5nI/AAAAAAAAABw/VeQyQpU7C2c/s320/Thulir%2520Feb%2520Blog%2520Draft_html_5571ae4f.jpg)
](http://bp3.blogger.com/_qz2j5uas_y0/ReMCReDB5nI/AAAAAAAAABw/VeQyQpU7C2c/s1600-h/Thulir%2520Feb%2520Blog%2520Draft_html_5571ae4f.jpg)We also visited a house in Bangalore that has been built using alternative technology and is experimenting with alternative sources of energy. It was an interesting visit and we could get exposure to many different technologies in one place. [![](http://www2.blogger.com/Thulir%20bolg_files/Thulir%2520Feb%2520Blog%2520Draft_html_5571ae4f.jpg)
](http://photos1.blogger.com/x/blogger/8103/1906/1600/901811/Thulir%20Feb%20Blog%20Draft_html_5571ae4f.jpg)  
  
  
Follow up workshop on bee keeping  
We had been having a bad spell with our bee colonies too and so we invited Justin and Rajendran from Keystone , Kothagiri to conduct a follow up workshop and help us with some of our practical difficulties. Analyzing our efforts and finding out where we were wrong and interacting with them helped raise our spirits . We now have three colonies in and around Thulir.  
  
Resource Centre activities   
Our regular evening activities have continued. Our regular children came even during the monsoons undaunted by the incessant rains ,our bad road and the stream across the road. A number of girls had stopped coming after the death of Murugan in the stream in summer. We went and talked to the parents and began escorting the girls back to the village after classes. Now some of the girls are coming back. The children still go back by 6.30 pm.[before it gets dark]. We have continued our sessions of learning basic languages, math and general awareness through various methodologies. Our first group of children show some improvement in all these aspects and in their confidence levels. Sometimes we ask them to teach the other kids.  
  
The head mistress of the local Govt. school and some of the teachers have been visiting Thulir and looking at our materials. The headmistress asked us to help take some classes for the older children in the school. We agreed and everything was fixed when some of the male teachers went up in arms against the headmistress and the whole thing fell through.  
  
We had a plastic awareness campaign . In connection with this, children have been making news paper covers and we supplied some to the local shops. The THI Staff had earlier talked to the shop owners about the need to reduce use of plastic. Together with the hospital team and the children, we cleaned all the village streets one day. Plastic disposal still remains a problem.  
  
Senthil, Mohan and Arul were coached for their class 10 English exam and they passed. Somehow a reputation has formed and now we have 5 new students coming to Thulir to prepare for their class 10 exam. But they have come 2 months before the exams and we don't see ourselves being able to do much for them.  
  
Most of them don't have a conducive environment at home to study [with the T.V. on and drunk parents/ neighbours, apart from other distractions]. So just providing the physical space to study would be a help  
  
Thulir  children go on an excursion  
  
We decided to visit Sathanur Dam, Thiruvannamalai and Gingee on a day's trip with 40 children. This had a been a long standing demand from the children that they be taken somewhere on an excursion. So finally we did it end Dec on a weekend. This was also the peak pilgrimage season. On the one hand we would have preferred a quieter day but on the other hand it was also an opportunity for our children to be exposed to crowds [being from a small village, it is certainly a novelty!; though we were nervous about losing one or two of them in the crowds!]  
  
[![](http://bp3.blogger.com/_qz2j5uas_y0/ReMDMeDB5oI/AAAAAAAAAB4/9S-qg5cUXZg/s320/Thulir%2520Feb%2520Blog%2520Draft_html_mbb0f904.jpg)
](http://bp3.blogger.com/_qz2j5uas_y0/ReMDMeDB5oI/AAAAAAAAAB4/9S-qg5cUXZg/s1600-h/Thulir%2520Feb%2520Blog%2520Draft_html_mbb0f904.jpg)From days before there was a lot of excitement among the children. Some 15 children who live far from the village, came the previous evening to spend the night at Thulir [we had planned on a 4 am departure]. Selvaraj went the previous evening itself to Harur [2 hours away] to meet the bus and bring it so that it could reach at 4 am at Sittilingi. We cooked food together the previous night and carried it.  
  
Amazingly, 15 children woke up, heated bath water, h[![](http://bp0.blogger.com/_qz2j5uas_y0/ReMD7uDB5pI/AAAAAAAAACA/iCAlGsRxwDQ/s320/Thulir%2520Feb%2520Blog%2520Draft_html_c10a89c.jpg)
](http://bp0.blogger.com/_qz2j5uas_y0/ReMD7uDB5pI/AAAAAAAAACA/iCAlGsRxwDQ/s1600-h/Thulir%2520Feb%2520Blog%2520Draft_html_c10a89c.jpg)ad their baths using the 1 bathroom we have and were ready long before 4am ! Our children were terribly excited but very well behaved throughout.  
  
On our way out we found a small archaeological site which was a stepped pond decorated with panels in stone depicting scenes from the ramayana. At Sathanur, we had a picnic breakfast and walked around the garden and the dam itself. At Thiruvannamalai, we visited the main temple, which was really crowded with pilgrims. We explained a bit of the history of the temple and the many mythical stories attached to it.  
  
[![](http://bp1.blogger.com/_qz2j5uas_y0/ReME9-DB5qI/AAAAAAAAACI/NWJXxjJZFBs/s320/Thulir%2520Feb%2520Blog%2520Draft_html_m7ffde9b8.jpg)
](http://bp1.blogger.com/_qz2j5uas_y0/ReME9-DB5qI/AAAAAAAAACI/NWJXxjJZFBs/s1600-h/Thulir%2520Feb%2520Blog%2520Draft_html_m7ffde9b8.jpg)We moved on to Gingee which has a fort with many rocky hills with palaces on top. The highest hill is about 800 meters high and we were warned not to climb it as we may not be able to get back in time before the fort gates close for the day at 5 pm. We were told we would need at least an hour to 90 mts to climb it. The children were very keen and we decided they could go as high as possible in 30 mts and return in time for the closure. They being fit for outdoor activities climbed up in 20 mts flat!! There's lots to see at Gingee and we need to go there again someday with lots of time.  
  
Pongal at Sittilingi[![](http://bp3.blogger.com/_qz2j5uas_y0/ReMGLeDB5sI/AAAAAAAAACY/jPFOTUbYQ7U/s320/Thulir%2520Feb%2520Blog%2520Draft_html_m655db059.jpg)
](http://bp3.blogger.com/_qz2j5uas_y0/ReMGLeDB5sI/AAAAAAAAACY/jPFOTUbYQ7U/s1600-h/Thulir%2520Feb%2520Blog%2520Draft_html_m655db059.jpg)  
Pongal was celebrated with great fanfare this year. We were invited for the poojas and lunches in people's farms and homes and the bull races in the temple .We are getting more accepted in the village and being involved in their pongal celebrations feels good . Vediyappan and his youth friends oragnised competitive events that had sports [traditional such us kabaddi, tug of war etc.]and song and dance competitions.  
  
[![](http://bp1.blogger.com/_qz2j5uas_y0/ReMFk-DB5rI/AAAAAAAAACQ/3hV3MxoLMWg/s320/Thulir%2520Feb%2520Blog%2520Draft_html_68da1256.jpg)
](http://bp1.blogger.com/_qz2j5uas_y0/ReMFk-DB5rI/AAAAAAAAACQ/3hV3MxoLMWg/s1600-h/Thulir%2520Feb%2520Blog%2520Draft_html_68da1256.jpg)A youth group, including Selvaraj, learning traditional therukoothu [street play], put up an all night performance that was well attended in spite of the cold weather.  
  
[![](http://www2.blogger.com/Thulir%20bolg_files/Thulir%2520Feb%2520Blog%2520Draft_html_m655db059.jpg)
](http://photos1.blogger.com/x/blogger/8103/1906/1600/234488/Thulir%20Feb%20Blog%20Draft_html_m655db059.jpg)At Thulir we celebrated by filling all possible floor spaces with colored kolams. It was a non competitive event that had very enthusiastic participation by all age groups and both the sexes.  
  
Visitors  
  
As we write this Blog, Sanjeev and Anita from Asha Austin are visiting us. They have been here for a week and have sessions introducing puzzles to the children. They have also introduced basic Electronic circuits to the Basic Technologies course students. Right now they are building circuits to make white LED products that would be useful at Thulir. Visit their blog [Sanjeev and Anita](http://smallisbeautiful.blogspot.com/), for more details.  
  
Dr Abraham, a dentist visited Thulir and conducted classes on dental care. He made nice charts with detailed drawings explaining the anatomy and details on dental care. The children went on asking a lot of questions, though we had warned Abraham they might not take to theory!  
  
Prachi Aggarwal, an architecture student taught the children embroidery. Mrs Mythili Santhanam who visited again this winter too continued her embroidery sessions.  
  
Rajamma Ravi, from Sittilingi, has been teaching knitting to the children a number of socks and woolen caps have been knitted.  
  
Rachel a student volunteer had visited us and during her sessions she taught some songs and games.  
  
Anisha, a medical student came to spend a month at the hospital. She took classes for Shreyarth on the human body. This was very useful as of late we had been a little preoccupied and Sreyarth was missing some serious learning sessions.  
  
Esther came again this year and held several sessions with the children. she brought dried fruits, pictures, currencies and stamp from Europe and talked about them.  
  
Dr Nick, who had come the previous winter too, returned this January. He has been teaching the children English conversation.  
  
We thank all the visitors for spending time with the children of Thulir. We really believe that it is important for children to gain confidence and get exposure by interacting with outsiders, and visitors by interacting with children really enrich their learning. In fact, during our "test for Thulir", some of the children have menioned "talking to strangers" being able to talk to foreigners" as an important skill they have learned!  

