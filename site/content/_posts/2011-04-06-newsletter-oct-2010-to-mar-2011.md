---
author: nimda321
date: 2011-04-06 18:24:08+00:00
draft: false
title: Newsletter Oct. 2010 to Mar. 2011
type: post
url: /wp/2011/04/newsletter-oct-2010-to-mar-2011/
categories:
- Newsletters
---

[![](http://www.thulir.org/wp/wp-content/uploads/2011/04/f_land019.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/f_land019.jpg)

**Transformation of Thulir Campus.**

** **

As we write this, a pair of Golden Orioles are singing away merrily on the tree outside the window. There is a cool breeze which is surprising for March and welcome shade under the trees.

What a change in the last seven years! When we shifted to this land in March 2004, it was completely brown, bare and overgrazed. There was no grass, let alone, shrubs, trees or birds. Coming from lush, fertile Nilgiris to this hot, dusty and barren land was very difficult for our children and us. We now look back to those first few months of digging contour bunds to conserve rain water, digging pits for the trees, getting mulch material from neighbouring fields, planting slowly, .....  It was hard work, and in the initial years we had to do it alone. Slowly the Thulir community has grown and now there are plenty of hands to share the land work. Nature has been kind to us -- plants have grown, trees are bigger, the bamboo bushes have grown back, weaver birds nest here, so do Bulbuls, White breasted water hen, Tailor birds, Robins, etc.; and  many other birds visit. Looking back, regenerating this land has been the most satisfying part of our work here.


[![](http://www.thulir.org/wp/wp-content/uploads/2011/04/f_land001.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/f_land001.jpg)_The land in 2004_




[![](http://www.thulir.org/wp/wp-content/uploads/2011/04/f_land021.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/f_land021.jpg)_In 2010 !_


** Land Work / Organic agriculture**

What started as tentative steps in learning organic farming methods has taken better shape in the last few months. Slowly the area under cultivation and also variety of crops and methods have increased.

Our efforts got a great boost this year when Shri Nammalvar, a well known proponent of organic and natural farming, and a great teacher visited us on two occasions in January and February. The interactions on site and theory  classes were an energising experience. This has greatly increased our team's confidence and resolve.


[![](http://www.thulir.org/wp/wp-content/uploads/2011/04/img_0821.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/img_0821.jpg)_Shri Nammalvar taking a class. _


Subsequently, most of our students and staff visited Vanagam, a demonstration Farm set up by Shri Nammalvar and his colleagues to teach organic and natural farming. Inspired, students have come back and prepared circular permaculture beds for vegetables.

In Sept. of last year, we had planted a mix of horse gram, cow pea and black gram in the new land we purchased for Thulir. By end Jan the crops were ready for Harvest. The team started work early in the mornings on the harvest and this turned out to be an exercise of great fun. The hard morning's work was always followed by extended swimming / bathing sessions in the stream. We have a good harvest and visitors to Thulir this year can taste dishes made of these pulses!

In Jan we prepared a plot in the Thulir campus for planting of Ragi. On one half we transplanted the saplings the traditional way and in another, we made raised beds and planted single sapling with larger spacing of 12 inches. As we write this, the former is ready for harvest, and we hope to harvest it this week.


[![](http://www.thulir.org/wp/wp-content/uploads/2011/04/vlc.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/vlc.jpg)_Horse gram, Cow pea and Black gram being threshed._




[![filling up the sacks](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00406.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00406.jpg)_and filling up our sacks!_


**Exposure Trip to Pune and Pabal **

In October, Sanjeev and Anita accompanied our senior students and staff on an exposure trip to Pune and around. They visited Vigyan Ashram, Pabal, near Pune; Science Centre at IUCCA, Pune; and an Army Service Corps campus at Pune. On the way back they participated in the running event at Ananya school, Bangalore. Not only Anita and Sanjeev, but also Sanjeev's mother, sister and uncle spent time and effort to make this a memorable trip for the students.

At Vigyan Ashram, they split into smaller groups and learnt a variety of activities including use of Earth Resistivity  Meter to predict water availability below ground; soil and water testing; basic animal husbandry skills etc.


[![using ERM](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_76271.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_76271.jpg)_Learning to use the Earth Resistivity Meter._




[![](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_7721.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_7721.jpg)_Measuring of Cow to record Health status. _


At the IUCCA, Shri Arvind Gupta and his team demonstrated making of science toys.


[![](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_7777.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_7777.jpg)_Learning to make toys._


At the Army base, they saw a museum of old machines that the Army used, besides visiting the workshops and training facilities.


[![](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_7810.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_7810.jpg)_At the Army Base._


**Exposure trip to Isai ambalam school, Auroville**

Rajammal, Devaki and Theerthammal help teach the younger students in the evenings.We had always wanted them to visit other alternative schools, interact with good teachers and get exposed to various teaching methods. Therefore it was an important opportunity when Sanjeev and Anita took them to visit Isai ambalam school at Auroville in November. Shri Subhash and the teachers there warmly and patiently demonstrated their teaching methods to them. They also visited a few other units at Auroville.

**Centre for Learning , Bangalore, students visit  Thulir.**

In December, a group of 8 students and 2 teachers from Centre for Learning, Bangalore visited Thulir for a week. This was the first time such an event with urban school children was organised in Thulir. We had many doubts as to whether cultural differences, language etc. would come in the way of meaningful interaction. We organised a series of sessions where each group taught the other new skills. Slowly, students got to know each other, and started to enjoy the sessions and each others company. Sadly, soon, it was time to leave !! We hope to have more such visits in future.


[![](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_8053.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_8053.jpg)_De-weeding at the Black gram plot. _




[![](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_8095.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_8095.jpg)_A sketching session under progress._




[![](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_8122.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_8122.jpg)_An LED reading lamp that was made during the week._


**Building a Battery room with a vaulted roof**

The junior batch's exercise in learning masonry was to build a small Battery room. We built this using a catenary vault using burnt brick and cement mortar.


[![](http://www.thulir.org/wp/wp-content/uploads/2011/04/p1070241.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/p1070241.jpg)_Preparing the temporary support for the vault._




[![](http://www.thulir.org/wp/wp-content/uploads/2011/04/p1070293.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/p1070293.jpg)_Top surface being smoothened with wet sand._




[![](http://www.thulir.org/wp/wp-content/uploads/2011/04/p1070300.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/p1070300.jpg)_The first courses being built._




[![](http://www.thulir.org/wp/wp-content/uploads/2011/04/p1070319.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/p1070319.jpg)_The vault coming up._




[![](http://www.thulir.org/wp/wp-content/uploads/2011/04/photo-0076.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/photo-0076.jpg)_Finally the support being removed._


**Evening Classes**

The evening classes continue regularly. The students, mostly girls are an enthusiastic, eager and highly vocal group. They learn quickly and with great interest. Now a days, Jayabal too has started teaching them. Initially Ravi and now Sanjeev, have been taking computer classes for them.

**Long distance Bicycling**

Ever since Balaji visited Thulir travelling on his bicycle long distance, our students have been fascinated by the idea. They started with short trips to Kottapatti and Thumbal [10 kms and 20 kms away respectively] on weekly holidays, on their bicycles. Then they tried Vazhapady [45 kms away]. Soon they did Salem [80 kms away] -- a return trip on a single day! The first major trip came when they travelled last year to Gingee fort. They cycled 105 kms the first day and stopped near Gingee for the night. They visited the famous fort there, had lunch, cycled back through the evening and night and reached home at dawn, exhausted but happy!


[![near Thiruvannamalai](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_8007.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_8007.jpg)_The team near Thiruvannamalai on Karthigai day_




[![getting cycles ready](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_7986.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_7986.jpg)_Much preparation goes on before a trip!_


[![getting cycles ready](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_7992.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/100_7992.jpg)

This year, on Karthigai Deepam, an important festival day in Thiruvannamalai [80 kms from Sittilingi] , a group from Thulir cycled to see the famed Deepam. On the way they took a detour to visit Sattanur Dam.

During the Pongal break in Januray, our team went on a 3 day trip to Hoggenekkal, which is 135 kms away. They stopped en route at Puvidham Learning Centre.

Getting ambitious, now they want to do longer distances. Balaji is working on organising a cycle tour in the coming months which will take them to more distant places. So far, they have been using old bicycles donated to Thulir. These are not ideally suited for long distance travel and are usually in very bad condition. Our trips so far required elaborate servicing of the bicycles before starting and quite a bit of repairs on the way [which our enthusiastic group carries out on the way, as they usually travel with their tool kit!]

**Meetings/Conferences hosted**

The beginning of January saw all of us in Thulir preparing to host two major meetings here. This was again a 'first' for us. Since we have the bare minimum of facilities here and access to logistics difficult we were apprehensive whether the participants would be comfortable, etc. We had the conference of the Fellows of Asha  from the 4th to the 7th of January. Fifteen participants came together for 3 days and we had meaningful interactions.

The next occasion was the network meeting of people involved in Alternative education.This annual meeting has always been a source of strength and inspiration for us and we were happy to host it this year. We initially expected around 40 participants. Finally 27 people attended the meeting from January 27th to 30th. We needn't have had any apprehensions; the Thulir team rallied together to play hosts; the kitchen team churned out their best; all the participants adjusted cheerfully to our organisational lapses; the friendship, humour, sensitivity, caring, bonding and spirit of sharing  of the whole group made the  meeting a memorable and  inspirational one !

**Improved firewood Stove for Hot water**

As part of the preparation for the meetings, we decided to build a stove for making hot water for the guests.

**[![stove 1](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00023.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00023.jpg)**

[![stove 2](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00029.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00029.jpg)

[![stove 3](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00035.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00035.jpg)

**Stabilised Adobe Training Programme**

Adobe refers to hand moulded mud bricks that are usually sun dried and used in constructing walls. This is a very old and simple technology. Dr Yogananda from Bangalore has now come up with an improved process for making Cement and Lime stabilized Adobe. We organised a training programme to introduce this method in Sittilingi Valley. Thulir students along with local villagers participated in the Programme.


[![Stabilised adobe 1](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00288.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00288.jpg)_mixing soil, sand and cement_




[![Stabilised adobe 2](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00290.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00290.jpg)_Adding slaked lime [lime water]. _




[![Stabilised adobe 3](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00292.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00292.jpg)_Pugging [mixing with stamping of feet .... the most important part]. Caution! - use boots and gloves to protect from lime burning skin! _




[![Stabilised adobe 4](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00294.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00294.jpg)_The mix is filled into a steel mould._




[![Stabilised adobe 5](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00298.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00298.jpg)_Ejecting the Block out of the mould._




[![Stabilised adobe 6](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00299.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/04/dsc00299.jpg)_The finished Block .... Now has to be cured with water for 3 weeks._


**Apprenticeship.**

We have been exploring avenues and oppurtunities for our senior students  to have further specialised training in their specific area of interest. We were therefore very happy and grateful when Vanya and Gautham offered to train Srikanth and Sakthivel in wood carving at their Workshop in Silvepura, Bangalore. Vanya and Gautham have  very generously taken Srikanth and Sakthivel into their home and lives for the last 6 months, spent enormous amounts of time and effort looking after them and teaching them ' to think  with their hands'. Santhosh's continuing interaction with them has been of great help.

Similarly, Manoharan from ACCORD, Gudalur has taken Senthil under his care and tutelage from January and is training him in crucial administration and computer skills. Senthil has grown hugely in skills and confidence , thanks to Manoharan.

**Village Survey**

The Tribal Health Initiative has started a new free health scheme for the senior citizens of the valley. They were hence doing a  village to village survey of all old people. Our students too participated and helped them in this survey.

Five students took part in a theatre workshop conducted by Sri. Arumugam  from Krishnagiri at THI.

**Marathon running**

Santhosh continues to guide and co-ordinate the training schedules for long distance running of our students. Balaji continues to help by looking after our students whenever they travel to participate in running events. There have been some breaks in our team's training for running, mainly due to the fact that there is an increase in Farming activities -- both at Thulir and  at home, as there is labour shortage in the village. Small groups, have been taking part in several events -- the Ananya school run, Bangalore ultra, and Auroville marathon. We need to better organise and integrate running  into our schedule in the coming months.

** Public Exams**

As we write this some of our students are writing the Tamil Nadu state board class x exams as external candidates.


***************************
