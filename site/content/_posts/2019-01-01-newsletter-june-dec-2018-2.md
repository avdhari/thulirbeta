---
author: nimda321
date: 2019-01-01 06:53:52+00:00
draft: false
title: Newsletter June-Dec 2018
type: post
url: /wp/2019/01/newsletter-june-dec-2018-2/
categories:
- Newsletters
---

### New School Inauguration


As we write this, we are all quite excited as 6 classrooms, office and toilets of the new school are ready! We plan to have a very simple inauguration ceremony on the 23rd of January. We would be honoured if you could join us on that day.


### News of the academic year so far…


It was truly heartening to see the children come back to school eagerly after the summer vacation with huge smiles on their faces. They asked after their plants as if they were long lost friends! They wanted to look at their favourite books and puzzles. They came to each of us with stories and incidents from home.

It is always like walking on a tight rope trying to fulfill the requests for admission while still keeping the school small enough to be able to maintain its quality. Though there were numerous requests for admissions, we decided to take only fifteen 4 year olds this year. One of our students left us due to parental constraints and so we now have 53 children from 7 villages.


### Parents’ Initiative


Our first parents meeting this academic year started with an informal interaction between the new and old parents about the school. Parents spoke about the school and its philosophy. Most new parents too showed some awareness of the philosophy of Thulir.

Bringing children to school has always been a problem for parents from the distant villages since we don’t have a school van. But we were very happy to see that parents from Dhadhampatti came together and took the initiative to solve this problem. One of them has bought a second hand van and this makes two trips each day and brings most children to school. The logistics of this is being handled entirely by the parents.


### Formation of parents teacher association


Over the years, we’ve been discussing the need to have a team of people comprising of both teachers and parents to handle and resolve issues that affect both the school and the family. They would act as a bridge between the parents and the school, especially taking decisions related to the scale of parents’ contributions towards the food expenses of the children and collecting it.

![](https://lh5.googleusercontent.com/tN2ome4rmt-9dn4cxYdilXertFASH-7QpEvmWx4YlWusZXjs8NXMzdD0O_vIARa0zYcOfA2Xy_APyoAmBUZg2sFXUz2mDqPL3sOhliU3J_mT-94csgecTfgcVdRx52u2Z8x5B_uK)


In Thulir, parents don't pay fees towards their children's educational expenses but they contribute in cash, kind or labour towards the food that the school provides for them. We were very happy that the parents took the initiative to create this association and form an executive committee. It was decided that the executive committee would have two parents from each village and two teachers. In fact, the executive committee members have already started taking initiative in helping with some of the school work.


### Foldscope




Do you remember ever handling a microscope in your high school days? Of how careful one would have to be around it, how rarely one got an opportunity to do so and how you hardly ever understood what you saw through it?! Imagine if microscopes were not so expensive and every child had one in their pockets and could explore the inner world of whatever took their fancies!


The Foldscope is an ingenious microscope designed just for that. It is just a lens mounted on a sheet of paper folded in a particular way. The Department of Biotechnology is trying this out on an experimental basis in a few schools in India. As part of this, our friend Dr.Varuni from IIMSc, Chennai, arrived in Thulir at the end of July with a dozen Foldscopes and a plan to study the pollen from different flowers.

[![](http://www.thulir.org/wp/wp-content/uploads/2019/01/IMG_20180728_160332.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2019/01/IMG_20180728_160332.jpg)

What we do is fairly simple - pick a flower with pollen, tap it on the slide (which is just a paper slide with cello-tape) and look at it through the lens. But what we get to see is a visual treat of yellows, pinks, oranges, translucent or opaque, plain or patterned units of pollen grains different for each flower. Although the pollen is what we intended to see we had surprise visits by creatures otherwise invisible to the naked eye. Worm-like, insect-like creatures were quite a fascination for the children and adults.

![](https://lh3.googleusercontent.com/mfFsa8hmJnGFAbOYgUeRFvPn3bj1aT_Nan1govFLkj2894ylHTe48olvflVtehLnZRUfCecr0z_pACscEW6HGztpXOF9_oYzUQJYhWCtRRq2lxmKf0YndNLPfCiC_-SDyrbRy1h6)



### Joy Of Giving




All of us were deeply saddened by the floods and loss in Kerala. We talked about this in school and subsequently when our friends from the Gurukula Botanical sanctuary in Wyanad sent out a call for help, the entire team wanted to contribute.


Ours is not a rich community so we were overwhelmed by the way children and teachers gave wholeheartedly. It was really touching to see 4 and 5 year olds come running with Rs.5 and 10 in hand. One child came with her piggy bank and shook out all the coins! We finally sent around Rs.18,000.


### Participation in Marathons




Sakthivel and 7 Thulir alumni students participated in the Bangalore marathon in July. 12 runners from Thulir participated in the Hyderabad marathon in August.




[![](http://www.thulir.org/wp/wp-content/uploads/2019/01/IMG-20180826-WA0002.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2019/01/IMG-20180826-WA0002.jpg)





### Interaction with parents




Swetha Nambiar, an artist and long time friend of Thulir, is currently volunteering at Thulir. Swetha handles English classes for the younger group and conducts art sessions for all groups.


Here Swetha writes her impressions of the Thulir Parent teacher meeting held on 21st September 2018

“On a fine afternoon, the quadrangle at Thulir was buzzing with people smiling and welcoming each other, the teachers and parents catching up on the lives of the children and each other. The soft buzz settled into a quieter quiet, welcoming the gathering into that moment.

[![](http://www.thulir.org/wp/wp-content/uploads/2019/01/IMG_20181221_153254.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2019/01/IMG_20181221_153254.jpg)

A quick update of the parent committee, school timings, and such. Parents were asked to share what they felt and that really opened up the expanse of the influence the school has on the lives of children.

"_He used to be very quiet before, now he is talking to us and tells us what is going on in school_" this was something that most parents of the youngest children shared. "_She used to be scared of everything but now she's open and demands we too sing the songs she learns in school”. "She doesn't like the food we make at home, comparing it to the food at Thulir_”.

Another parent shared that she felt very proud when her daughter helped a child who fell down and was crying, and on being asked, her daughter recited a line from one of the songs that we sing in the assembly about how the "_magic of the hands are in helping those in need”. "Other schools do not give Dhairyam (courage) and Thannambikkai (self-confidence). Ellaraalayum ellavishayamum mudiyum (everything is possible by everyone) and that is what is different about this school_" said another parent.

The problem of child sexual abuse was brought into the meeting, a concern that had cropped up after Anu had a class with the older children on body and safety. The children had confided to her about instances of abuse they had faced from relatives and older teens in the neighbourhood. Parents and teachers were equally shocked to know that child sexual abuse is no more the stuff of news carried to us from the cities but very much a reality here in the village.

What was interesting for me was if someone had walked into the middle of the meeting they wouldn't have been able to say who the parents were and who the teachers were, it felt like one whole community of adults sitting there, talking educational matters. That felt so right".


### The role of stories




In October, Mr. David Vale conducted a workshop for the teachers on using local stories as a resource for language and literacy across all subjects of the curriculum.




Swetha writes again …




“In the month of October, all the teachers would look forward to"David Anna's Class" in the evening. He is an excellent narrator primarily interested in English Language education. But the principles we learned from him can be applied to any language education. An important learning was the role stories played in learning a language.




Stories chosen for language classes were done so by keeping certain factors in mind - language appropriate for their age but without compromising on the richness of the language, possibilities of exploring math, science, social science, art etc. from different aspects of the story. This is important simply because the story gives them the foundation to do that and all the language used around it becomes meaningful in that context! This is extremely important in a language - especially if its not the home language.


These classes were very engaging because he did with us teachers what he expected us to do with the children. It was hands-on, and full of activities just like he wanted the language classes to be. From how to tell stories to opening up the expanse of where all it can take us unfolded in his classes day by day.


The high point was when the teachers made two books based on the life and folklore of Sittilingi titled - The "Magic Purple Chili of Sittilingi” and "The Bamboo Story". Books that we can hold close to our chests and call "ours". Books that children from here would relate to like it was happening in their neighbourhood. A small feat that, has actually fueled us to make more such books .





### Marudam Craft Week




Participating in the Craft week at Marudam farm school, Thiruvannamalai has become a much anticipated annual event. This year 17 children and three adults stayed in Marudam for the entire week. Our children are now older and it was truly a pleasure to watch them interact fearlessly with the older children from other schools.




[![](http://www.thulir.org/wp/wp-content/uploads/2019/01/IMG-20181102-WA0030.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2019/01/IMG-20181102-WA0030.jpg)




They learnt a wide variety of crafts and exhibited them proudly on their return.




[![](http://www.thulir.org/wp/wp-content/uploads/2019/01/IMG-20181102-WA0026.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2019/01/IMG-20181102-WA0026.jpg)





### Sad Event




Sittilingi was in the news for the wrong reasons. A cheerful, intelligent 17 year old ex Thulir student was gang raped by two youth in the village in November and subsequently died a few days later! Youth and women from almost all villages in the valley got together for two days in a peaceful show of solidarity with the family.


We used to be proud of the fact that Sittilingi was a safe place for all. But the fact that Sittilingi is changing drastically, is becoming 'developed' and 'non-tribalised' and women are no longer safe here was brought home rudely to us.

Alcohol, migration to cities for jobs, TV and internet access, exposure to porn on the cell phones etc. are huge influences on our youth. Our boys need sensitive upbringing and good role models urgently. Our only hope lies in the fact that the tribal value of community spirit and action has not disappeared totally as yet.


### Article about Thulir




Ms. Salai selvam, an educator, activist and a columnist in Tamil Hindu wrote a series of articles on alternative schools and learning spaces in Tamilnadu. Thulir was also featured in this series - [link](https://tamil.thehindu.com/general/education/article24431832.ece)





### Visitors and volunteers





	  * Sujatha Padmanabhan from Kalpavriksh, Pune visited us in June. The children were quite excited when they learned that she was an author of children’s books and asked her many questions about writing a book, illustrating and printing it. Later they enjoyed an unique experience of hearing a story read out by the author herself when Sujatha read out her story, ‘Bumboo’.


	  * Visitors from various countries talked to the children about their countries. Geography is more personal and fun this way. Ben, a medical student from Austria and Natasha, a medical intern from Zambia talked about their respective countries.


	  * Suja Swaminathan visited us at end of July to do a small study on “schools as  spaces for incubation practice and transformation of culture” as part of her course work at TISS.


	  * About 20 children and 2 teachers from Helikx open school, Salem visited us in July. Helikx school caters to children with dyslexia and other learning disabilities.


	  * Balaji from Asha for Education visited us in August. Balaji is a long time friend and supporter of Thulir. His support means a lot to us.


	  * Ramya Arivazhagan, a story teller from Chennai visited us in August. Ramya’ s telling of the Japanese folk tale ‘Momotaro’ (the peach boy) was particularly enjoyed by the children.

![](https://lh3.googleusercontent.com/LuO7-ui-yqpwIAgkNw2EXh0NdehNVlNTVqnYqHrAubykx3RYB3g5wxG_nFihrxDMAWvNR9unznnRW8zODmMIHlGBgMSNtKKVNZhxCQUe1HZd5vZrYzamo69i0a-Mc3A2MQhy-NVO)




	  * Sunayana, a theatre artist from Bangalore, visited us in September.


	  * Three young architects, Muthu, Dinesh and Meenakshi interned at the school building site. Muthu left after two years and Dinesh after a year as the buildings got completed. Other architects and architecture students have been coming in for shorter durations to learn practically on site. Meenakshi is volunteering now in the school and helping us prepare books for children.


	  * Srinath, a civil engineer from Kottapatti is currently volunteering at the school site. Most of the volunteers used to come from cities, we are happy that some one local chose to volunteer here.


	  * Sreyarth helped our teachers with computer literacy in August. We can observe a marked improvement in our teachers' confidence and their ability in operating a computer. Sreyarth also did some basic theatre exercises with the children.


	  * Siddharth, an actor and theatre artist from Kerala did a short theatre workshop with the children.



[![](http://www.thulir.org/wp/wp-content/uploads/2019/01/IMG_20181119_153501.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2019/01/IMG_20181119_153501.jpg)






	  *  Sri Madhav and Neelima Sahasrabudhe form Pune came for a weekend and taught us spinning and origami.


	  * Joji, Shani and Shashwath from New Zealand visited us in December. They boosted the  morale of our team by holding a special session and explaining how impressed they were with the children, the atmosphere and culture of Thulir.



###  A 'Thank you' lunch for the construction artisans




The first phase of the new school construction is over! As a token of our appreciation and gratitude we hosted a lunch for all the construction artisans on December 8th.




[![](http://www.thulir.org/wp/wp-content/uploads/2019/01/IMG-20181209-WA0008.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2019/01/IMG-20181209-WA0008.jpg)





### ![](data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAASABIAAD/4QCMRXhpZgAATU0AKgAAAAgABQESAAMAAAABAAEAAAEaAAUAAAABAAAASgEbAAUAAAABAAAAUgEoAAMAAAABAAIAAIdpAAQAAAABAAAAWgAAAAAAAABIAAAAAQAAAEgAAAABAAOgAQADAAAAAQABAACgAgAEAAAAAQAAAZCgAwAEAAAAAQAAAOEAAAAA/+0AOFBob3Rvc2hvcCAzLjAAOEJJTQQEAAAAAAAAOEJJTQQlAAAAAAAQ1B2M2Y8AsgTpgAmY7PhCfv/CABEIAOEBkAMBEgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAADAgQBBQAGBwgJCgv/xADDEAABAwMCBAMEBgQHBgQIBnMBAgADEQQSIQUxEyIQBkFRMhRhcSMHgSCRQhWhUjOxJGIwFsFy0UOSNIII4VNAJWMXNfCTc6JQRLKD8SZUNmSUdMJg0oSjGHDiJ0U3ZbNVdaSVw4Xy00Z2gONHVma0CQoZGigpKjg5OkhJSldYWVpnaGlqd3h5eoaHiImKkJaXmJmaoKWmp6ipqrC1tre4ubrAxMXGx8jJytDU1dbX2Nna4OTl5ufo6erz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAECAAMEBQYHCAkKC//EAMMRAAICAQMDAwIDBQIFAgQEhwEAAhEDEBIhBCAxQRMFMCIyURRABjMjYUIVcVI0gVAkkaFDsRYHYjVT8NElYMFE4XLxF4JjNnAmRVSSJ6LSCAkKGBkaKCkqNzg5OkZHSElKVVZXWFlaZGVmZ2hpanN0dXZ3eHl6gIOEhYaHiImKkJOUlZaXmJmaoKOkpaanqKmqsLKztLW2t7i5usDCw8TFxsfIycrQ09TV1tfY2drg4uPk5ebn6Onq8vP09fb3+Pn6/9sAQwAGBgYGBwYHCAgHCgsKCwoPDgwMDg8WEBEQERAWIhUZFRUZFSIeJB4cHiQeNiomJio2PjQyND5MRERMX1pffHyn/9sAQwEGBgYGBwYHCAgHCgsKCwoPDgwMDg8WEBEQERAWIhUZFRUZFSIeJB4cHiQeNiomJio2PjQyND5MRERMX1pffHyn/9oADAMBAAIRAxEAAAH0Vfn9A1IyFxRhaKRMRtZnDCnzCn1XVlVxtKmrWqqzqqtKeNxR1eUVXlFV7RGvaWrqjFdc/G/58G/56r/na6Cgq/oqvaI11RA3dHV3SGuKYVtVms6+D5gpeMYuWRnQhFFRBkERCQkrQpyxIWZKiETUTWXBK66a025IqwbWoBtaiNpVRtWQmmGrJM+WIZBN3EW7imzmmxItyRGugkiM1AOIB6CuIiUhVDJSZNpAVNQugmIbli2LQy1C5VzAchhY0JgTVChJggiHRSVIyK4TiA6oJSBroS6RJk4r6Gt+QS4oUIapkLpk6NzFiTU1paxaOlhYFa59TFQZHHLp2HPwvq4ixMVbPYsXtMXtMLCq6xqvsarLOqq3NT3UKS8qkvaor6FFfVRXtUN6aivYUd3VNcVT28Ke3NV2hFVawq7Sq2xhXWRFdaVW2xFfby07sNWPRMX9MbCDCzNxx89vUiNiEiMhYgkVgkBESNQ8Y8SXSWTXG7U+QVAtCBvO5z2bLitYPfK25iEBGukqpKqSqk0wN1zJuo5gXUctXVctXVczXT8xXT8zXS8xXTctXUcpXV8pXVcmR1nKV1XJV1nJC61uwOmtEMmlQLRUxEaRTBEhUugmeII5Scen1bdWESKJEmUkyhQpCwZl+eOzBomTvEvimYiVHzectm8EGhdPQU7czqK0ClNKipiPNtKHxZu1p1N43BeAIdBogKqnS9AMcrIJzKKjBowqMOpRWgXTLccaZNX1W+fb6TRSoqcIK1eoTz9ctzX3N78/beRdHL03NYdXv+1z2ratq2qJFTWqNyLpX4W4j1c6c49EeF5zs9myYHy6d6jXnexQecGnQ8qj9fzDp1cFOV6WuL7c68HYqAllg8IXSHlVndWvMdar0HQFOBfNixNAJoBNQCxCeunbvhzFtnsxsZLBm+T7mV6OngqpRAUq3Oj5u6Ide1edPnVKl05MuHZ6rurm2ratq2ramjdWrCq3Btkf0Vhrzvk0mK82LnrXroyV07tGmFjFc+pdXKefevjfHqdpj0UMBSRx9wrcOdHS4KoXF7WjTsuZXTs+PN1aZOOI/M1cQEagOKCShAj0vGHHtqE42DBNnPOzOqxen0ImmEqK1ujSehY9Xn3fMiVa8jRxHg+6x67rbZ7VtW1bVtTFypp1hvNFZ6dvU7ct5xUeyGp4HJowSScKv3g3ytsyUSU2Gzz1WLTLsQvlfuTcN30/l/bVxlvlrC8zz3Z63Id+2PO3i659FmSHGdXpycr1ZXleorleorlumrm+jBZ2Tc7Q55WhgaB5l28irPv9Cno4pxB1RuRDWsfCaWETYiIomi2AV4lvXV1Cad1mXatqpwZ6efvRGvdcuRjLp6qrfl5c2YqbwJz12dL5LG6bNVLd6ufjQR3zuGGZ9D4rW9A8Xc+n+SqPTowiTTrMjiSErz6nBZa1YdEacrlbZpXSFEJJABI8VU6ZvODA9A4aXpedXS0d8/Z3CduZQoCrs9+8aFnqSMmEwKJifEw3Kc1Ylai0IvWWej9jlo+Yxr1AlD0YUwWyrLAO3Yg01zzYiGedPOlrAT96X41EBxZ17KqUds/TQyejMIOoNpyHV1XG3QUgus5Zl7ji4eoi0zfYOlVMePl7PzuvR+Bh3/Dw7njTdhycKdlrny8LoVT5yoDrxYa2zF0ftRNZw62rxNOj4bbL07xZ8vcfLE19Q4AHs+c6eXseHVuxoWzuuSFfcRl0dzT2b+r35Z57Dut6ox3+mTCoE75hdLupKuSUws1aqv8APRneCouhm57pKo+krnOkhXOe9OajQoRietnle0dZTF+Qwcleo2uT3T7VyrxRzZU0Ap8YmkKBDNc2XbPnFMHmdENhX1WhtEZ6M3CaXlQS8T0c5HeWtMIrc0eHR0DnHax6Lv8AN4i4Bp3b51F5nrQ9Jl00vUaYc92QuS7HPXlOufPhO10y4Tug3mHqAby71EHie9z24rtYcr09UV2RUWdVttCpuA1K/I4cnZc1fZac33WZrLrOSezbuYhcwGaZ2zKuWEUKEE5VhYy1NpVY8Br3RFDZseUtZQvmWiM62Q4WjktSrRkVis9stc1dirci35+igBtz31Zjt7wLVSphCRRFEbtmdmQ6ZU+EGKoGMQLSC1SqMkDtXkK15CtsarrITRBEpBbkpm6pm9i2firnDK2VSNWmJYBcQCkUSAIMIEk0EwLJ7GvfAVL6Na8hU20eX6A3O3rIyZb5EZFRdfh1tbNduk5uF5RnKeQBQBD6lRb8vUup2jumT2DJ5Bk6pueI5EZjFy2gtMJHGUi2rEpMkRFPW6vgSjghE0hEXLUTgMZcwYvYM3dNHVNnNINTclANFkapkECiJRSh0nUFwJuSpTQJposaBNSoXVpWB+mpDjfvFPLdEj8lcsoN1cfoeXoVoxoDxyjAxqnTB0Cah4rtSdRtFGpjihcIujspTooxAdQ9W0U4qLQ2BeZH2ZZ0Eao0RaJMVa6h6k6o1FyuvACxm+ZU4EWirBh4hpobVb7k667V/9oACAEBAAEFAiBX+fp9/R1DyDyS8kvmIfNQ+ah85L94D575755fvCnz1vmrZlXXNbzU8lOp769qOn31y0PPfPfPL56nzlvnKfNWzKt82R85b5q3zFPmKeZeRdT/ADlO9O9HR0/mBfWpYUD92b2+x70/1BT7uv8AMEPV6vV4un3FJCmbG1L93mS63yX73RoubdZm9p0dHTuQ6Ojp9wjtp30ejp2p3FHV6f6lXFGsSW0KFqKUgLSXUOodQ6h1S+l9L0fS9HUOodUuqXVLqHUOqXp9yjoXRTop4rYTI8JHy5Hy5HypHyVPkKYtpC126kPlF8p8l+7u4BRN9664XHsWCQ8UvEOdP0SZVpYu1Bpu4i6OgdEvpfS+l9L6X0vpej0ej0/mNXq9Xq9e2TEtGtRUfuXX+M96dqO4HRP+7sOHaX91iCwNafSOn+rx2uP8a+9N7Fx+7sPY7L9gAFp4n95964lkQv3mZ+8zP3iZ8+Z8+V8+Z+8Sv3iZ+8zP3iZ8+V8+V8+V86V86Vx15f3j2UsJaJUqBWKLoqb704OE37mwQodzwo4+Kvb+9dfve1A6d5IFqduqvenajp2MiYbeTdpa2l+Zl/dOTK6vbula90so1TKM9x96WRRM/wC6inXEQahl0aOK/aD8vuX6ilZzrbyqX2HerjKa/flH0C40F2dvS4+6pXU51yojlRJS3CsvvTp1m/dITkvuRqj2l+0OHkqRCXzY2FA9r1OS+UhEaKdh2o5LRVLO1CRJagvF0+7Som6F2ysFVB+7MoBQOsA+jvDRAiBP3lAETpomxjyX3I6k+0viOA4XqcriW0QhW36LaLbm3F3ZRztcJi+5GmsmAUMWrRy/vO9HTVZJcsWgjoKUdxczxLSapa9EyamL20u/H0Een35VUC/ZilVGoEEdpUVWhNFyMcBwuP8AHLv9/YfvHEKRq436DVhhwV5iV6J4K1clc9e2vYe0CMjR85NTKkKvFpWE+y5/3JTViNaSjhdIyt08PvS+0eDtf3HaT94PbkafZHCb/Hbw/wAZsP3jhNY1B3n7l6PNAYuFAmddIo1KPkRrR0dHRy6ITmXEqYpVHVrBSaVI7XAJihgTEhQBaHOmRUcNpHF9+UMtXGEYxdpR9IPbk4I9lPCb/HNxnjRd7eoKW4lEQT7hcVuL7nITcAJUhPJxDxLhhQAEVTwZ9rtXshAWoWkEbIeIdwnSlJ+0CQVlrzU4EHL+YWKpa/3lxdmIR7orNy6yjRa1pLR7I4T/AOOXCf4xYCkjg/dLSkiWGJyWkSyLRGEMCY3i8dQNCNOREX7tC/dYX7pE/c42m1ShSuLLnpgOq5Z4Q+0OpyBxiie8sgjRLuNwTt91JOnueKh/GV2/NkVCAqrk1lp1TW+CYvYB0mOUsxUVwZIccmThWnlHhcSI7p7Jfl+SPVP3bq/MNwndIS/0lbF3F7DJDD/jDV7IVon2SA6/cuYubEI8Xtxxn7yH6QRrkvJQlEROa6qxQgLkWOlMebRwr01awCzoI+Mg6ZiXb097YaeyX5JGkPsfd3UUuvyhoAzgR9KC5VUQpWKoJUSD7pe5JIdjBiAa95/3kcaYoydeV9OtpaFpxMgqbiR0laplsSJIkmJSmaUGG6lWuf8AeJVhONxSxuMLRf27F5btGodGBT728DrR7bRpKR9DCqqZy1fvEc33lO4wNFxAsZB17SFzQ81EICYlKoArTJ3B6gr6BXtr0aypc0FtR3UCrdJJUoEBxzFJlqpUSi5I15gJQ4NVqNSrghCFJ5KHykvlBwfuu15zRcCS4DFzdhi+ug/0jM/0mXuFyJxEoBdXGKzXCnEpMbmWlT/vycVC4BU4oCu4gUceYtwS8ySShABDlMaEG4t1pEgA95QRz81i4kEhMvMVOnH3yFLXeyKEi1ydql+weY+LoovlPHGIvztUpMXLS8UuiHB+67XiPpSl4ujo6O7DHGgYXjIJkyKmCihEiolc1INqZVOVGsUqbZgjEPbpBzF82GdG4RyOa45oRVC+mRUxBTX6Wv0scceN1iVYtLTDIpqgwKYksRPkMRvl0eLmNEuE/SWJ6Hil8pw/u+13xLq6h1Zd1wTqQO0UaqkqJMeY5cvITHRkYu4ArCvFKYxjZ0RCuSq5EIATHkK6IW6rLRbP3J+73bTt8qzPaKiVHZTKYtGLdiBL5D5L5TwfLd2Rks0CNHBklWbydS0Typf6R0O5FyXUkpyZq9exBc0ay4UHmzDRIFUAUQEUnFCb36NS8ha1Uq9QlLTVKudp7vpNtgUVWcvKjs1pT7qpotKMQgPlvFlKnynyA+QHy6OjqGe/Lco6FQFSuVioCR4EvlvlvlvlvlMwsRPAMJZjZjeDWFJCI1MxVYSEyZVYTpMkgdBNjHE5beOKO69pTh49iHgHTtq6F07EF8Ow1SQzV0eLxZWWe1Ax0urq6vJhTIAejoHi8WUujKXgl8oF8oBqtRXkKS0KAQnqRHApUiUqjSMJY/dIC57GxxSbZMqPZJo+p6s9gSGJO9XV9RaQR2xS8Uss5F4qdHRL6akJpillLwDweBeJZLyebGrq9HRlIdGU1eDL5CwuKQpcKClE0dZEXKkiTcU4XS83boJkt5c09up1DoS+WWI0h6M0dUvJ1Lq69qPRqydF9qBjls6ks/do6Oj4AdqsssHsQ6VcikJFUkSKCHbwieNdpA5bMAyIINjJQ/cq6h1ZJevaj1eIdGaPIB1LqrtRmr1afa5geSC6B0dB9wvgKvR6PR0dNaMup7VAdwMzY3C7Yc5C2o1cztf3o4fd8n5Mdktb82hr+6niew+4e35j28uyfuHueA4wfvi5H//aAAgBAxEBPwEcd1FotF2l2l2l2l2l2F2F2F2F2F2F2F9svt/1fb/q+3/V9r+r7f8AV9v+r7Y/N9sPth9sOyLsDsi7I/k7I/k7Y/k7R+TQ/JoNB4+hy8t/0bbDbHx/oMaX3223pel16PuFln2+j+p/3K/qv9yo6m/7L7v9H3T/AIr75/xH9T/uV/U/7lf1P+5X9T/uV/Un/Ff1R/xX9Sf8V/Un/Ff1R/xX9Uf8V/VH/Ff1J/xX9TL8n9TL8n9TL8n9TP8AJ/UT/J/UTf1E39RN/UZP6PvzffyPv5Pzffyfm+9k/N97L+b72X833s35oy5fWT7uT/Gfdn/jPuT/ADfcn+bAkht3O53Nu5vTL5GsfIT5QeP2OuyvpCMj4DIGPlHLAcd1o0nEnskx8fRvuEYV4ZRr6FWX2T+aYSHntx/iDQREAcPUVt48sDNx1+R7wUM5UOyTHsjC32f6so1rGcR6MpX2gU2kBPlvtOaIcPVxkalw5upiTUR/nRnH5MRvFg64zUxpKVBlKzbhP3N940q0ijrP0Yeuh0h4LD8Acn++ewHXbL8mGOXqHamI/PQ+T2gbuH9FP/GD7Wyf3+P6NdLXqx6bHP8ADkYYY4/BKUsPxBOX+icliqZAhx3ud47wdTIW3pLwGPqghLbj8Fjv2iiHJfrrvj/iMZwviL7kPyRkiy6gDwE55/miRIHOpPJbbdzudznynxZb06We2f8AhZny3pDz3yuuGAJ/FaY12Bl4Ye2TyOU7Yjw25c0I1ZcXU4jIxPH+FyZumj/a5/oiQlEEeDoJFjkrgpN67bdtanT0d3KTRcv4+6UQTy+zB9uLCMRIFOQGJ0DijuMnkN9s5UjKL8Jk4zudrtQ5CdN0qq2MJS8M+ky5JXwE9Jnx39kSPzY9GNtzl/rOMgARjGRpIn+TVDmDH2/Xc0L+0F235ZADX7Pyag1j/q7cf5lHnXZu53vs/wC5w+yf8YPsy/MPszfayfkyhISFsPxAuQVMobSxen/tO0FnjkPCSR5RGw7P6sepxSkYj/gKSJf0TjI/MtG+UARFjllnkJeBX5uTrZj8NF6bMcsLMaLlnCJFhnnw/wCJ/vhjlh/u2+6fQAJyZT6lrNLzaIyieW3cR4fcl/R9w/0fcd7fOg8oOvP5P5/4dY0bdse2Z5HDcvRyCXkyt/J5DF4KZe1j+31YZpR/r/nf1E/yD9x52sJRqrQGttbcY/qnJEjbLE4sJAsZK/IP+qb45H9WPvesY/67l6cTlcp0PyDHF08PAJLv/IMwJeX24oEf8VO30jTHgvu/0TlH5PuQ/wAV3w/xXdj/AKt4/wCqPb/MpEPzRH/B/ruzjyjHH/HZQj+b7T7RRAu3/cjz/isI/wCMyMR+GLvOt6e2JeS7APBZQMqry+1KMhbLywzGAqg+9fkBMhMVdB/Tm/IcOOp88s004rA5PGhr1d1eE5T/AFfddzbcfzTwk/1b05R45drRaLX9Xj6Fn83dL8y75/mXfN92b7sn3S779Hf/AEd8f8V3Rvw3j/q/Z6I2po/2nbH8325/k7ZA+HHhHr5q2IuFngJx/wCKU+54p9oU7ox/EUEnyX2/6vt/1fb/AKvt/wBXaPzdsX7fydxf830LTpxpbdvH5tNNHS229LHbWt0X3Jfm+5L+iJg+fKZRryH3P6t/1YuXDjyD7h/n9UI0PhGvpofHYGWo/ZR9GHlHhl4f/9oACAECEQE/ASb/AGS2222222++29L+oX7fyL9v5lof4zt/wO0tH8v9CE/Upp5/NFn1diMIPq/p/wCr+n/3MnB/V9r+r7Q/N9kf4z7H9X2P9zPsf1fY/q+wP8Z9gfm+wP8AGfYH+M+wP8Z9gf4z7A/N9kfm+wPzfYj+b7EfzfZj+b7MfzfZi+zB9mD7MH2YPtQfah+T7MPyfah+T7WP8n2sf5PtYvyTjx/4r7cPyfbh+Tsj+Tsj+TIAJxOx2NNNMfOkPGp8aFH7Dbbelt/SJARz4aP5M9KdodjsdqI6RIHbJHf6u1rt5b+h6O8fk2O2X4S2Wy4bs/kna5OfXup2p8MR2BkjUmn3Ag3oLSO+ta7aZQ/JjE6XrMXE6AIFBn+FPedBx2BkjWTIfcWPYRrwkhtv6G8O78n73cfybtGh8FGM+qIVpLw+2e8601oGSBrJ2xPJtGtf1SD+aYX6vsm/xIg7A1rTTTTTSANcouLHwO4dgZf0Qey2I5TuAeSUgu4B327yEHTYE4/yaCXHAT9X2q9WUKB57qQx8dwApoO0Ji7edZSoBBB8dwFpg7Wf2vuH8neWLHSgyICQgW+27K9Q/wCdB/KSTP8Ao7ifJb/J6e9/+ZLk9P6psFstyd0uwyr0d/8AR3j+rvDvi74/mwIMSy/CWBuIZeDoEub0YkiIYzB8vCZO9OKYjZRw7mwy+7hGEEeeWPSw9XLjGOVAsBIhGOf5pgf8Z2IjANwH5JkG9ONaenFY/wDDpkPF/k5o/fx6vOnH5v5az9Gz24vBTt9XGY1QCfBaBSDpGAyS58BnijL+n+BGCP5lqP8AjMom0zAd1+ZFAINibPIPG3/O3hr8knH+Z/1mGbaOIssuWX9GmMiPDvmky/NuX5pNu0/m7T+bsP5u0/m0Wi8oRlxRjVn/AFiy6kf2Y3/sH9Qf92/9i+5M8Hx6O93O53f1bTI+jyfJa7RMxTO/RhPbdsssTEgOMimeMS9X2h+aImBurY5x+TmyXHjhii3KIn051MbdgdjTTR+lbz23rw1FqLQdodgdrtaaP5tF+55edAafch+bY/NMiTx4aAdw/NG3+jvaMvAS7nc7nc7my/5/r01Tz+Tbbfdz9Cna7Q0WjaY6SLiySgeDwn6I/bT9GXhLDy//2gAIAQEABj8Cr/qji+L4vi+L49+D4Pg+H3OL4v2nxfH+dpR+y+D4Ph9zi+L4vi+Pfj/q3WTE+iun+F6EH7p/32air0jxPqk4/wAD6LpfyUAoPVEa/kcX9JDKn7Mv4HpKmvo/s/340UhJ+YYoKfCuj9qj9oPi+L4vi+L4vi+Pbi+Pbi+L4vj93zfAv2S/ZL9gv2C/Yfsvg/J+T4vi+L9piq37T9p+2X7aniFGmn30v7XIXwfBr+T9t6pSXqmn+ouD4Pg+D4Pg+H3eDqfu/h/MyfPuv+yex+bA/wB8avvqZa/7XdXy7H5tP36JPk+L4v2n7T9p+0/aftP2n7T9p+2X7T9sv2y019P5r0emrJ1B9CKffNGrRrJGhpTueyvmx9/7B9/Iqakny/mM1cEpfRGkfPV4LSAfKn3qejBLI9QylUmo9AS5JSdDon4D75HBr+T04ejB+4r+0x98kegdalkK4j7upqB+pqp6/wAwoUr0vRGjSa8K/ePb6PKqvQeT1jUB60ozxp9+rX8mkep+4fm1fNjvqoB+2n8Xoex+QZ11Z+6V1oD5PI/Y6p0+/R4pBaq+mp/mA0E+rJT9+hax8Hn+z9xWvmWr5sMdoh6hlNS5fkO2R9lNPtdU9C/hwPzdP1/cSPizUcXTsr5/eoHkPaAdPxdHxFPLRg/DsT2Hb/Kdfv0avkwR9rB7q6iNS1asMdrf7P4Wv5ubsOyFfP8AmFfP7or696NPUxTyY+XZb4MVQQw1/j+H8ye0fy7r/tFqYafl2t/mP4WpzdksFr+zvxfSl+jqqoT2P3S65kF0Ue/HuQOJID+Pme1Hgge1ofgH6n1++D2LQPh3X/aLLDT8u1v8x/C1glykdiQKkeTpiE0+DSCKa+RaBgDoHlRI7pVxPc/P7oB4OqU6/HuPmwP5Q7j4dvRmpr/NkfymAlNSwJECh8x2k/tNTGrT8u1t8x/C7hS0DVZcvyHZL1D9gfg68D8GE5KenH1+4H7Afsvg/N8Swqp+4onyY/tjvXyDr94qL6FcGrPiPP7hf+W6FWjOKfZ1/Dsv5tQDrlVo+XZKx+Xg1EmpPq6jsnUdkjIVKv6vvn5lg/eUjl1AA83rGsfg/wA/4ORACupNHH/a7geoLH3iBxZLpX2gdPuKeKBXUvTUji6evm+LWT6sKQOBfUljsGGGe1Klw+tfvyBj71f5AZ7Iqws6UV3gp8X08RxH3wqnSWmfLiOH3PsalDivUnsfSjo/tYrV6lkJ/F8S6EULrxfBnH+B4Kp+HaNfpq/3ZfsqftH8H+8Dr/Mxn+QWOyPmz69ktHya1RrxIo6S1QfP0fRIk/b91Sa8f4XEn9kU+37ifk/kS/sdWEj9kktRUOPAHWnzeQkqK+nr28mRSryPEvFIqxUUdSplfkyWfkwaPg+D4tHy7roo0+b/AHivxftl+X4PVCXrF+to6aUq9f2T+PaIeqmlL6lANNDVj+yGqv7RdAHjxx1UfgGTXzftFr+Tp2UTSnF0QupD1LKhWjLkiBFOLyJfUoPpBJ9XRPT8fN69uL6ddHwdcS/zP2P1tXxPYj+S0fcR3r8PvfDuFfsl68X8H8GtY1ASGerQOgX9rWgfm4qenZYNaqGj5oQftfX0/rDWgezwqWhQdavECj09NXIrzpQF1IqXoB24P0+bGWtfN8O/B8PufNqHorvxY7p+8GR2LqdGE/q7ezpkxmrR8O2PxdSXIqmunzo+LP8ACya9qPQceJfsskHVnQfi9VpDxyBPo+FHTEv2Xw+7wdB2BDKqaKHfi/h8X+7ekb1Ap93i9Hw4MMB5K4Bg/m4vMD8GEMF4nhRppR6vl/g9C6pNGRjVRPH0YB+/wfAduP3qsgKoX7VPsdAFE+p7cPvaGj11+8SNe3VwaqvD8p7dPB6ivxo5dAT6nydY0AHTV6/h21/1HqXp34Phx78f531fF1CS6eYGrAP7NWQPLiwEYp/Wx6PVAPzfUMR82cMlD4sff4fd4PXvx+9wer4duL4/z3FrI1C06+rAVGrKlODK/wCUNfV1SAmnEvhr+pnjVjWr09GO2n3OP3OD4ff0epfF61fB1H81p/M8GKpU06kU4MnJheXS/ZdU1YUBQ/B4/wA5x7cHqXo9P5nT+b4/d4Pg/ZeiSyhYOBNfk+k1YaWP9QDsP54fz6vk4/tf2v8A/8QAMxABAAMAAgICAgIDAQEAAAILAREAITFBUWFxgZGhscHw0RDh8SAwQFBgcICQoLDA0OD/2gAIAQEAAT8hZlY/6/8AWbFixYsWLFj/AIix/wAy5ZLPlfWvpX0P+T1P+L3X56j2vz1o+NfSWXxZ+7IRR5lfIr5t9iy+Wg2P+N1tj/8AFJ9Nl/8AVXb/AMg+K/Hfcf8AbMt+1fcuVX7r5V9ll82af82vzWaN5oE8WJ/6jbwsX6piqpUV5q2XzfmnNaoEl0qoOSPIz/8Ah/gVf/wIsv8AjzWbz/8Ag+bP/Q/4VK08c/8ACn/CVyjW7Y90m+i/CzRLmn/GLMVoUGPZNYWS5Uv4Xd8T+kG5YftZfhn+bj2vq/nYAz2Q/hr34o3/APAgKn/Q6/46sFQoWD/jPFY8U9P+IXPFaCwR/wAH2q2pTnj/AIzWjDVnzNPix/zardniszCNcr/+CLFm+hqZHjAj8LLEfK8N+S+vfUvo18H/AIZ8NXwWbL5Fn/mPb/yP/NGr335L9qPp/Fmevwsf/G/5Sn/4CLEBGNm631V7K9G+nTw70n8XGMvgvs/iz8/xRV1zbhglz5sWLH/4Bt7otGQJkK+L+L61HPGe1zF97/N/1QVwHl91PgpDoX4LHpY9bHrY9L8D8WPAv0/7/wCMs+H6snh/Fn0/iz6fxZ9vxZ9vxZfK75//AIEPiX4FGkOqvTUPJ/0oXS9AqWLFaj/h0R07Tr5L+s/itSmRocguQvEKQ+F/NigjixYsWLFixYsWLH/44sf/AJLz/wBFCj8p/H/I/wCRYsUH1UfnLH/J1X/hkff+KIMdUn37j6v5pTg//DiSQ6p4P4q3U+r/AJQoz/pT/qBS6fi/4CtPv/8AE9v/AJklWlUqP+TZ/wCT/wAUa02xEnNYPl3RNkvENmuXsj0yf/xoiTSqKM4aHCfYVrTPwNC+fzcB739cpwU4P/xNHuwef+ALBWu9zgqyXpYJ5sHn/iLBL8rxdwQFioUT1zfquohlcGLNmz/xJyvhU3s1v8jLJp/2ldTLDyHqKGqIEeD/APHxVDEUXyZ+3RuSRJW9rF7fpuS9r+neBTh/+HkEyqem/LRPlFlqazWYqU3xMGKxvtj3dsWGw1Gw1Gh9IfVSSGHJW4dJx1x/+CP+gvFCB4ysp9PNGXEaf8r1hCf/AMcZjz/NE/LqeDSla2XR5cUxT+reD4pwqUOe2KLx+BeCH4f+RL/IuRAtnli9T11Fjiigf8QD2HR908Up4cdTS+V+rh2lQVLGV6PDUSOEpEkzOVH2zWAc34IoCRGxYsVuBrPrY/btLjGfyWb1np8//jVcF7b3/FFV8D21rW9wPF83J/5y/q1/gLwX4JP3SYkJrFMD/CX/AI3X32cxWNPL+AP7qpKRlKKUpMGNQrDCUEQO6AQUBDUWL9Urh+S6ZB6rLk4ns8Wdlz+VAwfVSBKJlUhdh/4jwx/zGJCSaMLgfAqcvP8A+PNctE/IqNMZDyUUcJJWtSPA4vm88PHNUfRr/GXgv7FMFX6R/L/wPgz+bxUxDGD/ADds7Lq5F6GtC9nm7RUn96NS/wDG1z1oUCPVUJ8V3JPdnkQrtGgyufmn8KxlcfBV4JfBzVor2UfisR8H9lGP/wAf8JT+BrUs1P8AwTung+bkPi8fg1/gXj/5A0fv+r/C/l/5+hfixPRxXBR3+zSJp8K5n8Nq4OHzzYEzPXNDB+AtCD8UvuaH/wDAEoP8mtYEZnj1dAjzEXU7d0Z5sj5Sl4F6opyAPa0QQMaSX8dNgHpYkF0PHkpgvzv6/wDx7UFx8jfWprW9mn+ymD+L+i1fiXh/5wuKRJAmMr0Yh/LZqoZkDux0GuFb+axogq4pVjsBnp818rvz+G7ZrQ+w+LESQmXbAVMFcfKonizWmlfKYSs9hyp/mi9XkIoSRTAc/wBn/HioSeR81gbZUmOrIFgcf8f/AMch/wA+8Z+6XtPLhdaQHaTnDZvdPK/orAAztf41ea6RtQQjyQQfkvHgnI+f+APaTcFPyU2n4V5X/wAnF4hj33Zp0uVYeCk/tYqk/wAuLzlF7v8AlW+z+W/4WvRU4TD3FP62Lijpgn8VCfP/AECxngz93i/D/l9t/wDg63uDy1vkcoOPzTUmMDBH/wDBELw3j+381lgMn4pMZDTzTib3Lzv0QrxQEqduY3LQmc/uNm8tCrraJUhec5qjebvyztrKA8YIne1Gn/B/zGr2eKr2x/yP+RUsAWgw7VXA+X93/A92ZFQZHN38Sly2p5n6CVip4KyJ/wCZf+HFb2ofV5l1CcVQj6T13/8AgQD3UElM+A8tgHmnr1XKPKgpM6oeTg83jV6KikAnPLVBHRT9F6frzRvI9UfIg5uSnxZJRQPC8SxQYgIf3NP+hR/NjVm9xbw/Gf8AYsVKODlX6mrHyU5+KwuEk0PMCP8AgHyV1cgdWiD6WtK2KcVQTeYmCnI/+3vCgfKiLJZpiXkXn8vsf6rB9ZZDMTkfPdcfI/iy1PK5g1Oo/uzEQV8sicBsUZnZ9tMZBHCxLg690gZ3H4r5355UQKEfDSwkeDblszx56va+hL3B+ip6PlVv92WIDR4sWEOUTBT/APA2Pzx/CXY+5/BYia6IivKxB4vEuvX/ADbysPwfT6vSGcj9GkkX0L4Er7/8gLpcjE+BpVJ5Pwxv2H/AOaLQyfS/dTE8q/ixcb4aiYPXuvpZccewOWw05UxH+l0Mu0DBTxQgXE/FmflCjmZfLx9VZEAw+e7qq6Q4LZk5WaN/H9r2GH/AeRfYv6/+P+4fqHEdXhqO1PkG8hL5sDkfkvhKcifzTzWz4R8rBZK3+czQIHln8ZVYmI5awiEclSfTSaHIM5fdh1L5pPAKTr2XSUrebP8A77MzgEHinU29BcwyUvnzYATDlnIFMmDykS2M/EYiyWHI8z6oIKAg2zgcfHNmqwIYf7reeSYelhp5YhNik0Q+qSCKM+WXYD5byVS+7pw/LJs8W4fFOpZn31n42Ir4LtsfmoPM2GXEH/ZPYbB/xGvgVHZWIgqfSaQ59x5qDHZtkuF11YBPKSersdXNWVgo82Fv+81VcshrYvTyB18VZeC1XfAP4VQi5iE81YJ+5QYO6HI+7DJd2KiAYOfBdGRwevNXBXh9LPoIeB5sAKCVaJQMjCvi3GFQLg80uABIK+kgAywuMaKHlGwmzDHBn/JpvZ/Dl08T/Jee7NyDQ3AE6P8Ao0+aZsdvusfN3Vj80noKR1YUj4rJEDaABV4Hd1CG7knjPf8A8rDFwCbwER5OK3Jsb7umnsNOdSTZiZlNDHULH5qQoP0qCF4zz8VLEuysAmOJf9WXKqITPxRDkfdUqEumxoIvNQ5HruuPTnO93aYPnf1YAk/LTOfyrOLNZflZnFXG/wDJB4jWknloUBz1eOBHTutC8UYrTmC8an4seyfmxO/20bCFl4opK3zehRYiYvwQzeGn36sAHhoFwMh4oOBI+FeRG69rJxIeq0hn8fNy6Z2HgvbSGxfTujlrJV6siEE81QxeCt7LFwLE0/Fneg8BYksfi9JYFmHFdzZVJzbXgpYg8VDM/FhcXkoAcRQPCwqW7Ca1nR7dCc8aMFjGP4ve1SXJY0+VjY+L5SoaM3FcEoeLB5qqQBjI5KGcheakR/leuHRdAEQfzxZakXdx8VmMV4EK8i0EBMO6qfYPdKoxXxCyGU7sVvJ/5FkvlLGp/wA+2wHVs+L5FlXC1lo/4ghBZzC3lhTMt8XgSiyLI8lVvLypE+byxvxWScDQeSx0VKNNsPV+t04qrjLXoWvb/VksoHpqpH7XMgG+eLMD/MUnLlftReY6OfioJGTDpO5ijC0GdlZH7hSpbyTF+K5BHhxXL+qHKsuEqHlmy6su5dezsNNUuLA3unZU5IqD4r5Ck/a5MP8Ai7o/NZ8pfZRhI/PP/OcsKj5snFEelE4j80ghqDD/AMS8VmKyZsLquL4FAcTeTVDUhAj4bzdAE4ThLHdiRJj8XhAfIa7H8UDQhxwk7ih3INDleLDw81cJEuaaJZv0aszIUY4NhOzYPLegWXmuymzXyV9y+ty32ael+qOYKY5rGW/7hvz+JSoBtR9TpQ95Q+KGcWGwHOU+G6qPBYzEJSEr4Ua6svNn1Y2oUByrEQnzNDIeTBNOYZPifFhRcbN5wix7iyHT5RSh4PieJqJZCTH49V9X/wCDKw7/AODXRoOs8cX4V6y+rpRdUOiei4f2bKSK67d7LJ0ouReKo+6+l+rNqLL0Um/5Nbq7lUPVn2v3/wCHq15loxmbyUbptLJCKZdeqv8ApxX24CO7z9NASApqgcHhX9f8HF/x/wCHF6py/wCvF5V/55n/AC813vCjv4/521vB/wAN5N6P+PV5XzT/AIH/AIL0XvXu9lLwL1T/ALH+D7vOvD7X/9oADAMBAAIRAxEAABD7rzGHt24v5v8AUHMj/wDg7wIA9dshcRvr6jEeIYGcj+zSay67KnsPbhCu/wDmeCFXoyhQzWkyPX7SlIalpmIi4J3v/squdMdm1AMDP1i8JvWdUeZcNeszuoImZu6qLVShQVP8JPmhzkVu0VKoGWzKf22rrcWD3V+svPicRq8UpMzMSOPCR9e/+Oh7kT9iBhbnnA0/7v1+Z/DMzMyIEMkuG+nWt/6j6jh/v/ZvY5RGivqczMzMNKdjsGLRKo/hjd44vp2dbqsPFJl23YnLzMbwBl7pD1FUzJrZy2qqScVH8wSoSfoG+q/Lpx19plyG1CevXuRAwbwrKzaCCkZIRkEapF2xo5u+WzJ6hZ18zOWeZpGhLAQnMncZfaagahXaQFyy/lXITmvF/uW4wsseVKMS6pAA6PrJVRNR84ei+Xt5qg2zstgd965y/E47R0yU5OlO8tS8U7elWNPa5NSPweDDsDHMMzweECHBIDLC8MERzz/BHyHPzPHMDf8A/8QAMxEBAQEAAwABAgUFAQEAAQEJAQARITEQQVFhIHHwkYGhsdHB4fEwQFBgcICQoLDA0OD/2gAIAQMRAT8Q4sNttu7IhDQvtX277N9nz+z5/k33S/Lvvn4Nhvm39H7W/wDxfqy/Rl+rLc+4j6rP1G+632G1+I+l5Psp+gn4RZHSdOiPsQfQSH0LD6SP0sbmViD6z9r8h+8L6rH0f2vvfvY+pMm+Hox4XFnnFxcWlp4ttvnFss/lELa3HmsrJz1D622Jfpc3NyS7LBsWIfHH0j4BfYJUcXbf/u/TtoDhv3tfB/ez/wC5H5fvfq23/wCrf/q3/wCr9Rv0G/Ub9Rvsf3vsv3v0m/Sb7X977e+zvsL7a+2X2S+2X2ov9L7xfZerfez9dPw/2WL5L7y+6vv77mAKrDLUKPEnItv6DzLh+db6XM363w/kz3bbb7ljZZZZZZZZYyvrY2WNqTPn8G2+ArgLIjjE7pdIzbkAT+WwII59HfdthiN+YebDzOPiyCOy7XZ+cdScv4tAjXQsWWWWFicX+YQo/PBP4zCb23838onRhv4Nk8Tbqoa2GGG7bAWa4ncZwfNkvdnLvf48eGRpA31evCO55nHxPh/OOp7fC+dqZznfOM5voWJiGe7GqEXBV/mxmHBL7JYjxvm2zLiqPxaj3vD8fyw9CDyvm0Y/13EpP7TxLco/K1mV+k7r5nGfUhP4z8x3IGJaJ9LPOR+WBD4hy+zmv2vj+bx9scu/iE6b/Fr3D87a/ECMvg+bfF8XRc3jZ14QlgM/MFvnJftty2p88ckOrHOdZcv5ysP50B1r7zMhySvJxcVOjv8AKS6F/HhbyW4b9pC152B5/SzyajmXLMf71gADPpssnDt6iO7A5D/MLXL+cJzohvPB9iKzZ+rL+j8iYlalpssX81mzYsQDp5k40+d21C8R4vRywOXxMW7X7SxxIPxdfHqPBrLJg/EQOf3/AADQZ4/rHD+cfm1dwZ9IC9wY5D4lZ5nazY/q/QttgsNJ2BuLn5WZ/gl1r+1nHJGXB3cndsvg5T84Tol0xBvSWBffm222223w+O5RAE/mzeF/e51cNkm9S3JlxfBImJzM1/AucO5AHh+diubm8SaH4vzX54IckdAe21+bbnJ9LYx1L0h93/Ex2DhYs5MPsNyC4ABSA4BnTx/GmLI7j+RGDc/V4C0BfM1hi/WOpB5+kH1P3vss/WjcA7cb49RkIfk+D6f7t99/mfpn732v7lgnOH5gaHMYsHTyfzPM/O1BeiCbxHhfeXq88zAnd+JHBjODtuIzyHG8fwiQU/uluD7nxYfAfMIV4fFiDidnn9rt+Dy5x/EDd8wmH8WDy/J9pOAnI/kx+bsGftBcCfwf8viH+ccR/EM4zj6baF47/N+kWhyfstH0lv0nUGe4gDNzLT6kv2GX6H7W9Hs9FCnTcauzm2WP3ufvOC/ygEXn1/KGhQ9H0jBO8tvY7PmXslUCq4fgnXtPW8Jc5/rTwoB8cWZgJafJPJKdYM4maHTvhnjPoeX+YCeP1sFxf0rP9SMf5lc99VNx+C+o/v8AN9lYjzr94J5P5LLJB3844c1fe77395+s/eMeSfegR/ovgZ/OZeFfwh9if1gc438v+yD5P1219f6yB/27IKfnfyv1XZ7Hb+YN1OfTGVeDe3n/AHOGNo+a+k/lZzj/ACeZX2/0kRj83Vu/jv8AaLr6TANz29ymLvy4Y9j9B/5ObMn57t7gMfyj0HAd22h+8+MhPmTJHsR8R/gg94kPw/vO/j+sH5WF7X83JxcEw/WFs+AxXAyfpSy+l+ROO5g+9v3uPrafa7+SRDhJ362v1b7794Hr96Jguls/n+kqi5xfZLfBEA7L5zvcLX4j9Zs0RR+0x5TKHLnbhJvZPYN2ZgzmwvBnMLdhmGG/lcjwJ8Pz+THsX78WAV5+0HwMfrzdqWGcOnP6yx/5s+1Y/wDtw6N8GFv2fta/b9r+fOLPNPls51LfrZv1uHxcQD4J+hZHLpJX28uvm4dWoXzd/BGFpfzZ9yz7lq6td6h6DzHzD9oEwf5F8YPq+IXyvtKeGG+v6rXbYbfOhwPyTku90PO6+UfN8eD59Pi+Lv5+L4uk9ETfF8R158R49+HhHz58XxfBdZ7Y8Ivjx9btv//aAAgBAhEBPxB3rZZceZ7xcXFtttpaefxbaWPH5bX0vyeMeNbWFtfFtWvWlpHjDbbaXEeF/wCz/ln/ACX0h/I2vhX8/wCb6Zv5c/2ku1+1jYyfg38G2ttvuXNlllzc/hftZBY2WRmwwdTsv2ttt+938wWGdyNlnoY6VyBUH6sK8ix/5v0ZYi/22J+qX6Bfryz/AOLP/mz/AOb9Qv1S/UL7j9r7z9r7j9r7n9r9Uvu77i+4vvL76++351+dH1mA+t9p/Blz6W+yj5Zp4F9jfY3299jbABIyyVL+noMmXZ+fm3ZCXx/KX94sssssly0tttttLbS2x9LH0sW/a/JZ+lu/jcO7u7ByZvtkkbr9oo8ib9TPEPcw8mRzNcd4k4uE59emD73w/K/2jr8WbgnHzKPwr9f9LlnH40jjWeXfp/BkTksDBbRF5iGfhzL6P6zQcMOgPw5Ificc3JWqPx7kPvDr8r5R159F5/ReDXEr82Z7lk7nFrtebqDjxlllkcJs0b9o/K9/FjJOzwLEH0gPkiWMCPb72n8b+JOGTjIdN9eb+c1C+UeDksvst1/gsjqy49936iF0+RPxZBwWWWWS5zHzEhPqtT9pHuK7BDggj+1K5WfaFaMIwFibsJ2h+MbzJxBrDww4lefL87kEhvEdQcX+tqdC/RyIPHWeHVpeMWLnhIEV3xjnEBoX5SvbZThQPMjgeN/a3bt/JC6CySVM7OYQHFk9eN9X4MbzIPhIvJ+Dlsj0nwHj6Rk5gGz2d37RwcZn1tnLv2geYtOzbZHhBBwRxMbQHKfGTt/jB9AN69L4ndnhDS5TLLPX7z5uy255jBzuEDfrZPBAx+WA1afjyBowEJuMO77CWMwlxEw+mWWLuGwDto7asDYS492hyH8xnbiVTl+zxEOH99gwgka4tevGtuTDHhxw36SYL15H0i+wS8ei81Z+q+1Pz77j+3ixF+beB23Muzhj+xYBzdLqSwH52WccW51+8aNGEUyx9I0Xbz85+cFaa/2sJzhcHHLJWuOfm5ovsDiYcUfzsAEneaH1u8k/rH+r+JD2rdgH83wv2TzCRaPfNn0Nh8Fjah0+U7NpwcMf4+ZDXIGln0N+8fcg4/J6k0RWttvhHl/i4HG/SUsKdv1uz8BYeMjDxbnYkB2Ycm9whiA+jmD0n7WHGthJxz63EYr9rW/ynzCAd9T8Rfr0llqr7c3xP0vzlzdfq8XAKD6Ep7fDH1iV/wAwHYf4mCaT8ELHi35d9kl+ktqD19cgYwD9HF0kGdsccxiY/oWDAA8DnD6bcPKIW8Uf1voFv9gs3PmETbgO9zHOH1ifJiQEcvUgnz9JQVH2LAQX89TDlfUsGb4/i6Mre75r3YcyuiV9PAfjYOzHxwD4Pd+lAWfawsLiTz+PPyWqz7XP0ufpOnxGuy4+lx9LPoJ+gX2i+vb30LP1nHzb+vk+9Z47BT4nQ4Qh1PtCnTPzkA0+q0buv5z0osORj3w4+89g4+lg+IR8X5C/JfktfQ8tZn4Nbn3+LlgR7LX6lz9bn63L5bg722uHY2LNu/FlhZa/WRWNz5v2t8wuZzZvqD+8/AuR8BuTixOxm4Nhumuzs/a6Hp3N8+M+H8Hx4zHbMR49+Mz3HX4H08LvfSb4nw8+nh3um//aAAgBAQABPxCKNRF5VKG0dWBazKZjRUgqumvv/wAJ8XtW41wX52HbYPNSpFfIV7h+aj/sL/8AXsX+y7RCkoDLUv8AxU+79VLgX1eKf8WCLEe3mqGfvYWF+WvVXZ/SnOh9WGJE5lVzxXpflqufrbPz+Vqn+5sJ1fmkGT+bD22TKUS0dCxtSvNXma43Wyw2fO3fhUBx+bL41SYHqvg/CyZh+Krx+Kw5B9FfL/BYHM+rpv5r5/z0ISz7oEz/ADFcs/kaxGn5Vve1t7/NxytB5CvvFFTbPzYcrKiXbkaiqwVh3UjlKhJTyVB3cmlUlA82DloY6ojBnVkyQ+K9WKIuvxYhTqxjNykeRAlPgATT6RwIfqq+a1KxNP4F+qwRtNSbCc/Nhoqsy5USCWpcNUPdlSqTNAR2sFVjLCEqJpKb1zY92BmaJRPYUXu/LScu1SKSZjZvSDbo3+L4solLLdFj83Qwx8Un193lo/FkIH8FQxU+OKgmf2mpdUcjKkc1PlKxpJ6rhmakROiH7s64SlnyrlVqIRAg+4/bRJiX6XmaRNx6fzJcT5sC85RH5gaQ2SDM1Snm7hio7bGMoXizmVmAubIurCeP1YCEUd0vDMioDZqIafRcdL9VfJ+KpaqZ8vu9Cfw2HhX5qF+FLcijCtSlJYOz80HkP3YXO3ccl6Ca/d8QM+KAZ/7d7oebvQ0mqfZd48aOV4+rEdWTMVfJdQE/FKQCde/dEOLLxYrWbiUr5SxJD8lQ8XxYd4oQtMEoKFr+BSP+2q8/nsrlQxNHxPzea0LOKgvCyv8AdUwgzG1OPzsbxSz8g/N/+C1Ryvkvigct2rcE/MVVw/GLCcv2q/IjyuyJPtTHI0Zkf6uimlkpL2+6HMvP/wBq6Yfukkxfdm6WydPxR/8ATVuE/DWML7WMIBY1D3Nf/GVX/WuNCTwBWVZTyyU60yCvlP4qama0m8WCOK51ZYBIz8ll+Ar0rkycib/4lXp/AXFwHBiQTiXGV7FB/awkT5iX6mq00EgGVjrb0/xWP09yF9P4lT6/G+j8Sng/G+n8KFg/8CwOvwp6n4p6H4o+Ne1+Vh/3LH/3VP8A7rH/ANVXdJ/rs/8AyWfN+Sn+aVOo/Nj/ANVA/wC1qHIfVDpaTjvta7WvHB6LFAKJ/wCGR/8AGn+7Jedf+w0+CQ34cXd8fzUvHlw/4El/gUoQmK5NJBUmsV0ZCmLCY8VPixsbGxoKVH/gqFgqFQsFgsFhUWCwVr/yD/kl5vn/ALJ/zRl6i+I1P+I9VptpE973j+SmDJMJPUKNqU0Ox/avDXYUf1Tcucjdv/k/SrH/AOA6VWiDqvmldb0n+rwHwD/dz/wfq88/hfJ/gvNvnwUAJH2F/wBYKpACfht83+D/AFZ/6gqqR+iziQsXy/nLP36mP4qqESvKtWv/AAmtTRCkFQBGR7sDRVEFaMwhGn9TYNgSBp/BUwhzzEahFM/5FixUqU+RYoawNZgxXkN4asoUjHCWSPmj/gBeS/JTtQhEiP4oJBQ59aof2fhrn4Cr8dWyWf8AiFC8f2UM8afIpFE7YvNhNaQXZjblWiZcDoKZOMWTsTCM0IYm5aKKeSvGEsxoVjL/ACqMoeqU41CVzgKaSHSV+RAfTWZJrPlCPD3/ANZWbFfLK5dqAhPikghIw89q9McY9k/3WGSAI+CCMdxX+57Dx58Ksvz/APhipdRKQcseWy/IbKayl/8AAfdRhQGHqSYuGoj4NkqiymhTqTGjy8Uo/wDna/xn8Vfgq0aP/OpAeiztYyFlZN+qBLGPRTjfdiSsJbIEUtAopE+KlGyYIHjhzvxWQ4CoOh/Nhd2fm9gtMqI+7ITOTYU6yxUIimMRkPaiPYiR2eaKU4/DCFfO1LBQvb4qRvikzgSY+aeJs51PasEljpc+MA6TrBVivCNBR/8AwRYqWBMGF9LGP84oNJfol39UQB4Lz/5DKwGk7bAdYevPBc/L/mv8L+L+pSSsSBpPuwEqkzj8wMfizXgZFPyZLDkcfUdD4pm2s6+D9/NPZcnEuKZmx891UKhKMjk44/u7o8p7CW+0w9VKXoDVZQERRPZQOW7VEdKHtK0St/g1SEwE9JZBmkQ144FmyoA839zSwx7GbClOH4pAXqwuEOZ5KW7EN4PCJe580oJlWTyeag6EJOO0HR/+JosSv8kszOBn8qopAjsi5+A/4P8Ay6OIIUdqSaWBqy6Kt/a/4d1V+KnIxFnnbc1GA4Mcwc7SHAgHPAdf8jBCr6cfA7oZjOFj/gBtX++TrDDJ0jWQWMLTKM82UMCfYbUNHYcRxFKc2c6gpUoAvSZV+FNGVPSwpJlgwiyX7sglMODHmauaAayQlBy+GuDAPAJXe3EA7fFftn8rmIs7DWAEoR5Sxj8VycSNQqpj1SNkpHsmlkZBZ/8A6nJYzkJf/jQlTvOitMVXn+KQ8FC4XIn8Vi5Enp2qxz5oIEOHE65sE5QlT1M1k/FP8q6v69M+wP4Un+Iw/wCRpvveZ96onbQmol0XQzCf3VAVeeVsfBpeMOn3EH83VELz9NjJpKXmvFwc0MuY/TZe0CgpsmBKU1Gh+65UlkH81yTmn6p4LzJ0h81q+hrOLJW3RBAiN2uH4/grpZL6n5QqQMuAlfRSaBNYL+CWDuTPyqg9z/8Ajmr6LMHn+K5Yr1yIHwLFdXj5rwCcajtRsPPNl4vkU/wjquqzTCokLIy+MVIHqouw6H8kWNE5Snd6vXBKfP8A8KmjFNTx80glRckq9CX6q3kA6lHxxZeU5SlL4erLhkjKnyPP3ecLANZcrHe/5rIaZ4qoKgTaawrBvteN1ApBcgXmrB7JKkXOefmw9y+KsGJYOqAxQTPtogPAFTdaiHXKECi5ETmrHB6LGAhiFaHjeKAAThD1ZYxUoQb8vFM8PowfTr/8fywh+rg500w/H6lqtcgfmJrmvj5shvjTE6sBL82evdU/4GVFfP8AD/x591S7TKCkUmLJJbLA81L49wuT+aJc8s58lH6uK57QEjYR28FNiFR5Q1lB8pBz4ENmo5KcBZTD3Dftb8AaE+uiqdN2gJPX5Kw4wP8AdlVHebgVSZRaBp7Hmje2SWZGdLDSH6rIQCMo4zyzyWNAI0ONDQ2jdl9QfiET+6vfoea0aPAzPnloityOJf3cMiLr/wDHHTk0+qhJVxJ/YRoRu1YOPXL6rNPqMiA9hvms2zDF4WO6QBMQ9Z8+bMJGgd4o8/8A86Vymn/46NgWEgKCegGwIAnoNWdLC3le1aqx+IH+bzdqEeixSRgVgIeVlHKVUWeu4cPqjk4oCY8HgsvP4iySAAojKyj0/wA2STv+1OzgYQq92XqfAf3VOv4svwA+/wDNTw/sf6q28oMP1dHwrQobEJb4NUz8NPiSn/DPhJZiYQFE140+H/tgiOymGapak1Q/4EEx5Ik4KDAyYca4YM1K9kPwROkj/wDA/JU/dk+W/CaWAufDAjF5bHjYRyV75fFzy5Boc8M8MTtmuA7qzAzLTEu8BpJ1HN17s/MXu6phDWQp+AawgpsJEc/4WRIAfAmGeqcYQYfHyUkukEA0Y7qJCI9yRSYqCA8nA+P+HOrLwo1+WxkpKMn+JtR4hF/JND/iP+pDJzZEzxCRTB1Qoix4iVkMEsdXw2yCsidD20QETlHkg7/4aV0NeN1j7n8Xi8YvxZF0r0CycsVX4qkPkmvNAT3PT7owILNkhPI8RUI7KSOciP8A8DO9z7JqkRR/KOiy0XLzayR4EqSDg8gxeKESONqA5KLGHv8A1S+DyEjkp3eXRkZ8h0UOGAHfGXjD2ppoR5PZ/dRonjSER80hTHOfOsjznBmkcSmS+/NICQOJnliYay88gBYB34/45f8AHC83yv6F9FE+yKWZzL8MoWH/APAawQnHlQqJ7/tTBJq/jhU5JsniAn9xlmuwDDER/ZQQZmtDuRXgOQaahDYygE2tJ/D02fdEr3QntvGVbCEcRlloLHRzQ48DM6cx1TPTiSEC03mIpFnqyd1Ec1faH4yxuKO9jA9CxM6FHpvG7TMZqSPRxYoINvw2oLOP3s3lIU4vuiJKllYwyvCCjQdObEr6MTT9X2f2AHe+bEmRCpWWyn8WYAzlgNTH+7HkrOhlZGAAkBJLLli5KIvLz9HqqRndGBRP3Z5+ev5iqT8wfw2FmpBzH9v5FTIaEcIkzUIjY4ASQwV8cEs/Pm8qB5oE1KcsbHIP0v5scINvsYqWfD599+rM/A5+U/O2NgUR5iOIrIcwPyTYj5GtCvKx/h4rG0MPDHZyuyjnYUp8ZHI+amSGTL1fRZoeX1RHhrHsTlBuLdvJPieSjocAdLr7kqFDrn6KiNpOdhnmU/mtc7y+QLGoLEI94kH7sZM1D6soQIpZAAdrqzRKLYPaB6OCxpknneiEUJYFRJGEErB1pTL0gfy0+9AYCXcx1VMMArKB0eLlw50jhpwX5qKtZkBpgLrSoANSY/VUuBAp6qc2f5UfPlPkFhiYfW2WdZ4WljF+W4MBj3Y5s4z9P+PFlsSQJo6PiuCV+/8AN/xgXxVCP+R0l/zEPzYD9F/2VVKAqJEGDxxZB36WEF9c1WQj80CQIBPUJqC2sDxh+a0eFDFu5LZTM6Cc+qBP/qWmDIJBIHJd1I5KKdCjA9wNEh2sFCwRTLhbBBY8TpMIMNwS3yl4Dv4pXaIX5oZiQkByZdUqwhpzvXq5Vg8tgYCCATonfdmAYgug4/PNPzkA6AZL46r5t2E55yv8nxOL5fXJB1MCKKkgZq8IiIe6YGgzE8vt7J0Hh4pF8jC8RPdSlyMy1jxY8xCXpHZnuzmGdAJNpIChkJGm+rLUBKoF+Xm4VWmqSwwWbsnhNg8RikJ2UT4RqcEVw5hsICkYrw4G+LHFI5Am/VV8f9UQH9BS4pT8XCj1QPb7rhy/dio1eccIEjXunhKSIWBmkZEeFwxwf7psFAcBRhHwVxuWJzAMB80nMorpZyb1RHRQO/Z7uskTtBg+ZaK33LCG+fzUhigmIHnioKWFHoQ61XXPA8901PmgJBAEhNUY455uaTowTlIMnpoieWZHypp91cl8Q8B0/wDlzYYBZPEPqeaCaGXg7jzQvLHhny9tgGeg8KxqYDDkmfMntMr03gB+jqp4JAAQdcUyJA7Kn93zTVF90B9U5lxWRhjR4+LHzF89fi/pETSdUiecqwenzFKD5HOUPAMPG0Y8YB8ted+6DcAvoozTKJ6BP80AYT4SqZe8oTdfVjkJY82b0wHM/wDYIvIP7oodqTJ4bPXoVgYdsidIj6eKqI0Y9x16sikZf8/FFhVRH55+6LVBEaqdfFUomSVxOHcFdVg5hFH3Q0ahHkhAJ7d+bDNkGAvuIyhlWZxDqfFEMPFjB0lV5RLmAJX3oADhP80aaYMC5Z6JoAKXSsCidqUGJN5+R7aXSOSJDko8NgMh3nh/yaysjBEoZ2eR1RqW0UB47r7pwJMdv7ucIMkmV8UKiB+qtcWQyWepIvAi4JI+ySK6Kn1JI9FJFI7V2lK4GylMsYwfbTISPcjQ9frFOYH4bnAn80IaH9fu5BohzHRdVCQP91/aQ4nj4ofM4w5dD83oY+i9dH5KgVh+/wAVeHMuA9DSbEmQcjCfiQqtv1z8k0A10BZV9tEJ/WyVjHeLwIoRBIxeqtao3IHY0ojkJ8k5D82NEBYlHLxYjpmR/wA5yvsvQAtl9tYc8cGSMzPeKWO7yzj5jqaQgMmVyezzYU2AJOt6Hv8AdlNOJJJI3oa9NkhxE9Po6r8h6mcu4L7h0PkqoqQYRsdPqvJ7MsEninJHSZkSosRUxQNwDO7+aZRwPY5qRP45fuxEA9xLTiT9lMJL74sLBd4jPqKZMj5yjJ/SqQgn0xH4rmYJ725Ea8lUkfSkRRIX1ePPnxWwn2KBweJZrhQclh4A81mddDZfEsVkhmSJHR4sQCMkB/VnDMDFOPzZQrx0UTz+aLlTYhVwwnHNlZS0UhE+GitKDlP9U6AFgAI+Sf8AhJwRdzJRgLAchOzwWTMrpYc91VRlscI93WC4fRM81R30vMklfJxYogSIiMiyeZAZMb5D/JqjcGWQzxBxZOGfHoE/SljgiBPMTXvrxQniQcEjzL0Tdsbg8DpU3wEccUhCoebCg4pNILY+APqpsAPmLHm/iicr7oBKfHVU4IrHl/NUA8hq1iI7ppErs+ZGoHGz5vAng7igISvmkAlPosqCI9JrAUDvuuRVX83tLPmLsQnzBST44GMAsiKKagSp92Y7+ApOGjrKGBm3AFIki+x+Kbh0gzirQD73lwv4u6EHrC7BGZ+aGR504s8B+iGgJP71UGZImgz8WZ4w8pz3RmLBDX8Kq4uC9iWPzdoP6UaTC8I6XYe3lsq5HmHQ+U980PU6zEOJjhK0yUjlLzjZhjlmnxqX1FbxjAUowBAk91FJlHN5Z9VQMWfHViJZcwxHxZTkHtdqcIfSlWikPXNIlHs/uymemnPdVyWPdkgl0xz+K8+jMg/dSSp6OryYrKlD7sM/QJWcYRxZfcR11QzQ94/mzJx+N/is4qX0RP206iFiTIbwWY48/VQQDuZ2tWWHwWK8/wAZY3JvGlKkr4DeYyOE2K1CI9XvBvTZCPu1UkgOs0W8mIPFQ/2LPCBLvc/GVXQfmaIQB9TUConxWEF4UNsWBA35JfP4qeOMCBCzIh81sqgRJww99VMThRKhAerNiwIHEZgHiDh5e7rDzATL0lfbDgzPPhawBFnIR/fqiIkgcUyJ+aqgbG43QQysgZfJxWPOH4pEgA65qEqXtrFClQ6dYyte1aqcj2AWZsvyrWJp9ZZQiVYMQPu8AyvWUQGkOWlcD5r5fkaYTA+k/lioZH6/1cJo7RaQqLiSHPE0c8nvKk8fNMxv5iyvYeKb1K8Pwv8A5VJn91Si3uNrFXwGyrgHVDGypE9fugIzipMiPRQmQp44oQow/wA1pMTZYSQYrL8QqvPNiiLyCMdT2b5o3C8Sj6ZRIBA5snXcnxNEASAbpGvgj7biFagU/WVR5tKkB/P5sSwCZq9rvyOSqVh0Pm8/9mWBTUMLMTLD+6Ws/RUcH5S8+z5G+kPRZCxN8rNTZRdmKoUJk7aM6PLhTIzXss5FWI7N8FRwbSwcdkRUefgdqRRDwk1HZlBBWyPzWwhXcE1eZn8VFk/n/wArHr5AoF3vNFAxCs5yPmo3xVLDuqAG8tNJgnZjYNU/dVDDL2JdmxbFJrAJAcEUGCS8UvlTk15Uj6rTIHG0CCEdJJ81IGe1GSpJehF6Q75Hub8OoePk6pE5Ch7Y38VOAfnGRHxF/Rb+l/zjem8/3Tl8f8B383rX+d/g/wCOn/A8vm/tF4fN/wCDl8F8fFf2Xj9X9RvJ+bwb/Hp/BTl+bw+V5a/w/wCTt8X+re94v3f6b/Pf5K04Py395py+bw+rzXkv+X6f8r/M9N//2Q==)





### New school Construction




“_This does not look like a school at all!_” is the comment we often hear from visitors to our school building site when they see the various cottage like classrooms scattered about. The normal big multi-storeyed rectangular school buildings appear huge to a child and therefore inspire fear and anxiety. We wanted our school to come down to the child’s scale and soothe, excite and welcome her.




![](https://lh5.googleusercontent.com/Cw6eGGzj1dNZZjjeB1ptV9VfjOVxbTVTbunUeCJkp5TvPbHVD9KlxkYMQOU2IAGOxCf1hW5xu0NuwvCpWqvykl4RfZVGRjKFYG1csDSe7Nhf5ty2ylIzGMTtxM30ysGWHpiQH0TE)





We have also tried to incorporate as many learning elements in the buildings themselves. For example, place value bars and beads and fractions are incorporated in the door grills. Geometrical shapes are shown by the different windows and floor patterns. A clock is part of a window grill. We hope that these buildings would stimulate a child's curiosity and creativity.




![](https://lh5.googleusercontent.com/gTVwX049fd05U-_a7HgtYiCmSogUeb2Yoe8Mk8AC9U-Enj-4oaCi6zHyN5h6PC7aJX-f_suz9uDwnIGS1tPTkcNQyyreqVPrrdD7-o7ert1A4m4mqbVpK4Ucb6hhXhOwC02n9sAV)





We are yet to construct a library and a kitchen and we are still raising funds for this phase. Here is the link to [our online brochure](http://www.thulir.org/wp/wp-content/uploads/2019/01/Thulir_An-Appeal.pdf), we request you to spread the word and help us raise funds for the new buildings.




We thank all of you for your contributions and support towards the new school! We could not have done all this without your support.




We wish you a very happy and a meaningful 2019 !
