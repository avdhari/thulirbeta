---
author: nimda321
date: 2013-01-28 08:05:30+00:00
draft: false
title: Thulir Newsletter Jan - Dec 2012
type: post
url: /wp/2013/01/thulir-newsletter-jan-dec-2012/
categories:
- Newsletters
tags:
- News
---

[![](http://www.thulir.org/wp/wp-content/uploads/2013/01/P1020960.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/01/P1020960.jpg)

The year 2012 started well with the campus looking very green, lush and prosperous! The organic farming group were at their enthusiastic best and had worked hard transforming the campus. Our animal family grew in January. We had 3 mother cows and four calves! Coriander, cowpea, rice, Horse gram, black gram and ragi were planted in the present campus and the new campus. The white flowers of coriander were like a beautiful and delicately woven white carpet!


### Pongal Celebrations


Pongal celebrations in the valley are always heartwarming. It is a time of visiting all our students' homes, interacting and festivity. This year, since we had more cows in Thulir, we had a Mattu pongal pooja in Thulir itself. This is a thanksgiving festival for the cows and the students did it in the traditional way. All the decorations and offerings were of natural flowers and plants!

We also had the kolam festival just before Pongal.The staff from the hospital and Thulir and all the students participated. Soon the walls and all the floors around the buildings were a collage of colours, designs and creativity!

January was also a month of harvests and team work.Our yield last year was not as good as the previous year though.


### HIV AIDS Workshop


Julie and Jpaul , a couple working with HIV infected children in Namakkal, conducted a HIV/AIDS workshop for all our senior students and staff in January.

[![](http://www.thulir.org/wp/wp-content/uploads/2013/01/P1030317.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/01/P1030317.jpg)


### Manoharan


This year was a sad year for Thulir and us personally as we lost a very good friend and colleague, Manoharan, to cancer. Manoharan was the person who urged us to register Thulir as a Trust. He had taken on the role of Managing Trustee.We are still recovering from his loss. In the meantime, Dr.Shylaja Devi Menon has kindly agreed to be the managing trustee and another close friend and colleague Dr. yogananda has joined the Board of Trustees.


### Classes in the Government school


The government High school in Sittilingi had got computers, projector and some laboratory materials for Chemistry and physics.The teachers were not familiar with the things and were unable to use them. So they asked us to help. Through the academic year, June 2011 to may 2012, we conducted sessions for classes 6 to 10. We conducted chemistry and Physics experiments, we showed them good documentaries on our planet, space and wildlife etc. Sri. Ravikumar and Ms. Kavitha of Rishi valley school and Dr. Yasmin of CFL, Bangalore guided us with the experiments. While at the school we noticed the difference between the children coming to Thulir and the others who don't attend.We have always wondered how effective our evening classes were and whether they made a difference at all. We always feel these children have a long way to go and have so many more things to learn.At the government school we were able to perceive that the Thulir children were more confident, had more exposure and knew more things.This was heartening to know.

In June the headmaster and many of the cooperative teachers were transferred. So we have been unable to go and take classes. We hope to establish a rapport with the new staff and continue these classes with the children as soon as possible.


### Guests and workshops


Katrin Jansen from Germany conducted a workshop on Ninjitsu and another on body awareness and its impact on the mind for all the young teachers in February.She also demonstrated playing the Japanese flute Shikuhachi. We never imagined that sitting in a remote village like Sittilingi we would get to see/ hear/ learn about all this!

Dr. padma, an opthalmologist and Dr. Paul, a surgeon from C.M.C Vellore visited us with their first year MBBS students for a week in April. Dr.Padma's class on basic eye care for the students was a lesson not only on eyecare but on good interactive teaching! She also checked students' visions and identified students with problems.We have to send them To her hospital for follow up treatment. The Questions which came up during the interactive session between the Thulir students and the medical students were very interesting. It was an exposure and eyeopener for both sides.

T.Jayashree and Gayatri Devadasan from Bangalore conducted a photography and video workshop first in January and again in April. The senior students have each made power point presentations of their impressions of a day in Thulir. They have also recorded their impressions of themselves and Thulir.

Arvind Gupta and Suneetha visited us for a weekend in May. Arvind Gupta's hands- on workshop in 1992 in Gandhigram was the spark which inspired the two of us to work with children and education! So meeting the two of them and interacting with them closely was an inspiring and rejuvenating experience for us. As he was not very well, we invited only a small group the next morning, but Arvind held them engrossed for almost 4 hours making toys!

Anita Vargheese From Keystone visited in July and talked to the older about their knowledge of the local forests and plant species and the importance of documenting this knowledge for the younger and later generations. She was impressed by their knowledge but they don't value it or consider it knowledge. She initiated them into documentation. They have started this work but it is yet to gain momentum.
[![](http://www.thulir.org/wp/wp-content/uploads/2013/01/P1030169.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/01/P1030169.jpg)


### Workshops For adolescents


Preethi, a young homeopath from Payir, Thuraiyur,volunteered here the initial part of the year. She taught the children Tamil folk songs and some basic bharathanatyam dance. Anu and Preethi also visited the Local Government school and conducted special sessions for adolescent girls aged 13 - 15 on women's health and adolescent issues during Feb/ March/ April. This was a fantastic learning experience for both sides. We had always thought that in villages adolescent health and sexual issues are discussed very frankly and openly and that girls would be familiar with the changes occurring in their bodies. But we were surprised to know that with TV and alcohol occupying all the adult's time, middle class morality and modesty taking over and superstitions oppressing the children they had no one to turn to with their questions, fears and doubts. So we were overwhelmed with their response.

Subsequently, we did a 2 week long life skills workshop for both adolescent boys and girls in Thulir.The topics of discussions included understanding oneself, clarifying one's needs and wants, values, communication, listening, relationships etc. These were not lecture sessions but were interactive and sharing sessions; exploring and clarifying with no right or wrong positions or tests. All of them felt these sessions were very important and necessary especially because..

Psychological problems and attempted/ actual suicides have become a significant problem among the youth here. Maybe the rapidly changing society around them (changes which happened over generations in our families are happening here within a decade) breaking down of traditional community ties, new and mixed messages from media etc. and the general change happening in our country could be factors. We had one former basic technology student coming back to Thulir with acute psychosis. He had initially come to Thulir as a dropout. With great perseverance and effort he had learnt many things , went back to school and finished class 12. He was good in masonry, electrical work and craft work. But our society looking down on work with hands and valuing cerebral work more caused his family to put enormous pressure on him to join a college in Salem to graduate in Math, which he was never good at. He couldn't cope with it and came back to us a complete wreck! It was a very difficult month for us trying to convince his entire community that he was not possessed by the ghost of the boy who died in the stream some years ago but was sick and had to be treated in a hospital. Finally with the help of our doctor friends and Dr. Anna of C.M.C, Vellore, he was treated and is improving now. He is just one example.

Forums and support groups and mentors for young people to discuss their problems and maybe workshops for parents seem to be a necessity now.


### Health classes


Randall, a young doctor working in The Tribal Health Initiative , Sittilingi conducted sessions on oral hygiene, basic hygiene, ill effects of tobacco, good nutrition etc. on many Friday evenings through the year. Along with the senior students and Anu he put up some plays on these topics for the evening children. On one brainstorming session on Health, the children came up with almost 60 varied questions on the body and Health. Randall and the other new young doctors of the hospital have agreed to answer all these questions over the next months through classes, discussions , video and other media.

[![](http://www.thulir.org/wp/wp-content/uploads/2013/01/P1030317.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/01/P1030317.jpg)


### Arrivals/ Departures


Theerthagiri, Ajit kumar and Parthiban finished their year with us successfully and went on to join class 11 in Coimbatore and Kottapatti respectively. Chandramathi had already gone to join the diploma in nursing course started at THI. Jaganathan, Raman and Thirupathi were taken out prematurely by their families as they felt that the stipend of Rs. 1000 per month at Thulir was not sufficient to pay their debts. Many families in the village have started building concrete houses as part of a Government scheme. The money given by the government is barely enough for the sizes and it also doesn't reach them fully after all the bribes along the route. Families also tend to think that since they are building once in a lifetime they should build properly and end up piling expenses and loans taken at exorbitant interest rates.They then want their teenage sons to go to Kerala where there is a demand for unskilled labour at Rs. 500 per day. So the stipend of Rs. 1000 per month at Thulir seems abysmally low. It is a real pity because all three of them are very bright, motivated and talented boys and if they had stayed for another year they would have learnt some skill properly and could have gone out and worked as a skilled person.

Jayabal, Danapal and Parameswaran are still here. Sakthivel has come back,very confident and mature after a very fruitful and meaningful training at vanya and Goutham's and has joined as staff. He is showing great enthusiasm in teaching the younger children. He is clearly inspired by the teachers at Sita school.

Kalpana,one of the cooks in the Thulir kitchen was attacked by her cow and injured and so stopped working. Saroja has joined in her place. Chidambaram, Ambika, Vijayakumari, Lakshmi and Sasikala have joined in the new academic year. Vijayakumari has now gone on a maternity break.


### Organic Farming Enterprise


Senthil, Jayapal, Danapal and jaganathan started the organic farming and Dairy project as a business enterprise. The idea was that they would do it for at least a year, keep regular records of work, keep regular accounts of income and expenditure and see if it is a viable proposition or not. They started out with great energy and enthusiasm and teamwork. It was truly amazing to see them work.But unfortunately nature played foul with them this year! The period March to August was the hottest we have ever experienced here. We also had no rain after the end December2011 Thane cyclone till the middle of August. There was absolutely no grass for the cows. The price and demand of paddy straw rose up and they could only get it with great difficulty. One calf died.The two of us were preoccupied with Manoharan's illness and passing away tt we could not give them adequate mental support..With all this, two boys dropped out. Raman joined in. They continued inspite of the hardships though. They bought hens and started rearing them. A new cowshed and hen house were built. The bio gas was working well and the operation of it has also been regularised over the year. Raman too dropped out in June. Senthil and Danapal are still holding on.A bad monsoon did not allow them to plant the regular pulses and cereals in both June and September. The ragi crop of June failed completely.They have now planted coriander and Channa. The northeast monsoon, which is the main source of water for us, has been very inadequate. Our land which looked very green and prosperous at the beginning of the year already is dry and parched in December. We'll have to wait and see how 2013 fares! But it has been a great learning and growing-up experience for the boys. They have managed to keep all their accounts and records on Tally. We will upload them separately.


### [![](http://www.thulir.org/wp/wp-content/uploads/2013/01/P1020992.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/01/P1020992.jpg)




### Parents' meeting


Through April and May we had a number of informal meetings and discussions about Thulir's role in Education and the needs felt by the parents especially the staff of the hospital with Regi, Lalitha and the hospital staff. In June, we had a long meeting. Three couples wanted us to help them teach their 3 year old children and asked if we could keep them in Thulir during the day. There were many other requests which amounted to interventions in education right from the age of 3 to 21!

We have to work on these requests slowly. Our main constraint is trained, willing and motivated manpower to handle all this. We need a group here even to think about this, plan and provide direction before the actual execution. The two of us are too busy with the actual day to day administration, management and teaching, to take time to plan, discuss and decide. We hence decided not to take a fresh batch of adolescents for the basic technology course in July 2012 in order to give ourselves some breathing space to think and plan for the future.


### Balwadi


The Thulir campus has been enlivened by four fresh, energetic lively 2 1/2 to 3 year olds! All girls! This has resulted from the parents' meeting. This is a new development! We decided to open the 'Kutty Thulir' for just the children of the staff this year. We have two young married girls, Lakshmi and Sasikala, enthusiastic and eager to learn, to handle this section. Professor Ravindran has generously agreed for us to use his courtyard space for the balwadi. We are busy helping the children feel at home and helping their natural learning instincts bloom while training the teachers to find the right balance between letting them free to flower naturally and engaging them in creative activities


### Yercaud Trip


The entire village was agog with excitement as children prepared to go on a trip with us to Yercaud. We haven't taken the younger children on a trip for quite a few years now and this has been a long drawn demand from them. We didn't expect so much excitement though! Parents and children were awake and ready from 3.30am and on the road for the hired buses to come at 5am. As luck would have it there was a stray group of elephants on the forest road to Kottapatti( An unheard of event in this area) and the buses were delayed by an hour! But everyone waited patiently and except for the widespread motion sickness all 50 children and 10 adults enjoyed the trip, especially the zoo parks in Salem enormously!


### Sita School Visit


Jane and Santhosh brought a group of students and teachers from Sita School for 3, 4 days in the beginning of August. Jane has always been a source of quiet inspiration, friendship and support to us. Santhosh is the person behind the Marathon running in Thulir and a good friend of the entire Thulir team. So it was a great pleasure having them here and interacting with them. Sakthivel was well known to the Sita school students and he played host very beautifully and was a bridge connecting the two groups. Each group taught the other crafts they knew and they worked well together.


### Wedding Bells


Theerthammal got married in August and left Thulir to her husband's village near Thiruvannamalai. We miss her gentle and smiling presence.

Senthil and Rajammal, once our students and now the senior most staff of Thulir got married in September amidst great excitement. All of us were busy with this for the entire week. They continue to work here.

Best wishes to all of them for a meaningful and happy married life.


### Sports Days


We had three sports days this calendar year! One in March, one in October organised by the Thulir team and, one in December organised by Rolf and Susan. All of us enjoyed them hugely and the students of course would even like to have one every month.


### Quiz


We had a quiz in Thulir for the first time this year for the under 13 year olds. Since this was the first time, we kept writng sample questions on the black boards for a week before the event and children were encouraged to do research or ask someone and find answers! We were very happy with the response, the intense search for information and to see our encyclopedia and other such books being used!

[![](http://www.thulir.org/wp/wp-content/uploads/2013/01/P1030321.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/01/P1030321.jpg)


### Evening Classes


The evening classes have been going on more enthusiastically and are well attended. 190 children attended and used Thulir from June 2011 to May 2012. The children coming in the evening are of various groups. There is a faithful group which comes most working days and which wants to learn subjects with some continuity. They would have at least 75 percent attendance.There is another group which comes on holidays and random days to use the books , Art materials, puzzles etc. There are some who come in for specific needs- to prepare for an exam or competition or to get something explained. We sometimes call in groups for fixed camps. In 2012, Sakthivel, Chidambaram, Senthil, Ambika have joined Rajammal, Devaki and Anu to teach the children in the evenings. So each of us has a group and we also have sessions for planning and learning what we would teach each of the groups. What we plan to teach depends on what the learner has asked to be taught. so one has to start from what they know and what they need to know. Most times they come to us saying, " I didn't understand this, please teach me this" . The students follow the Tamilnadu samacheer kalvi system in their school. so we have to teach the same, but we might use other methods to make sure the learner has understood. We use activities and experiments to make the topic more interesting. But because of the limited time we have we definitely can not cover the entire syllabus. We have to do a balance of what the child asks for and what we feel a child pf that class ought to know.Attendance is not compulsory, so our numbers vary from 15 to 60 each day.


### Tests And Evaluations


This has always been a source of questioning and exploring for us. We are not a formal school and the government school children are not with us full time. But we would like to get a feedback on how effective we are and also gauge the progress of each child. We do have tests and worksheets from time to time when the child asks for it and is eager to participate in the evaluation. But our time with them is so limited, we have so much to share and they have come to us after 8 hours of school and tests that we are not able to have them regularly. We have tried various ways over the years! This year on Anita. B's suggestion we have started files for individual students where we file all the work the child does. So at the end of each academic year, the child and us can assess what the child has done over the year,even though this leaves out all the oral or practical work, games, songs , dances etc. We also from time to time ask them to evaluate Thulir.

[![](http://www.thulir.org/wp/wp-content/uploads/2013/01/DSC06724.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/01/DSC06724.jpg)


### Exchanges with Marudham Farm school


We have an ongoing and meaningful exchange programme with the Marudham farm School. Teachers from both schools have been visiting each other over the year. In September, Poornima brought a group of students and teachers to Thulir. Both groups went trekking together, swam in the stream, taught each other many songs and dances and had a very good interaction.

In December, Marudham School invited a group of us for their craft week. Anu and all the young women teachers attended. Meeting similar minded people doing great work, learning various crafts like palm leaf weaving, making masks, embroidery, theatre etc. was energising.


### Classes for nursing students


The hospital is conducting a diploma course on nursing from last year. They have seven students now. They come here once a week for classes on general awareness, life skills, environment etc. and also to use the resources at Thulir.


### Participation in Running events


At the beginning of the year it seemed as if interest in running was waning in Thulir. Fewer students participated and they were not very regular in their training. But still over the year they participated in the Auroville, Cauvery and Chennai marathons. But as the year ends interest in running has reached a new high with many younger boys-10 to 15 year olds- joining in. They are very enthusiastic about their training schedule and most days come to Thulir before dawn, and sit around a fire waiting for daylight and their seniors to arrive! The seniors, especially Parameswaran aAnd Chidambaram have taken responsibility for their training and are discharging it admirably. They are all preparing for the Auroville marathon in February.

[![](http://www.thulir.org/wp/wp-content/uploads/2013/01/DSC06792.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/01/DSC06792.jpg)


### Cycle trips


The older boys continued their long distance cycling trips. They cycled to Payir, Thenur in June.Their cycles are old and worn out. But they carry their tool kits and repair them on the run.


### Volunteers in Thulir


Florence From France and Rolf and Susan from Switzerland volunteered in Thulir towards the end of 2012. Florence was here for 4 weeks and taught English classes for both the staff and older and younger students.She talked to the students about France. Rolf and Susan were here for 6 weeks. Rolf taught carpentry, metal work and metal bracing to the students. From him the students learnt how to be systematic and organised about work. Susan continued Florence's English classes introducing new activities and Games. By the end of their stay, the entire group's confidence in English speaking had grown enormously. They could communicate with ease in English. Both of them did other activities as well, cooking European dishes for everyone, Showing pictures about Switzerland and talking about it, organising Games etc. We thoroughly enjoyed having all three of them here. They will be writing separately about their experience here.

[![](http://www.thulir.org/wp/wp-content/uploads/2013/01/P1030009.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/01/P1030009.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2013/01/P1030004.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/01/P1030004.jpg)


### Reviving Traditional Art


The older music, theatre and other art in this valley has long been over shadowed by cinema music and TV. There are only a few remnants. We have been trying for a long time to get older people who know this to come and teach the young at Thulir. Finally at the end of this year, Vellachi Ammal from Thekkanampet agreed to come and teach. We were apprehensive whether students would find her music 'cool' and we kept talking to them beforehand and preparing them. We also decided to keep the teenagers away for the first session as they might not be interested. But as she started singing all the children started keeping beat unconsciously and soon the teenagers too drifted in,shoved their cellphones in front of her as if she were a celebrity and started recording the songs. 50 children from age 3 to 23 remained singing and learning for 3 hours!

2012 ended and 2013 began in Thulir with this music, a meeting of old and new and a hopeful note!

Thanking you all for remaining by us and supporting us and wishing you all a happy, peaceful and meaningful New year,

**_Anu and Krishna_**


******************



