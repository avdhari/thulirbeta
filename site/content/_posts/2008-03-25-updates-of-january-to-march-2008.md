---
author: thulir
date: 2008-03-25 06:33:00+00:00
draft: false
title: News Update of January to March 2008
type: post
url: /wp/2008/03/updates-of-january-to-march-2008/
categories:
- Newsletters
---




**Children’s Committee in Thulir**




Selvaraj** **shifted from Thulir to help with the organic farmers’ co-operatives. Gowri left to pursue a B.Ed. course. So Thulir is now run by some of our students – Perumal (19), Senthil (19), Rajammal (17) and Devaki (17).




Senthil and Perumal teach “vocational skills” to the young children in the weekends (making bamboo toys, basic electronics, etc.). All of them are continuing their studies while learning to help us in administration and teaching.




[![](http://bp0.blogger.com/_qz2j5uas_y0/R75rlU178RI/AAAAAAAAALw/g7IxCwRHr0E/s320/PICT3643.JPG)
](http://bp0.blogger.com/_qz2j5uas_y0/R75rlU178RI/AAAAAAAAALw/g7IxCwRHr0E/s1600-h/PICT3643.JPG)




We have been , in the last few months, talking to the four of them and the three boys who stay here in the nights about taking more responsibilities and participating in the decision-making and ownership of Thulir. Senthil's and Perumal's visit to Kanavu (where the trust is entirely managed and directed by students) and Vinu’s presence here has helped strengthen this process. An informal children’s committee has been formed. They decide their weekly schedules, clearing responsibilities, free time. activities, visitors’ teaching, schedules, etc.




**Pongal Celebrations in Thulir**





Pongal was celebrated in Thulir  with the kolam festival [kolam is the traditional patterns drawn on the ground] organised by the senior students. The entire campus was given a thorough cleaning and decorated in a simple way. Some staff from the hospital and guests too participated. Each participant was assigned a demarcated part of the ground to draw the kolams.
[![](http://bp3.blogger.com/_qz2j5uas_y0/R75rlE178PI/AAAAAAAAALg/cgOoOJUrMnQ/s320/PICT3288.JPG)
](http://bp3.blogger.com/_qz2j5uas_y0/R75rlE178PI/AAAAAAAAALg/cgOoOJUrMnQ/s1600-h/PICT3288.JPG)



The rule was that each person could use only two colours. Every available floor space around Thulir was filled with a blaze of designs and creativity. Traditionally Kolams are only  done by women and hardly ever by men. But in Thulir the boys too participated with enthusiasm. In fact, Perumal's was the largest kolam. Krishna's mask and Chutty's dinosaurs also drew attention. Jayashree and sangeetha both staff at the hospital, excelled in traditional patterns.



[![](http://bp2.blogger.com/_qz2j5uas_y0/R75rk0178OI/AAAAAAAAALY/tbHtIoMVw4I/s320/PICT3231.JPG)
](http://bp2.blogger.com/_qz2j5uas_y0/R75rk0178OI/AAAAAAAAALY/tbHtIoMVw4I/s1600-h/PICT3231.JPG)


[![](http://bp2.blogger.com/_qz2j5uas_y0/R75qV0178NI/AAAAAAAAALQ/GgywTnITsjY/s320/PICT3211.JPG)
](http://bp2.blogger.com/_qz2j5uas_y0/R75qV0178NI/AAAAAAAAALQ/GgywTnITsjY/s1600-h/PICT3211.JPG)




**New Year’s Day in Thulir – Children’ Committee’s organising capacity**




We did not realize the potential of the Childrens’ Committee until the morning of December 30 th. Anu, who was suffering from fever, was woken up by a group of teenagers. They presented her with a computer-printed invitation (in English) to attend the New Year's Function in Thulir organized by the Thulir  Childrens’ Committee. It was to be an ambitiously long program and they wanted to organize it in two day’s time. Anu was sick and Krishna had gone to attend the Asha Fellow’s Conference. The children coolly assured a flabbergasted Anu that they would handle everything. She had to only correct the spellings in the invitation and give her opinion, when asked for ,during the  rehearsals.




[![](http://bp1.blogger.com/_qz2j5uas_y0/R75pWk178FI/AAAAAAAAAKQ/F0qvxkDIMEc/s320/christiannabig+346.jpg)
](http://bp1.blogger.com/_qz2j5uas_y0/R75pWk178FI/AAAAAAAAAKQ/F0qvxkDIMEc/s1600-h/christiannabig+346.jpg)




[![](http://bp3.blogger.com/_qz2j5uas_y0/R75nmE177_I/AAAAAAAAAJg/xqIkOBr3AbE/s320/christianna+325.jpg)
](http://bp3.blogger.com/_qz2j5uas_y0/R75nmE177_I/AAAAAAAAAJg/xqIkOBr3AbE/s1600-h/christianna+325.jpg)




And true to their word, they did! About two hundred people turned up – including the health auxiliaries from all the 21 villages. We were really proud to see our hitherto tongue – tied  teenage boys talking on the stage, and our shy girls dancing and the smaller kids putting up plays. True, the songs and plays needed more practice and polish but we were proud to see the way the kids took charge of everything – including refreshments for the entire audience – and carried it through.




We were touched when our  friends who were visiting that day  - Christiane, Kai-Uwe and Susanne told us, “We couldn’t understand the language but we could feel the children’s spirit and pride for the place and that they were entirely self-motivated.”




Anu suffered a ligament injury in her back and collapsed that day. She has been immobile the last five weeks. She is progressively getting better and is expected to recover fully in the next three or four weeks.





[
](http://bp0.blogger.com/_qz2j5uas_y0/R75pWU178EI/AAAAAAAAAKI/uLG11d8KaUE/s1600-h/christiannabig+328.jpg)

[![](http://bp3.blogger.com/_qz2j5uas_y0/R75nmE178AI/AAAAAAAAAJo/B3ACi4eym7A/s320/christianna+327.jpg)
](http://bp3.blogger.com/_qz2j5uas_y0/R75nmE178AI/AAAAAAAAAJo/B3ACi4eym7A/s1600-h/christianna+327.jpg)


[![](http://bp0.blogger.com/_qz2j5uas_y0/R75nmU178BI/AAAAAAAAAJw/MfHaB5WabMA/s320/christianna+338.jpg)
](http://bp0.blogger.com/_qz2j5uas_y0/R75nmU178BI/AAAAAAAAAJw/MfHaB5WabMA/s1600-h/christianna+338.jpg)


[![](http://bp1.blogger.com/_qz2j5uas_y0/R75nmk178CI/AAAAAAAAAJ4/dKql9Ct4_Ao/s320/christianna+350.jpg)
](http://bp1.blogger.com/_qz2j5uas_y0/R75nmk178CI/AAAAAAAAAJ4/dKql9Ct4_Ao/s1600-h/christianna+350.jpg)




**Other news **






	  * 


In  October, just before Gowri left, we along with the senior children  visited most of the children’s houses in Moola Sittilingi, Kalayankottai,  Malaithargi and  Sittilingi. These social visits went on for a week and the warmth  and hospitality we received made us feel good.



	  * 


Of  the evening children many girls aged 12 to 14 attained puberty in  the last six months and their parents have stopped them from coming  to Thulir in the evenings. The boy’s death in the nearby stream 1  ½ years ago and the belief in the spirit, which is thought to reside in the land of the road to Thulir contribute to this. They believe,  that girls of that particular age group are most vulnerable to the  ghost and hence have stopped them. We would have to visit parents once again after Anu recovers.






[![](http://bp3.blogger.com/_qz2j5uas_y0/R75pXE178HI/AAAAAAAAAKg/RcqPm8ouNNs/s320/homesept-oct07+007.jpg)
](http://bp3.blogger.com/_qz2j5uas_y0/R75pXE178HI/AAAAAAAAAKg/RcqPm8ouNNs/s1600-h/homesept-oct07+007.jpg)








**
**


**Work on the land/ bee-keeping update**






	  * 


An  organic farmer form Sathyamangalam, Mr Kumar visited us and talked  to the students. He commended Senthil's banana patch in Thulir and  our vegetable patch, cleared our doubts and gave tips to improve. He  initiated improved Rice planting method in Rajkumari's land and all  of us went to observe it. 



	  * 


The  Hospital had also organised a  2 day organic farming workshop. The  senior students of Thulir participated. After that they started  making organic manure and vermicompost. Each of them took small  patch of land and started growing vegetables organically. but we  observe a feeling of reluctance among the older students to do any  work on the land or grow anything. There is a feeling that it is a  low status work. We have to keep gently pushing them to do it and   emphasize that farming too is a valuable education and that the  knowledge that they possess is something to be valued and not  forgotten. This we find is a very difficult task as mainstream  values teach exactly the opposite!



	  * 


We  harvested our first honey this January. We got about 700 ml. We now  have divided colonies and successfully housed them in five separate  boxes. Our beekeeping seems to have settled down after a lot of  initial difficulty. We still have a long way to go.






**
**


**Our thanks to:**






	  * 


Dr.  Ravi – a close friend who used to work in the hospital here and  now works in a remote area in Orissa – got married in December.  His wife Prema and he have donated the money they got  as wedding  gifts to Thulir. This was a very touching gesture. All of us at  Thulir wish them a long life together filled with happiness, health  and contentment. Both of them want to work for disadvantaged people  in society. We  admire their principles and wish them good luck.



	  * 


Throughout  this year, Prof Ravindran and Vanaja akka have been looking after  Thulir whenever we had to go outside Sittilingi. This has been a  great help and has provided much relief to us. Prof. has taken a  number of classes for the boys on pumps, machines and electricity.  His vast knowledge,and his ability to explain the basics have helped  us all a lot. This year he has been teaching basic English too for  the senior students.



	  * 


Dr  Sridhar santhanam from CMC Vellore and V.Krishnan an Architect  friend  from Delhi have been consistently supporting us  in the  building up of Thulir through the past 4 years . We are thankful for  their support.



	  * 


Since  the formation of Thulir Trust, we  are happy to have received  donations from Tribal Health Initiative, Sittilingi,; Mr. Sujit  Patwardhan, Pune; Dr Mary Ramaswamy from Oddanchatram; Dr. Sunder  Iyer of IIT Kanpur;and  Mrs. Meenakshi Balasubramanian from Chennai.













[![](http://bp3.blogger.com/_qz2j5uas_y0/R75pXE178II/AAAAAAAAAKo/d2v104VJ4rs/s320/homesept-oct07+016.jpg)
](http://bp3.blogger.com/_qz2j5uas_y0/R75pXE178II/AAAAAAAAAKo/d2v104VJ4rs/s1600-h/homesept-oct07+016.jpg)










**Visitors:**






	  * 


Dr Nick Bateson of Friends of  Sittilingi, UK and Ian of Fairgrounds UK visited us. They talked to  the students about fair trade and showed some products that they  sell through Fairgrounds. Nick has been a regular visitor to Thulir  and has conducted engaging sessions earlier too.





	  * 


Susanne,  a teacher of mentally challenged children,  is volunteering at  Thulir. It is good to have her here especially with Anu being ill.  Susanne teaches English through Games and activities to the children  and they enjoy it thoroughly.  A good rapport has been established  and she was invited to various houses during the week long Pongal  celebrations.









**New community Kitchen at Thulir**




In December and January, the senior students built a small temporary structure to house a makeshift kitchen cum dining space. They started with a lot of enthusiasm, planned at length [thereby making it more elaborate and bigger], and built with great gusto. However, with  New Year intervening and Pongal fever taking off, the work crawled to a stop and with some gentle pushing and external help, we managed to finish the project. Now we have a kitchen operational, and so are able to host visitors and guests.




[![](http://bp1.blogger.com/_qz2j5uas_y0/R75qVk178LI/AAAAAAAAALA/8pDRbDn3WHE/s320/homesept-oct07+045.jpg)
](http://bp1.blogger.com/_qz2j5uas_y0/R75qVk178LI/AAAAAAAAALA/8pDRbDn3WHE/s1600-h/homesept-oct07+045.jpg)





**
**


**Class X public exams**




The Tamilnadu government class X exams for private candidates were held in September. Six of our students wrote one or two subjects each and four passed. We have a group of 11 students (including 6 girls) coming during the day to study and write X exams. All these students come to us only months before their exams and this whole exercise is a source of dilemma and frustration for us. First, none of them have been taught the basics in any of the subjects. They have been taught only to memorize facts in their school without any understanding. We believe in learning and not memorizing. So it is a frustrating exercise.




We compromise by selecting a few topics from each subject and teaching them completely.




Before the exams, they have to go to Harur (a day’s work) to pay the exam fees and again to collect the hall tickets (two days before the exams). Two whole days and about Rs 200 are spent in this travel alone. They are also allotted exam centres only in Harur or Dharmapuri. So they either have to go there the previous day and find someone to stay with or risk the bus-service on the day of the exam. During the last exams, the bus didn’t come on the morning of the English exam and the girls hitched rides and bus-hopped and reached the exam hall 45 minutes late!




When one sees the enormous hardships these students undergo just to write these exams and the poor academic skills and recognition they gain at the end of it one wonders if the whole exercise is worth it!







[![](http://bp2.blogger.com/_qz2j5uas_y0/R75qV0178MI/AAAAAAAAALI/mrzGaWxmFls/s320/PICT3206.JPG)
](http://bp2.blogger.com/_qz2j5uas_y0/R75qV0178MI/AAAAAAAAALI/mrzGaWxmFls/s1600-h/PICT3206.JPG)




**Other inputs for the class X dropout – students**




Realizing the importance of “hand-on”skills, we taught this group basic - electronics, soap- and paper making, embroidery, organic farming, bamboo crafts etc..




We also had many discussions on the social and personal issues that confront them. Effects of the media (TV, cinema), suicides, alcoholism, infatuations and romances, real education, etc. were some of the issues we discussed with them at length. Initially most of them were silent, but with time there is an increase in their participation. Schooled into “not thinking, just receiving mode', they have just started realizing the importance of “thinking” about some of these issues.




Senthil and Perumal have been teaching this group the skills they have learnt. The girls too make good LED torches now! There is a great demand for our bamboo torches outside Sittilingi and for our plastic torches inside Sittilingi!




Rajendranath Setty, a class X student of Rishi Valley school, visited us in November and taught them to weave wrist bands of thread. These friendship bands are very popular among both boys and girls! Our village children used to buy them earlier from outside. Now they make various designs in Thulir, thanks to Raj.





**
**


**Music and dance in Thulir**




[![](http://bp1.blogger.com/_qz2j5uas_y0/R75nmk178DI/AAAAAAAAAKA/6ekcyf7-GXw/s320/christianna+366.jpg)
](http://bp1.blogger.com/_qz2j5uas_y0/R75nmk178DI/AAAAAAAAAKA/6ekcyf7-GXw/s1600-h/christianna+366.jpg)




In our last post we had written about the visit of the Timbuktu school students. Their visit has given quite a boost to music and dance in Thulir. Prior to that, our children (and the two of us) had a great fear of singing and dancing. But inspired by them, we decided to have singing sessions every day and Kolattam dance sessions every weekend. We have also been encouraging students to enact the stories they read into small skits, which they have been doing. So now they have started organizing small performances within Thulir every month. The quality of all this still needs to improve a vast deal  but all of us have great fun singing and dancing.







[![](http://bp0.blogger.com/_qz2j5uas_y0/R75pWU178EI/AAAAAAAAAKI/uLG11d8KaUE/s320/christiannabig+328.jpg)
](http://bp0.blogger.com/_qz2j5uas_y0/R75pWU178EI/AAAAAAAAAKI/uLG11d8KaUE/s1600-h/christiannabig+328.jpg)








**
**


**Learning Exchanges with “Kanavu”, Wyanad, Kerala**




In September, Shirley from Kanavu visited us with two of her tribal students - Mangulu and Vinu. Mangulu and Vinu stayed here for a month learning and teaching. They taught the children songs, dances and a few Kalari payattu (traditional martial arts from Kerala) exercises. They started learning some academic and vocational skills. Their visit and the fact that five boys are now staying at Thulir regularly in the nights to study spurned us to start cooking and eating together in Thulir. All of us took turns cooking and cleaning that month. That all our teenage boys could cook a basic meal of rice and “sambhar” surprised us!




But this activity had to stop during the November/ December monsoon rain as we didn’t have a proper kitchen in Thulir. Mangulu and Vinu too returned to Kerala. But their influence in our songs and dances still lingers on.




Senthil and Perumal went to Kanavu for a week in December, to help the older children there with the harvesting of rice. Vinu has come back with them and he plans to stay at Thulir for a year. Vinu is skilled at pottery and he plans to start a pottery unit here.




**********************
