---
author: nimda321
date: 2015-01-07 14:03:49+00:00
draft: false
title: Newsletter Aug - Dec 2014
type: post
url: /wp/2015/01/newsletter-aug-dec-2014/
categories:
- Newsletters
---

_**Happy New year to all of you!**_

_May we together make the world a slightly better, wiser, happier and a more healthy and peaceful place this year._

[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/DSC_1047.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/DSC_1047.jpg)

_Read in this Newsletter about rains in Sittilingi, Raspberry Pi (a mini computer designed for villages without much electricity), craft week at Marudam school, plans for a starting a school, farming, visit to Puvidham school, re roofing buildings in thulir, sports days, hogenakal water falls visit, cycling trip to Sathanur Dam and about the visitors we had during these months._

**Rains**
This year has been worse than the previous year in terms of rainfall. Summer was unusually hot this year and when it was the monsoon's turn to show up, it never did. It seems that other parts of Tamil Nadu have gotten their fair share (and more) of rainfall, but little Sittilingi remains parched. Hard working farmers around us haven't got their expected harvests and are a bit frustrated especially after hearing from others traveling outside that other regions are blessed with good rains.
Our crop of Kambu (bajra) too hasn't been very good and much of it was consumed during the harvest itself (The evening children like it raw, fresh from the plant).

[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/9augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/9augdec14.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/10augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/10augdec14.jpg)

The water level in our well has increased but not so much that we can tide over the summer. This year, a bore well has become a matter of having water to drink, rather than a question of ethics. With farmers around us increasing the amount of water they use for their (relatively new) cash crops- to flood their paddy, or irrigate sugarcane, turmeric or tapioca for instance- all our judicious water use is like climbing a coconut tree with a weight problem and a fractured ankle.
The village panchayat has been of some help by digging a bore well on thulir land and installing a working motor inside it. Now there is a water tank inside thulir campus that holds the water from the bore that is pumped occasionally, but the only problem is that the bore well doesn't have much water in it.

**Ultra marathon runners from Runner's High**
Santhosh brought his team of runners again one weekend in September to train for their ultra marathon. They did a 12 hour run on Saturday and A 5 hour run on Sunday. Senthil and some boys from Thulir too joined them for part of the day. Unfortunately the sun too came out in full form and decided to join them making them thoroughly hot and dehydrated! Seeing and interacting with them, the Thulir team is now inspired to host a run here in the valley some time.

**Madanyu**
We had two interesting visitors Aftab and Max from the UK, PhD student and Post-Doc at the University of Cambridge, from the Madanyu Education Programme ([http://www.madanyu.org](http://www.google.com/url?q=http%3A%2F%2Fwww.madanyu.org&sa=D&sntz=1&usg=AFQjCNFLwaLr8IGc0YUdyq2SR91Gw0Gu1g)) to travel to remote villages in India to provide computer skill training at education centres via the Raspberry Pi. This is a low power, fully functional computer that will fit into a shirt pocket; basically a CPU with in-built ports to connect a monitor, keyboard, mouse, Ethernet cable and a memory card, which is the main data storage of the Pi. It also has GPIO (General Purpose Input Output) pins which are exploited by hobbyists to connect various components to the computer and use it in different ways. The memory card comes loaded with the Madanyu OS, basic programming software, educational games, etc. which we could all use and learn from. We had a three day workshop on how to use this computer and Python, a powerful programming language that is increasingly being used in the computing world. The children and teachers had a class on turtle graphics, a program much like the original DOS version LOGO, except more advanced and colourful.


[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/35augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/35augdec14.jpg)




The children watched videos of hobbyists who have used this computer as cell phones, radio transmitters, discreet cameras, and even robots capable of responding to instructions through radio waves! At the end of the workshop, there was a competition of sorts, where children had to come up with a new and different application for the Raspberry Pi and explain with a drawing and some writing to the two volunteers. Some wanted to study Marine life by attaching it as a camera onto the bodies of  aquatic animals underwater, some wanted to use it as a controller to control those serial lights that we see in Christmas trees and install those lights in shops to make the shop more bright, colorful and attract more customers(very practical!), some had totally outrageous ideas, like using it to increase the strength of an elephant so that it may carry more load, or generally increasing the capacity of an animal to do more work (we were equally boggled at the idea!)
The competition ended with the resource persons gifting us with an electronics kit, without any manuals or other help so that we tinker around with it and figure out what all it can do all on our own.


[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/34augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/34augdec14.jpg)

**Marudham craft week**
Marudham has almost become like another home for us at thulir, and if it is craft week, we somehow find ourselves planning and packing to spend at least 5 days there. This year also we did the same and had a great time, soaking in the energy of the place - there is activity in every classroom and outside too with varied activities like kalamkaari to silambaattam, and everything else in between so there is something for everyone. The school is so vibrant during that time that the whole 5 days there is like one long endorphin rush. Such an atmosphere is so charged with learning that we come away with our hearts full. This time, the entire thulir team went and participated in kalamkaari, palm leaf weaving, ear ring making, silambaattam, wood carving, stone carving and the art of learning how to learn! The football sessions in the evening, the unicycle that was just lying there, free for anyone to try it out, juggling lessons, learning how to perform a summersault in the air by jumping off a spring board and landing on a mattress, learning dive rolls, walking on a tight rope and interacting with different groups of people all added to the experience.
Senthil and Sakthivel will soon be visiting Marudham to conduct a workshop on bamboo chair making. The chairs are elegant and comfortable to sit on, and completely bio degradable, made of just bamboo.


[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/maru_2.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/maru_2.jpg)




**Shankar birdwatching club**




Dr.Shankar, a friend from Bangalore was kind enough to donate a pair of fantastic binoculars to Thulir this winter. This has come at just the right time when the children are beginning to pay more attention to birds around Sittilingi. Now with a few interested kids, we have started a birdwatching club! (and decided to name it Shankar birdwatching club as a way of saying thanks). So far we have spotted many birds and bird nests in Thulir itself, and roughly identified a beat. The best part about the binoculars is that it has shown the watchers beauty in chickens too! This is a big achievement as this is a bird they see every day, everywhere, it being the most common feathered friend around. Butterflies are also observed, along with squirrels, rats, dogs, cats, sometimes people, the hills nearby, tree tops, clouds, etc. Knowing about a person like Salim Ali and that it is also possible to write a book and make a living observing birds has given some kind of energy to a few boys here, and they are still amused and browse through the plates of 'The Book Of Indian Birds' with a quiet wonder.




**To school or not to school…?**




****For years the community here has been asking us to make Thulir a full time school. We have been putting it off saying that since there is a government school here, it makes more sense for us to supplement and complement it rather than replicating the entire infrastructure. But education levels around here have actually become worse and what we do does not suffice. The pressure on us for a new school has increased.There is no place for real learning or values and definitely no place where children are allowed to discover themselves and learn freely and happily. A new school is on the horizon definitely now.




Plans for the new school in Sittilingi are being made as we speak. We would like a larger community to have the ownership of the school and be part of formulating the philosophy and vision for the school. So all the doctors and the staff from the hospital, organic farming initiative, tribal technology initiative and Thulir have been meeting up on a weekly basis to discuss the school and democratically arrive at a consensus on all matters ranging from the values and culture it should uphold to how the school should be, how its teachers and teaching should be, what kind of an atmosphere do we want the children to be in, should the students be primarily tribals or non- tribal,should the students wear uniforms etc. The meetings have been of great help for us as a group to understand what everyone is thinking about the school, and also education in general.
A couple of practical problems like land for the school, funds, a team to get permissions from the government officials, water, still remain but are being discussed also. It is a challenging task ahead for all of us but we hope it will be a space for learning more relevant and interesting than any other institution in the entire valley.




![](http://www.thulir.org/wp/wp-content/uploads/2015/01/12augdec14.jpg)





_The children came up with this plan when we discussed with them how they want their dream school to be like!_


**Farming**

The enthusiasm during what little monsoon we had manifested itself in the form of ploughed fields and planting of brinjal, green chilly, onion, spinach (a variety known as pulicha keerai in tamil because it is sour tasting - which we keep harvesting from time to time for chutney, etc.), beans, ragi, rice, and for the first time in Thulir history, tapioca, a one year crop. Everyone, including the kutty thulir kids, helped us in preparing the field! We made furrows, flooded the fields and leveled it with everyone helping out. We hoped that the crops would benefit and mature from whatever monsoon rains we have. We would harvest most of them before March when the heat starts so there won't be much irrigation required. Tapioca doesn't require water.


[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/4augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/4augdec14.jpg)




_Due to the density of the salt water, the lighter grains of rice float to the top and the heavier or more mature grains remain at the bottom. The heavier grains are then selected for planting._




[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/2augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/2augdec14.jpg)




_Ragi Motherbed_


[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/6augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/6augdec14.jpg)

**Puvidham visit**

As part of the initiative to start the school, we decided that exposure trips for our team of teachers to different schools would be good . With this idea, the entire thulir team travelled to Puvidham school in a little village called Nallampalli, 20 kilometers from Dharmapuri. It is an alternative school catering to rural and dalit children. It was the second alternative school that many teachers from here visited, after Marudham. The men in the team cycled to and back while the women and the 4 babies went by public transport, changing 3 buses. The thulir team got to interact with the Puvidham teachers and students, learnt about their functioning, curriculum and exchanged songs, dances,etc. The experience was good and we learnt different things from it.


[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/22augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/22augdec14.jpg)




_Examining a corrugated roof made of compressed layers of tetra packs with a thin plastic lining on both sides._




Another big learning was how the Puvidham school team managed to deal with the government officials for the registration process. Getting recognition now even after fulfilling all the requirements is a long and cumbersome process and it has been good for us to hear the experiences of Marudham and Puvidham so that we are a little more prepared, and have a better idea of what to expect.




[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/23augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/23augdec14.jpg)




_Exchanging songs!_




[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/24augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/24augdec14.jpg)


**Re roofing Thulir**
The thatch roofs of the course room, tools store, engine shed and entrance verandah had decayed and it was time to re roof them. Normally we would have employed people from outside for this. But this time Senthil, Sakthivel, Ravi, Mohan and Perumal decided to take this up as a weekend project and do it themselves. They worked hard from Saturday early morning and finished by Sunday evening. Senthil's two year old son , Rishi, refused to be left out of the action and generally had great fun carrying and playing with sticks/leaves,rolling in the sugarcane mulch etc.

[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/36augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/36augdec14.jpg)

**Sports Days**
In December, we had two days of just sports and team games. It was such a hit with the children that we decided to have one later in the month too! Interests had changed, movements had become more fluid, children were planning a bit more, also executing plans a bit better, physical strength had increased from last year. But what remained unchanged was the adrenaline levels during games and the cheerful mood (well, mostly) throughout the day . No one wanted sports day to end, and it was always us adults who had to tell children to go back home or take a break in the afternoon or stop for lunch, etc. On sports days, children can live mostly on air and water!

[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/15augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/15augdec14.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/18augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/18augdec14.jpg)
Sports day started with a talk on winning, losing, sharing, caring, rising above just prizes (and as an extension, not being a slave to a materialistic mindset), and most of all, knowing why we play sports - not to win prizes but to learn to handle winning and losing and to have as much fun as we can. It is the joy that lasts in our minds that makes it more important than winning.
Then there was a warm up as the kids rushed out of the enclosed space to the open ground. A few stretches and laughs later, sports day was on its way! There was the usual games as always - running, long jump, relay, kho kho, kabaddi, dodge ball, frog jump and nondi- hopping and catching, a thulir favorite that is very hard on the leg that hops.
To end it, there was a prize distribution where all the participants got a box of crayons. Guests Marina and Charu taught us some songs.The songs revived the children and again no one wanted to go home but since the teachers were all too tired, it had to stop!
_**[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/DSC_17311.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/DSC_17311.jpg)**_

[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/20augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/20augdec14.jpg)

**Excursion to the water falls at Hogenakal**
This happened immediately after the visit to Puvidham School. There were two teams from thulir -The bus team consisted of mothers and their infants - Rajammal, Sasikala, Lakshmi and Ambika with their babies, all barely walking, Prema, Anu and Nikhil. The cycling team (Senthil, Sakthivel, Ravi, Perumal and Mohan) left much before the bus team, early in the morning to Hogenakkal on their cycles so their experience was very different from ours, also due to the fact that we couldn't meet up there. While they went bathing in the water, the bus team went boating with the babies on 'parisals',I.e, circular boats with the black bitumen lining on a bamboo frame, rowed with a small wooden oar. The experience of it was something new for all, and the different kinds of rock formations made by the water on the black granite over the centuries were fascinating to see. The spray of the waterfall too reached far beyond the fall itself, and gently wet our faces and finger tips.
The movement of the boat was gentle and smooth, calming both the babies and their mothers. Initially, everyone feared it, but later, as they got used to it, they started enjoying it, and were all smiles.

[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/DSC_0980.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/DSC_0980.jpg)

**Cycling to Sathanur Dam**

****The boys Senthil, Sakthivel, Mohan, Perumal, Annamalai, Solomon Raj and Nikhil went on a cycling trip to Sathanur Dam, around 50 kms from Thulir. It was Annamalai’s and Solomon Raj’s first trip outside on cycles. The uphills were tough and down slopes were a breeze, especially on old and rickety cycles with rusted moving parts. Rocky hills, quaint villages, semi deserted temples, idlis for 1 rupee each, vast green fields, winter skies, many slim tyres on black tarmac silently speeding through… We’re sure they will be back for more!


[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/31augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/31augdec14.jpg)




At the dam itself, we visited the crocodile farm that we had heard much about. With an entry fee of half a rupee, we walked in excited. What we found was that in a fifty by fifty compound with a wall of about 5 feet high were locked up at least 25-30 adult crocs. The “water body” that they had to “swim” in or cool off consisted of half the area with the water as dark as pools of tar at midnight and a stagnant stink of rot that would bring hungry vultures. There were many of these compounds and some were filled up more than the others with the animals lying lazily on the floor, some absolutely still with their jaws open, some on top of the others, taking the morning off with friends and spouses. Not that there is much else to do in a place like that where you are fed and watered according to a schedule. But it was a chance to talk about reptiles, alligators, dinosaurs and interesting things about crocs like how their skin can be bulletproof. The boys thoroughly enjoyed the animals and will soon be back for more.




**Kutty Thulir**




[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/1augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/1augdec14.jpg)




_Alphabet in Maida mixed with turmeric powder to make it yellow. Makes it easier to learn when the children get their hands involved in the process too._




[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/5augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/5augdec14.jpg)




_Little Farmers!_


[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/37augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/37augdec14.jpg)

**Visitors**
_**Roshan Sahi**_ from Sita School, Bangalore visited us for a few days. It was great to have him here. He was good enough to share his extensive knowledge of painting and sketching with the children in the form of a class on nature sketching. Concepts like shading, observing, and depth were discussed and examples were pointed out to the children, who all interestedly took to it, forming little groups and sketching plants and other objects around thulir.


[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/16augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/16augdec14.jpg)


_**Stan and Mari**_ from Gudalur have been like gurus for Anu and Krishna when they were working in Gudalur amongst the adivasis there, and their visit to Sittilingi was great for all of us. Stany's class for the thulir team on the injustice committed against the adivasis all over India from the Pre independence era, their struggle for justice, the Forest Rights Act which recognises their rights over their forests was very illuminating for us. Listening to his stories from Gudalur and how the A.M.S faced and overcame the injustice of the police and government laws for the past 30 years was inspiring to hear. Two sessions with Stan were not enough at all, we felt, but still we learnt a lot from them, and are very thankful for such an experience!

[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/DSC_1037.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/DSC_1037.jpg)


 **Rebecca**, the 14 year old daughter of a good friend Christiana from Germany volunteered at Thulir for two weeks. The children enjoyed having her around,doing things with her. It was amazing to see how so much cultural exchange happened, most of the time without language, as neither Rebecca nor the children were completely comfortable in English.


[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/21augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/21augdec14.jpg)

**Lakshmi**, AKA Kutty lakshmi, Anu’s niece, was in thulir for over a week and did many interesting activities for the children in craft. **Swetha** from Poorna School too was here. She shared with us the functioning of Poorna and the systems in place there during one of the school meetings. With Lakshmi, we spent many hours during these Christmas holidays weaving loom bands, quilling shapes, making new kinds of ear rings and paper jewellery, and also dancing to songs together. It was fun for everyone to have lively new persons in the group.

[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/28augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/28augdec14.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2015/01/30augdec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/01/30augdec14.jpg)



_**Thanks to all of you for being with us all along. Your support gives us the courage to face the daunting tasks ahead. Thank you.**_
