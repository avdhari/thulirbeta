---
author: nimda321
date: 2016-03-08 06:30:20+00:00
draft: false
title: Newsletter - January 2016
type: post
url: /wp/2016/03/newsletter-january-2016/
categories:
- Newsletters
---



[![](http://www.thulir.org/wp/wp-content/uploads/2016/02/IMG_20160210_100803121.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/02/IMG_20160210_100803121.jpg)

_Wishing all of you a year filled with innumerable moments of Creativity, Learning and Fulfillment._

**Pongal Celebrations**

We had a wonderful Pongal this January! Pongal is a Tamil rural cultural festival. It is also a harvest festival. The sun, cows and the natural elements are honoured and thanked this week. The accent is on community spirit and on using natural materials for all the rituals. It is a lovely time to be in Sittilingi. The only jarring note here is the newly added custom of broadcasting loud music in the temples the whole month!

[![](http://www.thulir.org/wp/wp-content/uploads/2016/02/Pongal-thanksgiving-155406290.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/02/Pongal-thanksgiving-155406290.jpg)

In Thulir, Pongal was special this year. Everyone creating countless kolams was enjoyable as usual. We also cooked pongal together in a huge mud pot on an open fire in the courtyard. Vellachi Ammal, a respected elder and an organic farmer from Thekkanampatu village was the special guest and resource person for the day! She taught us the traditional pongal songs and Kummi dances. All the women from Thulir and the hospital wore their sarees in the traditional Sittilingi style and sang and danced together with the children!

[![](http://www.thulir.org/wp/wp-content/uploads/2016/02/traditional-pongal-dancing_153156846.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/02/traditional-pongal-dancing_153156846.jpg)

The traditional music and dance of the tribal people in the valley has mostly disappeared from people's memories. We hope to re-discover and preserve what is left and revive it in the school for the future generations to look back on with pride.

**The Transformed Thulir Campus**

Those of you who haven't been here for some months will now find the Thulir Campus greatly changed! The old workshop and the small Thulir classroom have become residences. Two families stay there: Senthil and Rajammal with Rishi, and Ravi, and Ambika with Ishanth. Senthil and Rajammal have a thriving mixed crop garden in front of their house which has a special beauty and draws everyone's attention. The big classroom has lost all its books and teaching materials but is still used as a classroom for the basic technology students. It also doubles up as a dormitory space for big groups of guests. The office room is a guest room now. Most of the classes are now conducted in Professor's house, i.e. the earlier guest rooms!

**Towards a New School...**

Planning has begun for the new school. We are still looking for suitable land. We then have to construct the buildings as per government regulations.

As a first step we have started a small pre-school for 24 children (aged 4 and 5). Most of them are children of the staff of the hospital and Thulir. Professor Ravindran and Sri. Nagarajan have generously permitted their house to be modified and used as classrooms temporarily.

Our search for suitable land for the new school has not been successful. It almost was. We had located a suitable piece of land and we were all set for the registration. But the pre-registration survey showed many discrepancies between the actual boundary in the field and the documents. So finally the deal had to be cancelled.

Legally the Trust cannot buy tribal land. Non-tribal owned lands are few here in the valley and most of the owners live outside. That is the reason for the delay.

**New Energy and Cheer**

Thulir has always had children of various ages occupying it at all times of the day and night. But now for the first time we have a group of 24 young pre-schoolers the whole day. These children have brought new life to the campus! Their enthusiasm, energy, innocence, laughter, learning and play have transformed the days in Thulir.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/03/P1020356.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/03/P1020356.jpg)

**The Basic Technology Course group**

We had planned to discontinue the basic technology course for teenagers this year as well,  in order to focus our energies towards the new school. However, 5 teenagers found their way to us. Their parents insisted that even if we had no time to teach them we should at least allow them to stay on campus and take part in the work here. They have no other place to go to! All these students have not 'dropped out' of school. They have been perceived as 'academically challenged' and have been 'persuaded' to drop out by their teachers in the 8th/9th classes! The schools are under pressure to show ‘cent percent' pass in the class 10 public exams.

Years of corporal and verbal punishment in the  schools has made them extremely diffident and silent. But they are a sincere and earnest group. Sakthivel is in charge of them. Though we are not able to conduct an intensive basic technology course as we did for earlier batches, the group is blooming and growing more confident, vocal, cheerful and bright! The pictures above and  below show them learning to build Nubian vaults in sun dried bricks (adobe).

[![](http://www.thulir.org/wp/wp-content/uploads/2016/03/P1020402-e1457417376925.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/03/P1020402-e1457417376925.jpg)



[![](http://www.thulir.org/wp/wp-content/uploads/2016/03/P1020281-e1457417543854.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/03/P1020281-e1457417543854.jpg)



**Evening Classes**

The evening classes have been temporarily stopped this year. We needed to focus all our energies on the new school. The government schools also function for longer hours nowadays. Teachers come more regularly and students have more tests and assignments. So the number of students coming to Thulir has also reduced. Even those that come are often exhausted after a full day of work at school.

Also, after 11 years and around 500 students, we felt we needed to stop, take a breather and reflect on this programme and assess the needs of the community instead of just carrying on!

**The school working Committee**

****Ravi, Manjunath, Ramesh, Prema, Anu and the teachers meet at least once a month to discuss the  policies and functioning of the new school.

**Further Training and Exposure for the Thulir Team**

It was felt that the Thulir team members needed additional training and exposure in order to take on the huge challenge of the new school. As a first step, Senthil has shifted temporarily to the hospital for further training and exposure. He will work with Manjunath, who co-ordinates the organic farmers' association here, to get an experience of interacting with the community and the government offices outside. He will also help the Porgai artisans' group with their accounts, thus gaining a wider experience of different kinds of accounting.

**Bamboo Flowering**

[![](http://www.thulir.org/wp/wp-content/uploads/2016/02/IMG_20160210_100925163.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/02/IMG_20160210_100925163.jpg)

The bamboo trees in Thulir and throughout Sittilingi are flowering! Bamboo flowering is a rare event that occurs every 60 to 130 years. Producing flowers and seeds requires a massive amount of energy. As a result, the bamboo plants usually die. We expect the surrounding area to look quite different afterwards. Interestingly, when a particular species of bamboo flowers, the plant relatives and ancestors of that species will flower worldwide!

**Running!**

Some of the government school children still continued to come in the mornings till September to train for the long runs! Senthil and Rajammal were in charge of this programme. The Thulir team participated in three running events this year.

9 of them participated in the Kaveri marathon on September 18th. Although the younger students trained for it they couldn't participate as it coincided with their exams.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/02/kaveri-marathon-e1455436628595.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/02/kaveri-marathon.jpg)

The same group participated in the Bangalore marathon on October 18th.

The younger students and the teachers participated in the Ultra marathon and ran the 12 k and the 21 k on November 8th.

**New Trustee**

Dr. Ravi Manohar joined the Board of Trustees as a trustee earlier this year. Dr. Ravi was in Sittilingi in 2003/2004 when Thulir started.  He then left to do higher studies in the UK and Oddanchatram.  He returned to Sittilingi with his wife Prema and daughter Varsha a few years ago. He has always been a part of Thulir's efforts. He is now one of the main members of the group initiating the school. His 5-year-old daughter, Varsha, was one of the first students of 'Kutty Thulir' and now the pre-school. In October, Dr. Shylajadevi Menon stepped down as managing trustee due to health reasons and Dr. Ravi was unanimously chosen to be the new managing trustee. His addition to the Board of trustees has indeed given an impetus to the work.

**Workshops on Traditional Art and Music of the Valley**

[![](http://www.thulir.org/wp/wp-content/uploads/2016/02/bamboo-workshop.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/02/bamboo-workshop.jpg)

Ravi's uncle and father are from Echangadu, a village in the Kalrayan hills abutting our valley. They taught us the joys of toy-making using coconut leaves. They have also started us on bamboo basket making.

Vellachi Ammal, from Thekanampattu, has started coming once a month to teach us traditional songs and dances of the valley.

**Parents' Meetings**

Education here includes educating the parents by engaging the parents in a dialogue and making them a part of their child's educational process. If the situations and values are vastly different at home and school, the child suffers. We have had three major parents' meetings since June.

The last one on January 7th was very positive and left all of us extremely satisfied. Parents also got a chance to use their hands and wits when they were given different jigsaw puzzles and asked to solve them. Then they were asked to give their feedback on their child’s progress. We were pleasantly surprised and happy when parents remarked that their children were now intensely curious and asked many questions. All of them said that their kids could not wait to get to school in the mornings! We went on to discuss the pros and cons of a pedagogy which concentrates on only academics versus one which integrates art, sport and physical activities with academics as it is in Thulir. Some parents complained that we take their children every Friday for a walk to the forest and that we allow them to climb trees! So a discussion on the importance of Nature education and being with Nature ensued. After this the children came out of class and put up an impressive performance for their parents. Their complete lack of stage fright impressed many parents. Then each child took his or her parents to the class and showed them all the work done so far.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/02/parents-puzzling-over-jigsaws-e1455439148661.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/02/parents-puzzling-over-jigsaws-e1455439148661.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2016/02/students-performance-for-parents-e1455439292488.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/02/students-performance-for-parents.jpg)

We were a whole group of happy and proud parents, teachers and children that evening!

**Volunteers and Visitors**

As always many friends and fellow-seekers visited us over the months. As one young friend, Shankar from Delhi remarked after seeing the number of visitors we had, “I never knew you had such a hectic social life in Sittilingi! If you need solitude, please come to Delhi!”

Vijayalakshmi from Vellore volunteered in Thulir for a month before starting to study her B.Ed. She was a part of the teachers group here and took part in all the activities.

Lami, who was on a sabatical from her bank in Mayiladuthurai, volunteered in Thulir for some weeks. She helped the children with their English.

Sakthivel, a computer professional from Chennai, while volunteering in Thulir, translated many essays on Education from English to Tamil so that it could be used by the teachers here.

We were very happy to have the teachers from Marudam Farm school for a few days here. We had many interesting and stimulating discussions with them.

40 young teachers from 'Teach for India' visited on October 31 st. The discussions with them continued even after the session was over, with much enthusiasm.

The ultra-marathon runners group from Runners' High came for their training weekend in September.

Ajay and Neha from Asha Bangalore came for a visit.

Ramkumar and Archana from Bangalore visited in November. They are toying with the idea of visiting Sittilingi on a more permanent basis over the coming years.

Christina and Sarah, medical students from Germany, made a slide show about Germany.

Nondiya from Nagaland and Nisha from Meghalaya took some classes for the children, taught some of their songs and talked about their homes.

Lea, a medical student from Hungary, gave a presentation on Hungary.

Franziska and Sabine, medical students from Germany, talked about their country and taught some English songs and games.

Mithun from the US also joined them for these classes. He also engaged the teachers in some conversational English classes and helped with this newsletter!

We hope to make geography more personal and engaging for the children through all these interactions with people from different countries.




*******************
