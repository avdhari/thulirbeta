---
author: thulir
date: 2007-08-09 10:47:00+00:00
draft: false
title: News Update August 07
type: post
url: /wp/2007/08/news-update-august-07/
categories:
- Newsletters
---

[![](http://bp2.blogger.com/_qz2j5uas_y0/R71Y4E177-I/AAAAAAAAAJU/pJEyZD-Px0s/s320/NIOT_TTuCamp_Sri+115.jpg)
](http://bp2.blogger.com/_qz2j5uas_y0/R71Y4E177-I/AAAAAAAAAJU/pJEyZD-Px0s/s1600-h/NIOT_TTuCamp_Sri+115.jpg)  


Timbaktu students singing in Thulir  
  


Its been a while since we wrote anything in this blog [the last posting was in March]. We have been meaning to write but have been caught up in the excitement of so many happenings at Thulir that we kept postponing writing "just for a few days" , till its been quite a while. Apologies to all of you who might have been wondering what has happened. So far, often it has been crises of various kinds that had been the excuse for not writing. Rest assured, that this time it has not been setbacks at work, but  rather positive developments and being caught up in activities that has stopped us from writing.  
  
Visitors to Thulir!  
  
--Timbaktu children visit Thulir  
  
  
[![](http://bp3.blogger.com/_qz2j5uas_y0/R71YMU1775I/AAAAAAAAAIs/A7oClUs51dY/s320/NIOT_TTuCamp_Sri+007.jpg)
](http://bp3.blogger.com/_qz2j5uas_y0/R71YMU1775I/AAAAAAAAAIs/A7oClUs51dY/s1600-h/NIOT_TTuCamp_Sri+007.jpg)  


We have just recovered after an exhausting 5 day camp at Thulir with 12 visiting students of Timbaktu school. During this stay, the Timbaktu students taught us to play the drum, dance traditional dances, sing along; make jewellery, and embroidery. They learnt Tamil words from Thulir children while teaching Telugu words to them. It was 4 intense days of cooking, cleaning, dancing, singing, bathing and swimming in the river. For most students it was their first experience of interacting closely with others of their age, speaking a different language. Intermittent rain and the soggy earth and air did little to dampen the spirit of the children.  
Coming close at the heels of the earlier limited interaction with Timbaktu school [see below “senthil and Perumal's continuing journey”], this visit has helped to strengthen the collaboration between Thulir and Timbaktu and we are optimistic about sharing our resources and learnings. We have planned a trip for Thulir children to visit Timbaktu in end September.

  
--Sridhar and Sriranjani's visit:  
[![](http://bp3.blogger.com/_qz2j5uas_y0/R71YNU1778I/AAAAAAAAAJE/DqQpkffNPqk/s320/NIOT_TTuCamp_Sri+092+%28Modified%29.jpg)
](http://bp3.blogger.com/_qz2j5uas_y0/R71YNU1778I/AAAAAAAAAJE/DqQpkffNPqk/s1600-h/NIOT_TTuCamp_Sri+092+%28Modified%29.jpg)  


As I write this, Sridhar and Sriranjani along with their daughter Janavi from Asha Princeton chapter just left after visiting us the past 3 days. Its been nice to meet them in person after all these years of email / phone contact. Asha Princeton has been supporting our fellowships the past 3 years which has been a very crucial area of support for us. The visit gave us an opportunity to communicate happenings at Thulir and also explore future activities together. One of the feedback we got from talking to them is that our blogs are read and appreciated by many of you, and that we should continue posting regularly. That explains why we have promptly sat down to write this! Of course we do know that there are friends who  do read what we post and often write back to us and ideally we should be writing regularly; but then a bit of positive feedback does help us to get charged!  So  do write to us if you can!  
  


Sridhar took a couple of sessions on costing [using spread sheets on the computer] of our  white LED torches made with bamboo with the students. He also demonstrated some experiments using magnets with the evening session children. Sriranjani, who is a trained Montessori teacher, demonstrated use of Montessori material that we already have to Gowri, Rajamma and Devaki who teach the younger children [5-8 ages] in Thulir. Being a trained singer, she also taught songs to the students.  


  
---Anita Balasubramanian's visit:  
[![](http://bp1.blogger.com/_qz2j5uas_y0/R71Y301779I/AAAAAAAAAJM/t92g50EI_KA/s320/NIOT_TTuCamp_Sri+102+%28Modified%29.jpg)
](http://bp1.blogger.com/_qz2j5uas_y0/R71Y301779I/AAAAAAAAAJM/t92g50EI_KA/s1600-h/NIOT_TTuCamp_Sri+102+%28Modified%29.jpg)  


Anita who is no stranger to Thulir visited us for 4 days. She has been a source of constant support from the beginning and she has seen Thulir at various stages of its development. So it was good to have her again in Sittilingi. She took sessions in embroidery, electronics, and taught children to make balloon animals. She also  lead  a discussion on life in the USA, which lead to a lively discussion on the effects of TV watching.

  
Positive Developments  
  
--Basic Technology course:  
  


The first  batch of Basic Technology  course has  completed the course.  2 of the boys Balu and Maadhu have gained enough confidence to go back to school to attempt class 10 exams. We feel this is an indication of their increased self confidence. Mohan is well after his heart surgery [Thanks to generous support from friends of Thulir] and he too is preparing to write his 10th exams. Senthil and Perumal have joined Thulir as Fellows on an enhanced stipend of Rs. 1000 per month. They will explore possibilities of enhancing their skills and facilitate some hands on workshop exercises for the younger children.  


  
   ----Senthil and Perumals's continuing journey  
[![](http://bp1.blogger.com/_qz2j5uas_y0/R71YM01777I/AAAAAAAAAI8/h2wv7kOdiKQ/s320/NIOT_TTuCamp_Sri+067+%28Modified%29.jpg)
](http://bp1.blogger.com/_qz2j5uas_y0/R71YM01777I/AAAAAAAAAI8/h2wv7kOdiKQ/s1600-h/NIOT_TTuCamp_Sri+067+%28Modified%29.jpg)  


Senthil and Perumal from the first batch of Basic Technology Course are continuing to be with us at Thulir to further develop their skills as well as to pass on some of the things they have learnt of to other students at Thulir. They have been part of many exciting activities these past 2 months.  


  
       **Trip to Bangalore  
  


First in June, they travelled alone to Bangalore to get an inverter repaired. At Bangalore, a friend Ramasubramanian hosted them and arranged for an exposure to metal workshops where they watched a micro hydro turbine being fabricated. Senthil was actually offered a job in one of the workshops after he spent a day there. As a result of this interaction, they have been offered a trip to a remote Orissa village where the micro hydro plant is to be installed. They would participate in the installation of the machinery as they are skilled in plumbing and electrical wiring.  


  
**Project at Gubbi village near Bangalore  
  


From Bangalore, they travelled with Krishna to Gubbi, a village near Tumkur in Karnataka [a bout 100 kms away from Bangalore]. Here in a farm house that is not connected to the electrical power grid, they along with two students from Timbaktu school [Subbu and Sai], installed a solar panel based power system and did the wiring of the whole house. It was a valuable experience in working with complete strangers who speak a different language! Of course, Krishna from Thulir and Subba raju from Timbaktu were with them to help organise themselves as well as to communicate. This 2 day experience gave so much confidence and was such a morale booster that we thought we should do more such activities. From Gubbi we went to Timbaktu to spend a couple of days and look at some of their activities. This helped in further bringing Thulir and Timbaktu students together. At Timbaktu we met Ramudu who is a senior student now on a fellowship there, and also saw how he is teaching the younger children all that he has leant especially hand-skills.  


  
**Visit to NIOT, Chennai  
[![](http://bp0.blogger.com/_qz2j5uas_y0/R71YMk1776I/AAAAAAAAAI0/bR2NqZX-5cg/s320/NIOT_TTuCamp_Sri+049.jpg)
](http://bp0.blogger.com/_qz2j5uas_y0/R71YMk1776I/AAAAAAAAAI0/bR2NqZX-5cg/s1600-h/NIOT_TTuCamp_Sri+049.jpg)  


Immediately after the above trips, an opportunity for attending a week long training programme at the NIOT [National Institute of Ocean Technologies, Chennai] came to Senthil and Perumal. We requested that Timbaktu students too be offered this training and so Senthil, Perumal along with Ramudu and Subbu from Timbaktu attended this programme at NIOT. The training was on basics electronics of Solar panels and charge regulators along with assembling of white LED based lighting fixtures [table lamps, ceiling lamps and torches]. Timbaktu and Thulir students have been making white LED products for a while now and this programme gave an opportunity to take this activity further by better understanding of the technology as well as improving the products. A special feature of Thulir and Timbaktu work has been the use of Bamboo in these products.  


  
  
Girl drop-outs join:  
  


Six drop-out Girls have joined Thulir with the idea of writing their class 10 exams. They spend the whole day at Thulir and we are in the process of designing additional inputs apart from tuition for the exams, with the idea of boosting their confidence levels somewhat in the manner of the Basic Technology course. This is a happy occasion for us as we were aware that many adolescent girls don't  come to Thulir as their movement gets restricted by their parents. We have been wondering how we could encourage more girls of the older age group to come to Thulir.  


  
--Devaki and Rajamma join  
  


From the above group, two of them, Devaki and Rajamma have been offered a stipend to help them to come back to Thulir. Family circumstances had forced them to go for daily wage labour. Both of them have one more subject each to complete their 10th class. They would be learning to help 5 to 8 age group children learn in Thulir in their evening sessions. We also needed additional hands now as Gowri is leaving Thulir to pursue higher studies and Selvaraj too is working 3 days a week at THI helping the organic farmers group.  


  
Construction/ building renovation at Thulir  
  


During the month of May we closed Thulir for the school going children. The rest of us, staff and senior students, spent the time, renovating Thulir structures. Most of Thulir buildings are built with mud walls and mud floors. After 3 years of use, the mud wall plastering and flooring had to be redone. This while being a physically exhausting work, especially given the summer heat, was very satisfying. It was also nice for the team to work together. We now have Thulir buildings as good as new!!  


  
Earlier events:  
  
   --2 day Math Camp [ March 07]  
  


[![](http://bp0.blogger.com/_qz2j5uas_y0/RrtVr1-8jSI/AAAAAAAAAFI/ZtipAbClfN0/s320/2.JPG)
](http://bp0.blogger.com/_qz2j5uas_y0/RrtVr1-8jSI/AAAAAAAAAFI/ZtipAbClfN0/s1600-h/2.JPG)On 24 and 25th of Feb,  we held a two day math camp. 45 children came and stayed the weekend at Thulir. Sanjeev and Anita during their visit earlier that month had introduced plotting graphs. So we mostly explored graphs during these days, using the temperature measu[![](http://bp3.blogger.com/_qz2j5uas_y0/Rr8vyl-8jnI/AAAAAAAAAHw/xhIW7uXGN4Y/s200/3.JPG)
](http://bp3.blogger.com/_qz2j5uas_y0/Rr8vyl-8jnI/AAAAAAAAAHw/xhIW7uXGN4Y/s1600-h/3.JPG)rements that we record in Thulir using a max min thermometer. We also had a session of solving folk math puzzles. Mehboob Subhan who was visiting us then, helped with sessions on introducing Sudoku puzzles. Apart from math, the children were keen to put up short skits [plays]. Seven groups put up plays with limited preparation time. It was an engaging and enjoyable 2 hour programme, and came as an eye opener to us!  
                               

  
  
--Annual day/ Therukkoothu performance                                      [April 07]:  
  


  
[![](http://bp3.blogger.com/_qz2j5uas_y0/Rr8tEl-8jjI/AAAAAAAAAHQ/SG7nqF1kNjk/s200/5.JPG)
](http://bp3.blogger.com/_qz2j5uas_y0/Rr8tEl-8jjI/AAAAAAAAAHQ/SG7nqF1kNjk/s1600-h/5.JPG)The children were keen to celebrat[![](http://bp3.blogger.com/_qz2j5uas_y0/Rr8sZl-8jiI/AAAAAAAAAHI/nMgeLvJ0ZZU/s200/4.JPG)
](http://bp3.blogger.com/_qz2j5uas_y0/Rr8sZl-8jiI/AAAAAAAAAHI/nMgeLvJ0ZZU/s1600-h/4.JPG)e April 14th – Thulir's birth date. They decided to have a cultural programme. They planned, created, practised and put up the performance themselves. The audience was [ ](http://bp3.blogger.com/_qz2j5uas_y0/Rrtail-8jXI/AAAAAAAAAFw/STteGyTI9rs/s1600-h/6.JPG)only other students. The performance went off so well we decided to repeat it for the parents and other villagers. The cultural programme took place on the Thulir playground on April 21st.  


  
  
  
[![](http://bp1.blogger.com/_qz2j5uas_y0/Rr8u9F-8jlI/AAAAAAAAAHg/awrJJFRisvc/s200/7.JPG)
](http://bp1.blogger.com/_qz2j5uas_y0/Rr8u9F-8jlI/AAAAAAAAAHg/awrJJFRisvc/s1600-h/7.JPG)We also decided to invite the Sittilingi traditional street play  ["therukkoothu"] group to put up a performance in the Thulir grounds. Therukkoothu , a vibrant[![](http://bp2.blogger.com/_qz2j5uas_y0/Rr8tiV-8jkI/AAAAAAAAAHY/slzZwJaoimo/s200/6.JPG)
](http://bp2.blogger.com/_qz2j5uas_y0/Rr8tiV-8jkI/AAAAAAAAAHY/slzZwJaoimo/s1600-h/6.JPG) folk theatre form has been in existence in the valley and has been popular too. Of late it has been slowly dying out with TV and DVD player taking over. A group of Sittilingi youth have been encouraged to learn the  traditional art by Thulir. It helps that Selvaraj, Thulir staff, has been a keen learner, and that a teacher of this art had also moved to Sittilingi village a couple of years back. After almost a year of training, the young troupe has been successfully performing at events in Sittilingi.  
  
  


[![](http://bp3.blogger.com/_qz2j5uas_y0/Rr8vRl-8jmI/AAAAAAAAAHo/GN57IqhQpNY/s200/8.JPG)
](http://bp3.blogger.com/_qz2j5uas_y0/Rr8vRl-8jmI/AAAAAAAAAHo/GN57IqhQpNY/s1600-h/8.JPG)

[![](http://bp3.blogger.com/_qz2j5uas_y0/Rr8wil-8joI/AAAAAAAAAH4/GXGpOjrLZMY/s200/9.JPG)
](http://bp3.blogger.com/_qz2j5uas_y0/Rr8wil-8joI/AAAAAAAAAH4/GXGpOjrLZMY/s1600-h/9.JPG)So on the night of 26th April, the therukkoothu was performed at Thulir grounds. It was a Mahabaharatha story of “Jarasandran”, and the performance started at 10 pm and went on till 6 am!!  
  
  
  


********************
