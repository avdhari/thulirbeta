---
author: thulir
date: 2010-04-27 16:53:11+00:00
draft: false
title: Newsletter December 2009 to April 2010
type: post
url: /wp/2010/04/newsletter-december-2009-to-april-2010/
categories:
- Newsletters
---

[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-01.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-01.jpg)


You may have been following the BT Course updates through the monthly diaries we have uploading. In this New letter we focus more on happenings that have been left out in the diaries though we have also mentioned some of the events again here.

We have a group of 35 -40 primary school children coming in the evenings, after school hours. Girls outnumber the boys in this group. As we wrote earlier, we noticed that these children have developed an interest in reading books.This interest continues and activities based around reading are still popular -- such as reading out various stories to them, asking them to tell stories to the rest of the group, acting out plays, writing out stories, making story cards etc.

In Math we continued teaching the four basic operations .Measuring lengths,  volumes, and weights and time practically was very popular. Along with this a lot of mental math too was done. We keep  trying out  various practical activities that can clarify basic Math concepts to this group. This is an ongoing process.


[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-02.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-02.jpg)


** **

**Science day in Govt. School:**

** **

The local Govt. School Headmaster requested Thulir to help their students put up a science exhibition in the school, 2 days before the event! Thulir students decided to take it as a challenge and after hectic preparations, prepared exhibits/ demonstrations and taught the school children how to demonstrate and explain the concepts.

We thought this presented a great opportunity to our senior students, who are mostly school drop outs and so are easily dismissed by the teachers/ local community as not good in studies, to prove themselves. They worked very hard preparing for the event in a short time, and then went to the school and taught the school students how to demonstrate and explain the experiments and models.

After the science day celebrations at the Sittilingi Govt. high school, we did the same experiments in the evening sessions at Thulir, giving more space and time to the children to try out the various experiments/ models made.


[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-15.jpg)




[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-16.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-16.jpg)




[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-17.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-17.jpg)


** **

**Vistiors:**

** **

Our students had opportunities these past weeks, to interact with a wide range of visitors these three months. We had visitors from Jarkhand, Tiruchy, UK, Japan, Germany, New Zealand and Uganda. We had interactive session where the children asked  and learnt from the visitors, about their countries -- place, people, language, food, crops, animals etc.


[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-05.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-05.jpg)




[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-06.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-06.jpg)




[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-07.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-07.jpg)


** **

**Sports Day Celebrations:**

** **

The senior students decided to organise a sports  weekend in Thulir. They made a list of events keeping in mind giving opportunities to both the athletically good and the athletically not so good students. They came up with some interesting unconventional events like picking beads, carrying and filling water into a bottle using only hands [without the use of any containers], etc.  They then made lists of participants and along with the younger group started getting our grounds ready. As the Thulir playground is quite small, we used our neighbours field which is lying fallow. Both these grounds had to be manually  cleaned and surfaces levelled, then the running tracks had to be measured and laid. All this took 3 days to finish.

The participation was enthusiastic, despite the warm weather; 78 students participated in one or the other event. All  participants [irrespective of their position in the event] were given prizes of books to read! While this was a new idea, it was taken well by the students and there was much excitement in checking each other's book and figuring out which stories were familiar.


[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news20.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news20.jpg)




[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news21.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news21.jpg)




[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news22.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news22.jpg)




[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news23.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news23.jpg)




[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news24.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news24.jpg)




[![](http://thulir.files.wordpress.com/2010/04/news25.jpg)
](http://thulir.files.wordpress.com/2010/04/news25.jpg)




[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-26.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-26.jpg)


** **

**Organic Gardening:**

** **

In January, we harvested the small experimental patch of SRI  organic paddy crop. The local farmers felt that it was a good crop. So now we have a, new larger SRI plot [one fourth of an acre]. The seniors group took a lot of  initiative planning, physically  getting the land levelled and ready for paddy. They were helped by the SVAD team from THI, which is a collective of organic farmers in our area, in planning and planting of the paddy saplings. However, we have subsequently had  a water crises as our well  water level has gone down and recharging is slow. So this made us look at water saving techniques.  Recommended by SVAD, we visited Solitude farm in Auroville to learn water saving techniques. We have now started mulching the field to save water. Along with this, they are learning how to measure land areas, keep accurate records of  inputs into the farm- including water and labour,  keep a graph of average  crop growth every week etc.

** **

**Marathon running:**

** **

The long distance running programme continues and now we have more participants. On Feb 14th , after 8 weeks of preparations, our seniors went to participate in the Auroville marathon. Siva who ran the half marathon [21 kms] came first in his category and  Rajamma and Devagi, who both ran a 10 km event for the first time, came 1st and 2nd respectively in their category. All participants from Thulir managed to finish their run distances and this was a great boost to the running programme in Thulir.

** **

**10th standard Public Exam:**

** **

We had 2 of our B T Course students writing the class 10 exams this year. They were helped in their preparation by Ravi, who used to be a staff at THI.

** **

**Prof Ravindran's classes in February:**

** **

Prof Ravindran and Mrs Vanaja Ravindran visited Thulir and held classes in February for a week. Prof. Ravindran's classes were on servicing simple gadgets [has stove/ mixie]. There was also a session on completely dismantling a mixie into its components, with the idea of learning its various parts as well as the skill of disassembling a machine using appropriate tools. Mrs Vanaja Ravindran took Tamil reading and writing classes.

** **

**Dr Carolyn's English class.**

** **

Dr Carolyn Lomas is back in Sitilingi, visiting. She has been taking English classes the past few weeks. This is of great benefit to the students as she is good at teaching English as a Foreign language and also knows Tamil.

** **

**Ramsubbu's classes on Solar Photovoltaics:**

** **

Ramsubbu took a class on design and maintenance of Solar PV systems. This is an important area as Thulir's PV system too needs maintenance and occasional expansion/ addition.

** **

**Farewell to Vinu:**

After being in Thulir for two and a half years, Vinu has gone back home to “Kanavu” in Wayanad. He will be joining the senior students of Kanavu in organising new activities there. He plans to teach Art to younger children and restart the pottery and clay work unit in Kanavu. He takes back with him experience [that he gained while in Thuir] in making hand crafted items and marketing them. We will miss this gentle artist and a good teacher. We wish him good luck!!


[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-30.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-30.jpg)


** **

**Other events in Brief:**

** **

In Januray, Pongal was celebrated with usual fervor in Thulir. We held the customary Kolam evening. This year for the first time we celebrated “mattu pongal” . Read more in >  [January Diary](http://thulir.wordpress.com/the-bt-course-diary/january-2010-diary)


[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-35.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-35.jpg)




[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-36.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-36.jpg)




[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-37.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-37.jpg)


During this month we also had **a Bee keeping workshop** conducted by Mr justin of Keystone, Kotagiri. Read more in > [January Diary](http://thulir.wordpress.com/the-bt-course-diary/january-2010-diary)

Perumal and and vijayakumar went from Thulir along with Anu and Krishna to conduct a 5 day** Bamboo and LED torch making workshop **in the Rural Education Centre run by Rishi Valley School. Read more in > [February Diary](http://thulir.wordpress.com/the-bt-course-diary/february-2010-diary)

** **

** **

** **

**Sanjeev and Anita visited end December.**They continued their work teaching Electronics and new songs. They have written a [detailed report](http://smallisbeautiful.blogspot.com/2010/02/research-thulir.html). You can also read/ see pictures in our [December Diary](http://thulir.wordpress.com/the-bt-course-diary/december-09-diary)


[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-40.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/news-40.jpg)







_Hope you enjoyed reading this newsletter. Do leave comments; we would be happy to hear from you._




**********************
