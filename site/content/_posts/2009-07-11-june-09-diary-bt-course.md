---
author: thulir
date: 2009-07-11 11:19:20+00:00
draft: false
title: 01 June 09
type: post
url: /wp/2009/07/june-09-diary-bt-course/
categories:
- Life Skills Course Diary
---

**June 09**




**Plumbing line for irrigation**




Since the organic vegetable plot is slowly expanding, we decided to lay PVC irrigation pipes to take water from the open well for the plants. This work was executed by the students in the first week of June







![plumbing work](http://www.thulir.org/wp/wp-content/uploads/2009/07/btcourse06plum06.jpg)





![plumbing work](http://www.thulir.org/wp/wp-content/uploads/2009/07/btcourse06plum05.jpg)





![plumbing work](http://www.thulir.org/wp/wp-content/uploads/2009/07/btcourse06plum04.jpg)





![plumbing work](http://www.thulir.org/wp/wp-content/uploads/2009/07/btcourse06plum03.jpg)





![plumbing work](http://www.thulir.org/wp/wp-content/uploads/2009/07/btcourse06plum02.jpg)








**Making Torches with White LEDs and bamboo**




For the past 2 years Thulir has been making Bamboo torches that use white LEDs. This has been one of the popular products made in Thulir and one that is made with a lot of enthusiasm by most children. So the Course students too were initiated into the making of these torches.




![torch making](http://www.thulir.org/wp/wp-content/uploads/2009/07/btcourse06torch007_1.jpg)





![torch making](http://www.thulir.org/wp/wp-content/uploads/2009/07/btcourse06torch006_1.jpg)





**White LED bulbs using waste CDs and paper mache **




At thulir now we are shifting over to white LED bulbs that we run on DC using the Solar powered Batteries. Our students are trained in assembling these bulbs [there is a current controlling circuitry which needs to be added]. For the Bulb fixture we have been trying various ideas, such as bamboo shell, paper mache with discarded CDs[ pic below] etc.







![white LED bulb](http://www.thulir.org/wp/wp-content/uploads/2009/07/blogpic0906_003.jpg)



**Learning to make Friendship bands**

For a change, a different skill was introduced – one of making bands using different coloured cotton threads, creating interesting patterns.

[![1](http://www.thulir.org/wp/wp-content/uploads/2009/08/12.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/12.jpg)

[![2](http://www.thulir.org/wp/wp-content/uploads/2009/08/2.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/2.jpg)

[![3](http://www.thulir.org/wp/wp-content/uploads/2009/08/31.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/31.jpg)
