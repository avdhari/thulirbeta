---
author: nimda321
date: 2013-09-14 07:28:25+00:00
draft: false
title: 'Notes from Florence Esmieu, Volunteer '
type: post
url: /wp/2013/09/notes-from-florence-esmieu-volunteer/
categories:
- Reflections
- Reports
---

##  Overview of my stay in Thulir – an Alternative Education Centre




### October and November 2012




### Florence Esmieu


[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/f1.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/f1.jpg)

**_Series of questions and answers with Anu (co-founder of Thulir) with my added impressions and thoughts:_**


### 1) Is all theory taught through practice? Are there theory classes as well? How do you make the link between the two?


Overall not all theory is taught through practice. Anu and Krishna (the founders) have proven that their teaching method is flexible to deal with local constraints. The school is in a remote area therefore a lot of material can be very expensive (budget constraint) and not easily purchased. Thulir’s method of education is parallel to the founders’ own way of living: centered on the use of what one has around oneself. Therefore priority is given to local material for environmental purposes; this goes back to Gandhi’s principle of giving the priority to local elements.
The method of teaching used is to demonstrate theory through practice as it is one of the main problems of the schoolchildren’s education in government schools. In the latter, the learning process is mainly focused on books and theory with very little application to practice.
One of the missions of Thulir is to help compensate the problems of public teachings.
Furthermore, Anu and Krishna have observed that young children pick up very fast after they have been taught elements through practice.
Young adults also have regular theory classes directly related to the practical work they have done on campus. One example is after having built the hen house, the students had to calculate many things such as the size of the house or how much material they had used altogether. Another illustration is after having done electrical work, the students made circuit diagrams or when they make soaps, they have to calculate how much the cost of one would be. Subjects such as math or physics, are approached through theory and practice to facilitate and accelerate the learning process of the students.
The theory problem sheds light on one of the main goals of Thulir: to promote a multi- faceted, well-balanced and what the founders call a Meaningful Education. The goal is to focus, not only, on intellect and the use of one’s head but to promote practical work and the use one’s hands as equally important. Finally, another fundamental aspect of education is to understand one’s surroundings, finding one’s place within one’s community and caring for it with one’s heart.
All of these aspects are to be considered as complementary and equal, one of the main issues of mainstream education is the disequilibrium of an over-emphasis of theory, paired with a disdain and neglect for practical work and emotional development. By promoting intellectual, practical, emotional maturity and artistic work and highlighting their value, students can gain self-confidence through increasing progress and can start looking for a vocation that they are interested in.


### 2) How do you measure students’ progress? Do you grade them in any way?


Anu and Krishna do not believe in the traditional grading system as they are convinced that education is much more than grading. Thus, the students at Thulir are not graded but the schoolchildren keep a record of their works in files and exams are in place. Moreover, the schoolchildren and the full time students answer a series of self-evaluation questionnaires every 6 months. The latter is a feedback of what they have learnt, what they feel is missing and how do they feel they have evolved compared to the start. This relatively unorthodox method implies an adaptability of the learning curriculum – evolving according to the students’ own positive impressions and criticisms and not solely focused on the teacher’s assessment. The evaluation method is illustrative of the value that is given to the interaction between the teachers and young adults and the schoolchildren and the importance of their own involvement in their education.
Anu and Krishna finally came up with this method of evaluation after 8 years of applying other systems. The feedback that has been provided in the past has been quite challenging but very interesting for the founders as they have had to adapt and to find creative solutions to suit the children’s progress and needs. Moreover, they found that the students had gained increasing confidence not only by participating in their self-evaluation but also through their perception of classes in Thulir and how it can progress.


### 3) Regarding the Right to Education which is compulsory for all children under 14: how is that implemented at Thulir?


The schoolchildren do not fall in that bracket as it is not a formal school but represents supplementary education. Moreover, they are in Thulir only a couple hours per day maximum. As for the other group of young adults, they are above 14 so they do not fall in that category either.


### 4) When did Thulir start? Why did you come here in the first place? And what are the center’s goals?


Thulir started in 2004 with the help of the NGO Tribal Health Initiative. Anu and Krishna used the network of health animators and hospitals of the NGO to visit all the schools in the area. Additionally, they visited all government schools and established surveys to collect data for some background check. They placed great importance on the interaction with the local inhabitants to really grasp what was needed in terms of education. The founders understood the need for two different projects to be set in motion.
The first project aims at strengthening the level of education of these children that are for the majority of them, the first generation of their families to ever attend schools. After-school education is intended to reinforce basic academic skills for the students, in order to compensate the generally poor quality level of government education. Thulir’s purpose is also to offer these children the opportunity to use a playground, participate to arts and crafts, sports and other activities.
The second project is designed to assist those older students (usually young adults) that either have failed school or are in need of support to find a fulfilling profession. These students generally lack in self-confidence and are in need of educational and moral support when they dropped out of schools. In order to prevent some of these young adults to work in factories or the textile industry where the working conditions are very poor, Anu and Krishna persuaded them to come to Thulir. The founders focus on helping them to find a decent job without feeling embarrassed about working in the primary sector such as farming.
After observing the help that is provided for these children – for many it was the first time they were given things such as coloring pens, geometry tools or a ball – I realized their work is truly necessary for these communities. Thulir’s work is similarly impressive regarding the young adults. The founders’ dedicate a lot of their time and energy to counsel the group of adults and their families whilst constantly making sure they are respecting their autonomy as well as the students and family’s will. I realized the difficulty of their tasks but they both displayed tremendous patience, courage and good judgment while they worked (for instance: they encouraged families accept the reality of mental illnesses or participated in the sorting out of pecuniary disputes).
Thulir is truly offering an opportunity for these two groups to obtain a Meaningful Education that will have a major impact on their communities by helping them, at different age, to acquire the tools to lead a well-balanced life and have a dignified livelihood. The center’s work goes way beyond education in the mainstream understanding of the word. Anu and Krishna and many others, have truly devoted their life to assisting these communities to overcome challenges linked to modernity and help them find their own place in the Indian society (which historically has never been easy).
Work in Thulir has progressed thanks to Anu and Krishna’s efforts, Thulir’s staff team, Tribal Health Initiative, volunteers and the founders’ networks and probably many others. It is obvious that all of Thulir’s participants’ efforts and enthusiasm have truly made the difference in the fulfilling these projects.


### 5) What have the majority of students become?


The majority of the students have chosen many different paths. On the whole, alumni of Thulir have pursued higher studies, have continued to work for Thulir, have married, work in agriculture, have set up their own business, work in the factory or have learnt how to drive particular vehicle. Generally they decide either to become an apprentice in a particular profession and or to pursue their studies by following Anu and Krishna’s tutoring.
The students that have chosen the path of studies want to gain experience and confidence before working. Many of those wishing to resume studies have finished high school as private candidate under Anu and Krishna’s tutoring and some have managed to obtain a university diploma through distance learning courses. The ones who chose the path of traineeship have worked in workshops such as carpentry or masonry training.
There are many examples of students, including Senthil, who worked administration at Thulir and after studying at the center was able to obtain his high school diploma and is now part of the staff of the school. Rajammal is another example as she finished her high school and obtained a BA in History from the Tamilnadu Open University whilst teaching and studying in Thulir, her goal is to become a teacher for younger children.
One has to point out that within the factors influencing the decision of the students to pursue a certain career path, one of the most important one is family pressure. Some alumni have consequently not all been able to avoid working at the factory, despite bad working conditions, as it represents one of the highest paid jobs.


### 6) Can you name some positive and negative aspects about working in Thulir?


One truly positive aspect about working in Thulir is the possibility of witnessing other key aspects of Anu and Krishna’s educational methods. They organize four sessions each month for all the young adults to guide them to promote their personal growth and development.
These sessions are set up to let anyone speak freely about their personal views and needs.
These discussions revolve around societal issues such as women’s status and role or problems linked to alcoholism. The founders really focus on helping each student on how to relate with others when one is a member of tribal communities in rural areas. One key aspect of these discussions is conflict resolution and the need to improve the quality of communication with others through practical exercises such as role play.
These sessions were built to assist these adults to deal with the turning points in their lives and to find answers to intricate questions regarding identity and relationships. Young adults in the area sometimes face difficulties to find their place within their groups as they represent the first generation to ever go to school. Moreover, they live in an era marked by such drastic technological and societal changes. These alterations have sometimes triggered intergenerational clashes of values that can be hard to handle for many young members of these communities.
What is truly inspiring regarding Anu and Krishna’s philosophy of life, is that they broaden their methods by using a mixture of modern and traditional approach to encourage sustainable development and meaningful lives. Thulir’s founders’ view on modernity is in contrast with the popular view of considering India’s economic boom as the only true indicator of progress.
Another positive aspect of working with Anu and Krishna is their concern for working to establish genuine equality between: rural and urban communities, amongst genders, castes and religions. Their approach is to actually apply this principle of equality through concrete actions, for example establish a rotation of daily and weekly chores for all the staff of Thulir and themselves. Equality is a core belief of their Meaningful Education because it is directly linked to living a dignified life and promoting harmony and peace within a society. Dignity for all will come through equality for all and how one perceives oneself and others within one’s community.
Another positive aspect is living Thulir’s way of life which is very environmentally friendly.
Anu and Krishna have used solar panels for the energetic consumption of the school and their own house. Moreover, the running of the place consumes very little energy which is economically and environmentally interesting. The founders have also used their architectural knowledge to construct traditional mud buildings with added modern modifications. Anu and Krishna have also used the help of engineers to creatively use renewable energy wherever they could. One example of the latter is the use of a cooker using solar energy or cooking with gas stemming from a machine using mainly cow dung! The negative aspects of living and working in Thulir are scarce and are directly related to the challenges of living and adapting in such a remote area and within such a particular lifestyle. Having to adjust to the culture, the people, the climate, the language and the different rhythm of life is in a nutshell the main difficulties of living within Thulir.
Volunteering in Thulir means that you are very autonomous and if one does not know Tamil, there will be some times when one can get lonely. Furthermore, working with young adults and children is lot of work and it is not simple to find the right method but it is not unmanageable, on the contrary!
Overall, one can overcome that temporary feeling of loneliness and difficulty if one decides to make the effort of meeting people and establishing relationships with them, even if it is harder than back home! Thus, I clearly believe that the ‘positive’ aspects of living in this center clearly exceed the ‘negative’ sides. Moreover, I am convinced that these difficulties have made my stay more enriching by enabling me to truly connect with the students and staff of Thulir and really enlightened me on the realities of the daily lives of these communities.


### 7) What do you think about the government and its role in education? How do you think (and if) it should change in some ways?


The government has become more and more privatized in the sphere of education and it is gradually treated more like a business. Thulir’s founders consider that the government should further intervene in the area of education instead of privatizing it. One of the main targets that the Indian or Tamil state should fulfill is to concentrate on not only increasing its role in government schools but also to improve the quality of the teaching. The founders would like to see that education in the government schools be less focused on intellect and academics but also includes practical work – to establish a link with concrete vocations – and cultural and artistic courses (to develop emotional intelligence and creativity). The latter consider that there should be supplementary scope for self-learning and a supportive environment to increase the student’s self-confidence in the government schools. Finally, public schools should promote the importance of diversity of vocations: intellectual and practical in order to promote a variety of talents and to have a balance labor market supply.


### 8) What are the values that you aim at promoting in education?


The values that Anu and Krishna promote in education are apparent in their approach to Meaningful Education. Education in Thulir emphasizes traditions and the environment but also combines a mix of new and more ancient methods in order to be as flexible and inspired as possible.
According to Thulir’s founders Meaningful Education combines: -the “hands” = “the ability to shape materials and make useful objects.” -the “head” = “reading, writing, reasoning and critical thinking.” -the “heart” = “aesthetic sensibility, and a sensitivity to the environment that should ultimately lead to caring for the community around us.” The architects’ main concern is to develop and strengthen the student’s confidence in order to maximize its potential and to help him or her find their path more easily. The main values that are being promoted include: self-respect and respect for others, the importance of ethics, respect for one’s own will and judgment, and the importance of dialogue and interaction with others (where both sides learn from one another so that the teachers, animators and the community grow as well).
Approaches to education should be flexible and adapted to different circumstances and the student’s evolutions and aspirations. A dynamic approach to education means a multi-faceted education combining sitting and listening but also practical activities. One has to understand that we all have a potential for growth and change but mainstream education is sometimes too keen on a one-size-fits-all approach combined with an overemphasis on theory.
Anu and Krishna consider that a meaningful education should be established as a whole by the school, the family and the environment within which the students grow to meet the students’ particular aspirations and learning features. It is certainly not because a lot of the adult students coming to Thulir have apparently failed government school that they do not deserve or are incapable of having a decent job.
Thulir’s approach to education is interesting because on the whole all the students should acquire the same set of values, whilst having a relatively personal program emphasizing certain courses and methods tailored to their profiles. Meaningful Education means equality established through a mix of uniformity on the one side and heterogeneousness/specificity on the other. The latter is truly a mix of theory and practice through pragmatic and useful exercises such as helping run the center. Practical activities are truly valuable activities for the young adults as they learn how to deal with responsibilities through budget management and other tasks; they also acquire important skills such as teamwork or event management.
Anu and Krishna truly consider that everyone deserves the same chance and opportunity to have a rich and quality (in the true definition of the word) education.


### 9) What are the future plans for Thulir?


Anu and Krishna have different plans for Thulir but their goal is to continue to suit the students’ expectations and adjust their teachings to meet their needs through ongoing dialogue. The founders understand that the students all have different wishes, aspiration and particular skills and talent. Many of them do not have family support so the mentoring they receive at Thulir can be useful for them to find their own path.
Thulir’s founders’ hope is to create a Learning Community that “is creative, socially useful and productive.”1 This community would help their own members discover their new opportunities in the Valley. Anu and Krishna are not only planning on helping the younger generations of tribal communities to acquire a profession or to continue studying, they also have the intention of showing them different lifestyles available when one aims at living a “meaningful livelihood locally”2. Another important idea closely linked to the idea of living a meaningful life is to aim at encouraging the community members to “reflect upon and act on the issues that currently face us.” A meaningful life entails being critical about our surroundings and really try be responsive when trying to aim at other standards of life that are respectful of our own values.
Alternative lifestyles go hand in hand with alternative education; Anu and Krishna have really worked on improving all aspects of their lives to respect the environment and the community within which they live. The founders have in addition, used their knowledge as architects to improve their lifestyles and started using alternative technologies such as using only solar energy, renewable gas to cook or organic grown and processed food.
To conclude, Anu and Krishna’s approach to education has truly stood on the shoulders of giants (Newton) as they paved their own way through education. Gandhiji is one example of their numerous sources of inspiration! From the latter – who was the leading figure of Panchayat Raj which led India to independence – they took many ideas of how to approach education. What I remember about Thulir is sincerely to understand that education is truly about how to view one’s life and trying to fulfill our own inner aspirations through personal and collective growth.




**********
