---
author: thulir
date: 2006-02-28 14:16:00+00:00
draft: false
title: News II Feb
type: post
url: /wp/2006/02/news-ii-feb/
categories:
- Newsletters
---

[![](http://photos1.blogger.com/blogger/8103/1906/320/dscn0251.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/dscn0251.jpg)  
  
  
  
  
  
  
Esther's Stint in Sittilingi comes to an end  
  
A nurse by training, she came to volunteer in Sittilingi tribal hospital for a year. While she has been here, Esther came to interact with the Thulir children every week for a session. A Warm friendly person, she was much liked by the  children. She helped them to converse in English with her and in turn learnt Tamil from them. She taught children a variety of handicrafts such as making of dolls out of straw, sketching, painting and string games. She held a series of sessions  on "exploring the senses".  
  
She left in Feb, back for Hamburg and we at Thulir wish her all the best and look forward to her next visit!
