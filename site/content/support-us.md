---
date: 2007-06-17 11:42:25+00:00
draft: false
title: Support Us!
type: page
url: /wp/support-us/
---

You can become a part of the Thulir Learning Community. We believe that you can play an active role in helping this community face the challenges that confront it.

Please click [here](http://www.thulir.org/wp/wp-content/uploads/2019/01/Thulir_An-Appeal.pdf) to download our brochure that can be shared online.


### How to donate:




#### Indian funds
* Donations received in India as Rupees are exempt from Tax as per sec 80 G.
* For Rupee donations, from Indian citizens, funds can be  transferred  to the following "Thulir Trust" account
* Account no: 31799396966, State Bank of India,  Kottapatti Branch, Kottapatti, Dharmapuri District, Tamil Nadu 636906 India. IFSC code: SBIN0006244
* You can  also send a cheque in the name of "Thulir Trust" and post it to Thulir Trust, Sittilingi, Theerthamalai PO, Dharmapuri District, Tamil Nadu 636906, India.



####  Foreign  funds

* Thulir is registered under FCRA to receive foreign donations.
* If you wish to donate from abroad in foreign currency or are a Non Resident Indian, please mail us and we will send you our Foreign funds accounts details.



#### Important :


Please fill this [online donor form](http://goo.gl/forms/mp8HXeIClt) or alternatively mail us details of your donation amount, details of transfer, your full address and the purpose for which you would like us to use the donation.
