---
author: nimda321
date: 2011-06-09 11:44:06+00:00
draft: false
title: Thulir Trust
type: page
url: /wp/thulir-trust/
---

## 




## Board of Trustees


Thulir Trust was formally registered in October 2007. Read more...


## **Foreign Contribution Statements of Accounts**


Audited statements -- Receipts & payments, Income & Expenditure, Balance sheets, year wise. Read more..


##  **Foreign donations details -- Quarterly
**


Quarterly reports of donations received from foreign sources. Read more..


## **Audited Statements of Accounts**


Audited statements of accounts for previous financial years. Read more..


## Annual Reports


Annual reports for previous financial years. Read more...



### Board of Trustees


The Trustees of Thulir Trust are



	  1. Dr. Ravikumar Manoharan (Managing Trustee),
	  2. Dr. Shylaja Devi,
	  3. Ms. Rama Sastry, and
	  4. Dr. M. R. Yogananda.

Based on the rich experience of working with adivasis in Gudalur for more than two decades, S. S. Manoharan, Dr. Shylaja Devi, and Mrs. Rama Sastry decided to start this Trust to support educational activities among the children of disadvantaged communities like adivasis. S. S. Manoharan served as the Managing Trustee till 2012 until his premature demise. Dr. Shylaja Devi was elected temporary Managing Trustee. Dr. Yogananda, based in Bangalore and with a rich experience in supporting alternative building construction technologies among NGOs all over the country and abroad, has joined since, as a trustee. Dr. Ravikumar Manoharan was also invited to become a Trustee. He has worked with adivasi people in Tamilnadu and Orissa for many years. Since he is based in Sittilingi and has a good rapport with the local population he was elected to be the Managing Trustee in 2015.

The main objective is to establish Education Resource Centres in remote adivasi areas, to supplement the education they receive from Government schools and to work with the children and youth who drop out from formal education, and to start formal educational institutions like schools whenever necessary.

**Thulir Trust is a Charitable Trust Registered under Indian Trusts Act ; Registration No. 222/2007 [Gudalur, Nilgiris]**

_We are registered under Sections 12A and 80G of the IT Act and are registered under FCRA to accept foreign donations._



###  _**Foreign Contribution Statements of Accounts**_





	  * Audit statements -- Receipts & payments, Income & Expenditure, Balance sheets

	    * [2016-17](http://www.thulir.org/wp/wp-content/uploads/2018/02/FCRA_SOA_2016-17.pdf)
	    * [2015-16](http://www.thulir.org/wp/wp-content/uploads/2016/01/FC-audited-statements-2014-15.pdf)
	    * [2014-15](http://www.thulir.org/wp/wp-content/uploads/2016/01/FC-audited-statements-2014-15.pdf)
	    * [2013-14](http://www.thulir.org/wp/wp-content/uploads/2016/01/Foreign-Contribution-audit-statements-2013-14-new.pdf)



### 







### _**Foreign donations details -- quarterly**_





	  * [Jan-Mar 2019](http://www.thulir.org/wp/wp-content/uploads/2019/04/Foreign-Contributions-Donations-details-for-Jan-Mar-2019.pdf)
	  * [Oct-Dec 2018](http://www.thulir.org/wp/wp-content/uploads/2019/01/Foreign-Contributions-Donations-details-for-Oct-Dec-2018.pdf)
	  * [Jul-Sep 2018](http://www.thulir.org/wp/wp-content/uploads/2018/10/Foreign-Contributions-Donations-details-for-Jul-Sep-18.pdf)
	  * [Apr-Jun 2018](http://www.thulir.org/wp/wp-content/uploads/2018/07/Foreign-Contributions-Donations-details-for-Apr-Jun-2018.pdf)
	  * [Jan-Mar 2018](http://www.thulir.org/wp/wp-content/uploads/2018/05/Foreign-Contributions-Donations-details-for-Jan-Mar-2018.pdf)
	  * [Oct-Dec 2017](http://www.thulir.org/wp/wp-content/uploads/2018/02/Foreign-Contributions-Donations-details-for-Oct-Dec-2017.pdf)
	  * [Jul-Sep 2017](http://www.thulir.org/wp/wp-content/uploads/2017/10/Foreign-Contributions-Donations-details-for-Jul-Sep-2017.pdf)
	  * [Apr-Jun 2017](http://www.thulir.org/wp/wp-content/uploads/2017/07/Foreign-Contributions-Donations-details-for-Apr-Jun-2017.pdf)
	  * [Jan-Mar 2017](http://www.thulir.org/wp/wp-content/uploads/2017/04/Foreign-Contributions-Donations-details-for-Jan_March17.pdf)
	  * [Oct-Dec 2016](http://www.thulir.org/wp/wp-content/uploads/2017/01/Foreign-Contributions-Donations-details-for-3rd-qrt-2016-17.pdf)
	  * [Jul-Sep 2016](http://www.thulir.org/wp/wp-content/uploads/2016/11/Foreign-Contributions-Donations-details-for-2nd-qrt-2016-17.pdf)
	  * [Apl-Jun 2016](http://www.thulir.org/wp/wp-content/uploads/2016/07/Foreign-Contributions-Donations-details-for-1st-qrt-2016-171.pdf)
	  * [Jan-Mar 2016](http://www.thulir.org/wp/wp-content/uploads/2016/04/Foreign-Contributions-Donations-details-for-Jan-Mar-2016-.pdf)
	  * [Oct- Dec 2015](http://www.thulir.org/wp/wp-content/uploads/2016/01/Foreign-Contributions-Donations-details-for-Oct-Dec-2015-.pdf)




###  _**Audited Statements of Accounts**_





	  * [Statement 2016-2017](http://www.thulir.org/wp/wp-content/uploads/2018/02/Audited-SOA-2016-17.pdf)
	  * [Statement 2015-2016](http://www.thulir.org/wp/wp-content/uploads/2016/01/Thulir-Audited-Statement-2014-15.pdf)
	  * [Statement 2014-2015](http://www.thulir.org/wp/wp-content/uploads/2016/01/Thulir-Audited-Statement-2014-15.pdf)
	  * [Statement 2013-2014](http://www.thulir.org/wp/wp-content/uploads/2016/01/Thulir-Audited-Statement-2013-14.pdf)
	  * [Statement 2012-2013](http://www.thulir.org/wp/wp-content/uploads/2014/03/Aduit.pdf)
	  * [Statement 2011-2012](http://www.thulir.org/wp/wp-content/uploads/2012/11/Thulir-Audited-Statement-2011-2012.pdf)
	  * [Statement 2010-2011](http://www.thulir.org/wp/wp-content/uploads/2012/10/Thulir-Audited-Statement-2010-2011.pdf)
	  * [Statement 2009-2010](http://www.thulir.org/wp/wp-content/uploads/2011/09/Thulir-Audited-Statement-2009-2010.pdf)
	  * [Statement 2008-2009](http://www.thulir.org/wp/wp-content/uploads/2011/09/Thulir-Audited-Statement-2008-2009.pdf)
	  * [Statement 2007-2008](http://www.thulir.org/wp/wp-content/uploads/2011/09/Thulir-Audited-Statement-2007-2008.pdf)




### _**Annual Reports**_





	  * [Annual Report 2017-18](http://www.thulir.org/wp/wp-content/uploads/2018/07/Annual-Report-2017-18.pdf)
	  * [Annual Report 2016-17](http://www.thulir.org/wp/wp-content/uploads/2017/11/Thulir-Annual-Report-2016-17.pdf)
	  * [Annual Report 2015-16](http://www.thulir.org/wp/wp-content/uploads/2016/06/Thulir-Annual-Report-2015-16-.pdf)
	  * [Annual Report 2014-15](http://www.thulir.org/wp/wp-content/uploads/2015/11/Thulir-Annual-Report-2014-15.pdf)
	  * [Annual Report 2013-14](http://www.thulir.org/wp/wp-content/uploads/2014/06/Thulir-Annual-Report-2013-14.pdf)
	  * [Annual Report 2012-13](http://www.thulir.org/wp/wp-content/uploads/2013/10/Annual-Report-2012-13-final.pdf)
	  * [Annual Report 2011-12](http://www.thulir.org/wp/wp-content/uploads/2012/11/Thulir-Annual-Report-2011-12.pdf)
	  * [Annual Report 2010-11](http://www.thulir.org/wp/wp-content/uploads/2012/10/Thulir-Annual-Report-2010-11.pdf)
	  * [Annual Report 2009-10](http://www.thulir.org/wp/wp-content/uploads/2012/10/Thulir-Annual-Report-2009-10.pdf)
	  * [Annual Report 2008-09](http://www.thulir.org/wp/wp-content/uploads/2012/10/Thulir-Annual-Report-2008-09.pdf)
	  * [Annual Report 2007-08](http://www.thulir.org/wp/wp-content/uploads/2012/10/Thulir-Annual-Report-2007-08.pdf)


