---
author: nimda321
date: 2011-09-24 09:42:26+00:00
draft: false
title: Donation Form
type: page
url: /wp/donation-form/
---

As "THULIR" is registered under Section 80-G of the Income Tax Act, the donors can get tax exemption on the donations made to Thulir. We need the following details to send you our Official Receipt. If you are sending a separate letter with these details by post, then you may skip filling this form. Otherwise, please fill this e-form.

When this form is submitted, an email will be generated and sent to Thulir with a copy to your email address.



