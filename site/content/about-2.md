---
author: nimda321
date: 2011-08-03 13:23:55+00:00
draft: false
title: About
type: page
url: /wp/about-2/
---




### On This Page:





	  * [Where Is Thulir?](http://www.thulir.org/wp/about-2/#place)
	  * [Why Did It Start?](http://www.thulir.org/wp/about#begin)
	  * [What Do We Believe?](http://www.thulir.org/wp/about#belief)
	  * [What Happens Here?](http://www.thulir.org/wp/about#attempts)
	  * [How Are Things Changing?](http://www.thulir.org/wp/about#transition)
	  * [What Next?](http://www.thulir.org/wp/about#future)

[For More Details Read Our Blog](http://www.thulir.org/wp/blog)









# Where is Thulir?




###### The Place




[![where](http://www.thulir.org/wp/wp-content/uploads/2011/08/about.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/08/about.jpg)


Sittilingi is an Adivasi Village in the Dharmapuri District of Tamil Nadu, India. It is in a Valley enclosed by the Kalrayan hills to the East and the Sitteri Hills to the West.There are 21 Malayalee(adivasi) hamlets,2 Lambadi hamlets and one dalit hamlet here.

The Hill slopes are Reserved Forest areas and the Valley is green and well endowed with water. The Valley was, till 10 years ago, only serviced by the road from the North connecting to the small taluk town of Harur. The new road to Thumbal in the south has opened up access to Vazhapady [a major agricultural market] and also to Salem.



[View Larger Map](http://maps.google.co.in/maps?f=q&source=embed&hl=en&geocode=&q=Thulir,+Sittilingi&aq=&ie=UTF8&hq=Thulir,+Sittilingi&hnear=&radius=15000&ll=11.904979,78.615417&spn=0.806245,0.823975&z=9&iwloc=A)





# Why did it start?




###### The Beginnings


[![why](http://www.thulir.org/wp/wp-content/uploads/2011/08/about-2.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/08/about-2.jpg)

When we [[Anuradha and Krishna]](http://www.thulir.org/wp/?p=1142) moved to Sittilingi in 2003, our idea was to create a space for learning that would be tailored to the local needs. With the help of Tribal Health Initiative an NGO working in the area for 11 years then, we did a survey of the villages, and visited local schools to understand what was needed. To our surprise we found that almost all children below 14 were enrolled in schools! The parents were quite keen on schooling and so children were religiously sent to schools even when there were no teachers to speak of or any learning happening. We also found that most children dropped out of formal schooling at class 8 to 10 levels, often after failing exams. These teenagers consequently had very low self esteem, lacking in basic academic skills, frustrated and mostly migrated to nearby towns to work in the textile industry. Schooling had, however, convinced them that farming or any kind of work with the hands, is inferior and something to be ashamed of.

We decided to



	  1. try to improve academic skills of school going children
	  2. try to see what can be done for the school drop out teenagers.




# What Do We Believe?




###### Meaningful Education as we see it


[![why](http://www.thulir.org/wp/wp-content/uploads/2011/08/about-1.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/08/about-1.jpg)

We tried to design activities that reflect our thinking of what Meaningful Education ought to be. Meaningful Education, we believe, must comprise of a balanced mix of skills that involve



	  * The “**hands**” : the ability to handle materials and make useful objects
	  * The “**head**” : reading, writing, reasoning and critical thinking
	  * The “**heart**” : aesthetic sensibility, and a sensitivity to the environment that should ultimately lead to caring for the community around us.

Further, the purpose of Education should be to increase a learners self confidence, help her identify skills that she enjoys learning/ practising, to help lead a productive life with dignity.

We believe that current Education makes a divide between the “Head” and the “Hands”, classifying learning into Academic and Vocational skills. Further , Education has come to mean excessive emphasis on “Academics” and hardly any on the “vocational”. Matters related to “Heart”, are either “extra-curricular” activities that have no useful purpose and inconvenient issues like ethics and values that are best left out in the real world! This has lead to a severe crisis in Education today.




# What Happens Here?




###### Our Attempts


[![happenings](http://www.thulir.org/wp/wp-content/uploads/2011/08/about-3.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/08/about-3.jpg)

Over the years the after-school programme has been providing a mix of academic skills [reading, writing, math] and other learning opportunities not available in school — arts and crafts, theatre, interactions with visitors from cities/ other countries, filed trips, Environment studies etc. Classes are held in the evenings after school hours and on weekends and holidays. This has also been a training ground for some of our teenage students to learn to facilitate learning in younger children.

The full time programme for Teenage students has been exploratory in nature, given the complexities in the issues involved — bad academic skills with an inability to work on exam preparations, expectations of parents in terms of passing public exams, a natural flair in working with the hands, but a sense of inadequacy/ low esteem in pursuing farming or any vocational skill. So over the years, we have slowly tried to teach a combination of academic skills with working with the hands. When the two are integrated with real life projects, there is much learning and increase in self confidence. In the initial years we started with electrical wiring, plumbing and electronics, as these were seen to be “cool” things to do by most students. Slowly we have added organic farming activities after much discussions, exposure trips etc. One important aspect of these projects is to learn basics of Business– keeping track of costs and materials, working out estimates and profits etc. The idea is to provide possibilities of combining family farm work with supplementary income through small businesses. Most work that needs to be done in our campus [involving solar power, lighting, wiring , plumbing, farming etc.] and some in the THI hospital campus, and sometimes of other friends outside have provided opportunities for various projects.This is important as the Projects are real life ones rooted in specific realities [rather than being a merely academic/ training exercise.

An interesting development has been that the project based learning has given some of our students so much confidence and basic academic skills that many have been preparing and writing public school examinations as private candidates. Some have gone and joined school to pursue further studies and gone on to join colleges. We have been providing support for public exam preparations by conducting special classes and providing time for self study.[ Read more about our Alumni.](http://www.thulir.org/wp/category/alumni/)

The other development is that most day to day functioning of Thulir is taken care of by teenage students who after their stint as students have joined the staff. These include teaching activities such as masonry, plumbing, wiring etc., organising evening sessions for school going children, running of the Community Kitchen [provides lunch to students and meals to visitors and staff, and snacks to evening children], keeping accounts of the Trust, campus maintenance [looking after buildings, Solar PV power system, wiring and plumbing requirements, making LED light fixtures etc., organic Vegetable garden and farm] etc .[ Read more on our activities in our posts.](http://www.thulir.org/wp/category/activities/)

Thulir has become a dynamic place… Ever changing and Evolving. We began with the idea that we should be able to provide each individual student an opportunity to learn and to explore. The Dynamism we believe is do to the fact that individuals change with experience and exposure [here acquired through the learning process]. These changes have not only come about in our students but also in us facilitators and co learners. Further, community around us itself is undergoing changes resulting in changing aspirations of parents and students.




# How Are Things Changing?




###### An Adivasi Community in Transition


[![why](http://www.thulir.org/wp/wp-content/uploads/2011/08/about-4.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/08/about-4.jpg)

In the past 10 years, the Valley is undergoing rapid changes. Whereas earlier farming activity was mainly for family’s food consumption and therefore was mostly rain-fed food crops comprising of a lot of millets, of late there is a lot of cash crop cultivation [sugar cane, turmeric, tapioca, paddy etc..].

There are other changes too …. increasing mechanisation in the farms, more shops servicing local needs, improved running of schools, so more children pursuing high school, proliferation of cable TV, cell phones, increasing consumerism etc.

There are opportunities now locally for increasing income levels [cash crops/ new service sector/ organic farming etc.]. But there is als increasing pressures to continue academic learning — to join high school outside the Valley and to continue “college” education.

Since Health care and Education opportunities has increased [availability of Ambulances, easy connection to the city etc.], there is now increased need for cash.

While Basic health and hygiene has improved, there has been changes in diet [from millets to polished rice] and less physical effort in Farming [due to mechanisation]. Consequently there is an increase in life style diseases such as hyper tension and diabetes.

Alcohol abuse is on the increase and so is domestic violence. There are an alarming number of suicide attempts, especially among the youth.




# Where do we go from here?




###### The Future


[![why](http://www.thulir.org/wp/wp-content/uploads/2011/08/about-5.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/08/about-5.jpg)

These are the possibilities we see for our future:



	  * Our group becomes a small community that can help children of this valley in their Learning.
	  * We set up small businesses which can become places for learning production and business skills.

The Larger challenge that faces us is this -- _ " Can we turn ourselves into a socially active group that can bring the community together to reflect upon and act on the issues that currently face us? "_


